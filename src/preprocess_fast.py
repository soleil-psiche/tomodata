

# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# handle 16 bit hdf5 

from tkinter import filedialog
from fabio.edfimage import edfimage
edf = edfimage()
import sys
import numpy as np
import os
import h5py
import argparse
import par_tools
import glob
from scipy import signal
import time

def preprocess_fast(scannameroot=None, step=None, spooldir='/nfs/spool1/flyscan-data/psiche-soleil/com-psiche/', interactive=True):

    # get the scanname interactively - all data saved in the flyscan-data spool on srv3
    #spooldir = '/nfs/spool1/flyscan-data/psiche-soleil/com-psiche/'
    if scannameroot == None:
        scandir = filedialog.askdirectory(initialdir=spooldir, title='double click into the fast directory')
        if len(scandir) == 0: # we cancelled
            return scandir
        else:
            scannameroot = scandir.split(os.sep)[-1]
            spooldir = scandir[0:scandir.rfind(os.sep)]

    # read the angles file
    # get the list of scans - each time
    fullscannames = glob.glob(spooldir + os.sep + scannameroot + os.sep + scannameroot.lower() + "_*.nxs")
    fullscannames = sorted(fullscannames)
    # try reading the log file
    fh = open(spooldir + os.sep + scannameroot + os.sep + scannameroot + '.synchro_log', 'rt')
    lines = fh.readlines()
    fh.close()
    # this might have "refs" in it for intermediate references
    timedata = np.zeros(len(lines))
    angledata = np.zeros(len(lines))
    for ii in range(len(lines)):
        word0 = lines[ii].strip().split(', ')[0] # time
        word1 = lines[ii].strip().split(', ')[1] # angle or "refs"
        timedata[ii] = float(word0)
        if word1 == 'refs':
            angledata[ii] = np.nan
        else:
            angledata[ii] = float(word1)
    # make sure the two lsits are the same length!!!!
    if len(fullscannames)>len(lines):
        # truncate this list!!!
        print("Not all angle data available yet...")
        fullscannames = fullscannames[0:len(lines)]
    elif len(lines)>len(fullscannames):
        # truncate the other lists!!!
        print("Some projection data not ready yet...")
        timedata = timedata[0:len(fullscannames)]    
        angledata = angledata[0:len(fullscannames)]        
    else:
        print("All is happy, smile, smile :-)")



    if interactive:
        print("you have the following scans:")
        for ii,name in enumerate(fullscannames):
            if not np.isnan(angledata[ii]):  
                print(name.split(os.sep)[-1])
            else:
                print(name.split(os.sep)[-1] + ' (references)')

    # which scan?
    if step==None:
        test = input("Which scan to process? [1] : ")
        step = 1 if test == "" else int(test)

    # deal with myangle
    myangle = angledata[step-1] # nxs files are number from 1 - step 1 is item 0
    mytime = timedata[step-1]
    while np.isnan(myangle):
        print("this is a reference step - try the previous one (%d)..." % (step-1))
        step = step - 1
        myangle = angledata[step-1]
        mytime = timedata[step-1]

    scanname = scannameroot + "_fast_%d_" % step 

    # make the directories
    if not os.path.exists(scanname):
        os.mkdir(scanname)
    if not os.path.exists(scanname+os.sep+scanname+"_edfs"):
        os.mkdir(scanname+os.sep+scanname+"_edfs")

    # get the par file - rename
    os.system('cp %s/%s/*.par %s/%s.par' % (spooldir,scannameroot, scanname,scanname))

    # read the data from the nxs files on the spool
    filename = spooldir + os.sep + scannameroot + os.sep + scannameroot.lower() + "_%06d" % step + '.nxs'
    f = h5py.File(filename, 'r')
    daq = list(f.keys())[0]
    daq_grp = f[daq]
    scan_data = daq_grp["scan_data"]
    data_name = scannameroot + "_image"
    if data_name in scan_data.keys():
        orca_data = scan_data[data_name]
    else:
        print("can\'t find the data in the .nxs file!")
        return

    # do the images
    sys.stdout.write('treat images:       ')
    nproj = orca_data.shape[0]
    for ii in range(nproj):
        im = orca_data[ii]
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()
        # write as edf
        edf.data = im
        name = scanname + os.sep + scanname + '_edfs' + os.sep + scanname + ('%06d'%ii) + '.edf'
        edf.write(name)      
    f.close()

    # do the refs
    # if there is a reference closer than the initial refs
    refndxs = np.nonzero(np.isnan(angledata))[0] # intermediate references
    if len(refndxs) != 0:
        sys.stdout.write('\nuse closest intermediate reference group: ')
        dif = np.abs(timedata[refndxs] - mytime)
        refndx = np.nonzero(dif == dif.min()) 
        ndx = refndxs[refndx][0]
        refstep = ndx+1
        f = h5py.File(spooldir + os.sep + scannameroot + os.sep + scannameroot.lower() + "_%06d" % refstep + '.nxs', 'r')
        daq = list(f.keys())[0]
        daq_grp = f[daq]
        scan_data = daq_grp["scan_data"]
        data_name = scannameroot + "_image"
        if data_name in scan_data.keys():
            ref_data = scan_data[data_name]
            # take the median
            refA = np.uint16(np.median(ref_data, 0))
            f.close()        
    elif os.path.exists(spooldir + os.sep + scannameroot + os.sep + "initial_ref_000001.nxs"):
        sys.stdout.write('\ntreat initial reference group: ')
        f = h5py.File(spooldir + os.sep + scannameroot + os.sep + "initial_ref_000001.nxs", 'r')
        daq = list(f.keys())[0]
        daq_grp = f[daq]
        ref_data = daq_grp["scan_data"]
        data = ref_data["initial_ref_image"]
        # take the median
        refA = np.uint16(np.median(data, 0))
        f.close()
        if os.path.exists(spooldir + os.sep + scannameroot + os.sep + "final_ref_000001.nxs"):
            # read the final ref too
            sys.stdout.write('\ntreat initial reference group: ')
            f = h5py.File(spooldir + os.sep + scannameroot + os.sep + "final_ref_000001.nxs", 'r')
            daq = list(f.keys())[0]
            daq_grp = f[daq]
            ref_data = daq_grp["scan_data"]
            data = ref_data["final_ref_image"]
            # take the median
            refB = np.uint16(np.median(data, 0))
            f.close()
            refA = (refA*1. + refB*1.) / 2
            refA = np.uint16(refA)
    elif os.path.exists(spooldir + os.sep + scannameroot + os.sep + "ref_000001.nxs"):
        sys.stdout.write('\ntreat initial reference group: ')
        f = h5py.File(spooldir + os.sep + scannameroot + os.sep + "ref_000001.nxs", 'r')
        daq = list(f.keys())[0]
        daq_grp = f[daq]
        ref_data = daq_grp["scan_data"]
        data = ref_data["ref_image"]
        # take the median
        refA = np.uint16(np.median(data, 0))
        f.close()
    else:
        file_name = filedialog.askopenfilename(initialdir=spooldir + os.sep + scannameroot, filetypes=[('NeXus files','.nxs')], title='Select the .nxs file containing reference images')
        f = h5py.File(file_name, 'r')
        daq = list(f.keys())[0]
        daq_grp = f[daq]
        ref_data = daq_grp["scan_data"]
        data = ref_data["ref_image"]
        # take the median
        refA = np.uint16(np.median(data, 0))
        f.close()
    edf.data = refA
    name = scanname + os.sep + scanname + '_edfs' + os.sep + 'ref000000.edf'
    edf.write(name)
    name = scanname + os.sep + scanname + '_edfs' + os.sep + 'ref%06d.edf' % nproj
    edf.write(name)      
 

    # do the darks
    sys.stdout.write("\ntreat darks: ")
    f = h5py.File(spooldir + os.sep + scannameroot + os.sep + "dark_000001.nxs", 'r')
    daq = list(f.keys())[0]
    daq_grp = f[daq]
    ref_data = daq_grp["scan_data"]
    data = ref_data["dark_image"]
    # take the mean
    im = np.mean(data, 0)
    # median filter for hot pixel
    im = signal.medfilt2d(im, [3, 3])
    im = np.uint16(im)
    # write as edf
    edf.data = im
    name = scanname + os.sep + scanname + '_edfs' + os.sep + 'dark.edf'
    edf.write(name)      
    f.close()

    # update the par file to reflect the new location
    expdir = os.getcwd() + os.sep + scanname
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    pars = par_tools.par_mod(pars, 'ANGLE_OFFSET', myangle)    
    pars = par_tools.par_mod(pars, "FILE_PREFIX", expdir + os.sep + scanname + "_edfs" + os.sep + scanname)
    pars = par_tools.par_mod(pars, "BACKGROUND_FILE", expdir + os.sep + scanname + "_edfs" + os.sep + "dark.edf")
    pars = par_tools.par_mod(pars, "FF_PREFIX", expdir + os.sep + scanname + "_edfs" + os.sep + "ref")
    pars = par_tools.par_mod(pars, "OUTPUT_FILE", expdir + os.sep + scanname + "_slice.vol")

    #chk = input('for PE press - ZEROCLIPVALUE ? [0.1] : ')
    #chk = 0.1 if chk == '' else float(chk)
    #pars = par_tools.par_mod(pars, "ZEROCLIPVALUE", chk)

    par_tools.par_write(parname, pars)
    
    # finished
    sys.stdout.write('\nFinished preprocessing %s\n' % scanname)

    return scanname

                   
def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Preprocessing for a fast tomo sequence")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-d", "--dir", help="the spool directory", default='', type=str)
    parser.add_argument("-s", "--step", help="which step to preprocess", default=1, type=int)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    preprocess_fast(args.scanname, step=args.step, spooldir=args.dir, interactive=False)

    # ----------------------------------------------------- #


    




