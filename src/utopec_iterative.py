
import argparse
import par_tools
import os
import multiprocessing as mp
from scipy.optimize import leastsq
from scipy.ndimage import morphology
import edf_read_dirty
import numpy as np
import pickle
from fabio import edfimage
edf = edfimage.edfimage()
import sys
import fitting
import pylab
pylab.ion()
import time
import call_pyhst

# if the best way to regularise is to threshold, maybe should have an interactive option to walk through a
# reconstruction iteraction by iteration to set the parameters.
# should we systematically finish by a Leo regularisation step? Thresholding might leave certain artefacts uncorrectable.

def utopec_iterative(scanname, nsteps=30, histo_sharpening_steps=25, epsilon=0, initialise=True, interactive=True):
    
    # initialise - make the input corrected projections... otherwise continue from what we have already
    # parameters
    local_tomo_radius = 3000 # radius of the object in pixels
    #nsteps = 30 # how many iterations 
    ncores = 32 # for multiprocess histogram sharpening / smoothing
    # make a PFR config
    PFRconfig = PFR_config()
    PFRconfig['Options']['Epsilon']= str(epsilon) # start without this (laplacian thing)
    PFRconfig['Options']['Nbit']=str(histo_sharpening_steps) # start with quite strong sharpening
    PFRconfig['PlotOptions']['histo']='0'
    PFRconfig['PlotOptions']['stud_sl']='0'
    PFRconfig['Options']['ncores']= ncores # for multiprocess histogram sharpening / smoothing
    # for later steps, it probably makes sense to to gentle sharpening, but use the laplacian smoothing
    cleanup = True # keep only latest volumes
    timeout = 600 # for PyHST
    # need the zstart and zend for scans with bad ROIs
    zstart = 0 # 200 # 50
    zend = 0# redefined below # 165
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    # dimensions
    xsize = int(pars['NUM_IMAGE_1'][0])
    ysize =  int(pars['NUM_IMAGE_2'][0])#zend - zstart #
    zend = ysize
    midrow = int(ysize/2)
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1

    # output
    output = np.zeros((xsize, xsize, nsteps))
    output2 = np.zeros((xsize, xsize, nsteps))

    # need to scale to forward sim to account for local tomo
    # make the correction
    r = np.abs(np.arange(xsize)-(xsize-1)/2.)
    t2 = (local_tomo_radius**2 - r**2)**0.5 # the cylinder outside the reconstruction, which is missing
    #t2_im = np.tile(t2.reshape(1,xsize), (ysize,1))
    t2_im = np.tile(t2.reshape(1,xsize), (zend-zstart,1))
    t = ((xsize/2)**2 - r**2)**0.5 # the disk of the reconstruction, which _is_ forward projected
    #t_im = np.tile(t.reshape(1,xsize), (ysize,1))
    t_im = np.tile(t.reshape(1,xsize), (zend-zstart,1))
    # correction is a linear combination of these two...

    # fitting function for the correction
    def optfunc(x, real, sim, t, t2):
        dif = real - sim - x[0]*t -x[1]*t2
        return dif

    # get the colum positions
    cols = pickle.load(open('%s/utopec_replaced.pickle' % scanname,'rb'))
    mycols = []
    start = cols[0]
    offset = 0
    for ii in range(1, len(cols)):
        if cols[ii]==(start+ii-offset):
            continue
        elif cols[ii]==(start+ii-offset+1):
            continue # skip over a single missing step
        else:
            end = cols[ii-1]+1 # for range
            mycols.append([start, end])
            start = cols[ii]
            offset = ii*1
    end = cols[ii]+1
    mycols.append([start, end])
    print('columns are: '+str(mycols))

    if initialise or not os.path.exists('%s/corrected' % scanname):
        # only do this if starting from the "beginning"
        # we assume that the standard corrections have been done
        # make a folder for the corrected projections and the forward projections
        print('make the initial projections...')
        if not os.path.exists('%s/corrected' % scanname):
            os.mkdir('%s/corrected' % scanname)
        if not os.path.exists('%s/fp' % scanname):
            os.mkdir('%s/fp' % scanname)
        dark = edf.read('%s/%s_edfs/dark.edf' % (scanname, scanname)).getData() * 1.
        refA = edf.read('%s/%s_edfs/ref000000.edf' % (scanname, scanname)).getData() * 1.
        refB = edf.read('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, lastref)).getData() * 1.
        if os.path.exists('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)):
            ff = edf.read('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)).getData()
        else:
            ff = np.zeros(refA.shape)
        mydark = np.ones((ysize, xsize)) * 20
        # write projections in corrected folder
        for ii in range(nproj):
            if ii in cols:
                edf.setData(np.float32(mydark))
            else:
                im = edf.read('%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, ii)).getData()
                ref = refB*(ii/float(lastref)) + refA*(1 - (ii/float(lastref)))
                flat = (im - dark) / (ref - dark)
                flat = flat * np.exp(ff)
                flat = -np.log(flat)
                # crop if we're cropping
                flat = flat[zstart:zend, :]
                edf.setData(np.float32(flat))
            # write the projection
            edf.write('%s/corrected/corrected%06d.edf' % (scanname, ii))
    
    # get everything ready in the par file
    pars = par_tools.par_mod(pars, 'FILE_PREFIX', '%s/%s/corrected/corrected' % (expdir, scanname))
    pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
    pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
    pars = par_tools.par_mod(pars, 'TAKE_LOGARITHM', 'NO')
    pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', zend-zstart)
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', 1)
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', zend-zstart)
    #pars = par_tools.par_mod(pars, 'END_VOXEL_3', ysize)
    pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION') # remove this if we have it
    
    # don't know if paganin etc matter here

    # now we have projections and a folder for the output - iterate !
    for step in range(nsteps):
        
        # reconstruct
        pars = par_tools.par_mod(pars, 'STEAM_INVERTER', 0)
        pars = par_tools.par_mod(pars, 'OUTPUT_FILE', '%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step))
        par_tools.par_write('%s/iteration.par' % scanname, pars)
        print('step %d : reconstructing...' % step)
        #os.system('PyHST2_2015b %s/iteration.par PSICHEGPU,0,1 >> /dev/null' % scanname)
        call_pyhst.call_pyhst('%s/%s/iteration.par' % (expdir, scanname))

        # regularisation - or thresholding in step zero?
        # INSOL_9 series: scaling True, 4 and 4 iterations
        #thresholdsA = [[-0.2, 0.5, 0.12], [0.5, 100, 0.8]] # first steps
        #thresholdsB = [[-0.2, 0.5, 0.12], [0.5, 1.2, 0.8]] # subsequent steps
        # INSOL_11 - 2 and 3 iterations, scaling True
        #thresholdsA = [[-0.2, 0.35, 0.05], [0.35, 1.2, 0.65]] # first steps - sttrong bright features to include from the start?
        #thresholdsB = [[-0.2, 0.4, 0.05], [0.4, 1.2, 0.73]] # subsequent steps
        # INSOL_8 - as for 11? try with no scaling for thresholds B, 1.33 for thresholds A
        # try same for 7
        #thresholdsA = [[-0.2, 0.35, 0.05], [0.35, 1.2, 0.65]] # first steps - sttrong bright features to include from the start?
        #thresholdsB = [[-0.2, 0.4, 0.05], [0.4, 1.2, 0.73]] # subsequent steps
        # try same for 6 - but scaling on both stages, and 8 steps
        #thresholdsA = [[-0.2, 0.35, 0.05], [0.35, 1.2, 0.65]] # first steps - sttrong bright features to include from the start?
        #thresholdsB = [[-0.2, 0.4, 0.05], [0.4, 1.2, 0.73]] # subsequent steps
        # INSOL_13, 12 series: - note that for series 12, the best is after 5 iterations...scaling True 4 and 4 iterations
        #thresholdsA = [[-0.2, 0.35, 0.05], [0.35, 100, 0.65]] # first steps
        #thresholdsB = [[-0.2, 0.5, 0.05], [0.5, 1.2, 0.65]] # subsequent steps
        # INSOL_14 series:scaling True 4 and 4 iterations
        #thresholdsA = [[-0.6, 0.25, 0.05], [0.25, 100, 0.45]] # first steps
        #thresholdsB = [[-0.6, 0.35, 0.05], [0.35, 1.2, 0.85]] # subsequent steps
        # Laura Ce
        #thresholdsA = [[-100, 16.0, 0.1], [16.0, 100., 32]] # first steps
        #thresholdsB = [[-100, 16.0, 0.1], [16.0, 100., 35]] # subsequent steps
        # andy - test different idea - 
        thresholdsA = [[-0.1, 1.6, 0.5]] # first steps - just get rid of the sample, treat only the small bright things
        thresholdsB = [[-100, 16.0, 0.1], [16.0, 100., 35]] # subsequent steps

        if step<10:  # do four with two phases + pores, then four with 2 phases + pores and precipitates
            print('scale = True - thresholding with thresholdsA ')
            # increase upper threshold to 100 for first 8 steps, then normal for 8
            fch, myslice, myslicereg = Threshold_regul('%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step), False, thresholdsA)
        else:
            print('scale = True - thresholding with thresholdsB')
            fch, myslice, myslicereg = Threshold_regul('%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step), True, thresholdsB)
    
        #else:
        #    print('step %d : regularisation...' % step)
        #    fch, mask, myslice, myslicereg = Phase_field_regul('%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step), PFRconfig)

        # output
        output[:,:,step] = myslice
        output2[:,:,step] = myslicereg

        # save it
        print('step %d : save result...' % step)
        edf_read_dirty.vol_write(fch, '%s/%s/%s_iteration_reg_%d.vol' % (expdir, scanname, scanname, step))

        # fp from this one
        pars = par_tools.par_mod(pars, 'STEAM_INVERTER', 1)
        pars = par_tools.par_mod(pars, 'PROJ_OUTPUT_FILE', '%s/%s/fp/fp_%%06d.edf' % (expdir, scanname))
        pars = par_tools.par_mod(pars, 'OUTPUT_FILE', '%s/%s/%s_iteration_reg_%d.vol' % (expdir, scanname, scanname, step))
        par_tools.par_write('%s/iteration.par' % scanname, pars)
        print('step %d : forward projecting...' % step)
        #os.system('PyHST2_2015b %s/iteration.par PSICHEGPU,0,1 >> /dev/null' % scanname)
        call_pyhst.call_pyhst('%s/%s/iteration.par' % (expdir, scanname))

        # update projections    
        print('step %d : update projections...' % step)
        for column in mycols:
            # make profiles before and after
            before = edf.read('%s/corrected/corrected%06d.edf' % (scanname, (column[0]-2))).getData()
            after = edf.read('%s/corrected/corrected%06d.edf' % (scanname, (column[1]+2))).getData()
            real = (before[(midrow-25):(midrow+25), :].mean(0) + after[(midrow-25):(midrow+25), :].mean(0))/2.
            midpos = int((column[0] + column[1])/2)
            sim = edf.read('%s/fp/fp_%06d.edf' % (scanname,midpos)).getData()
            sim = sim[(midrow-25):(midrow+25), :].mean(0)
            #fit, using these profiles
            #xfit = leastsq(optfunc, [1,1], (real, sim, t, t2))[0]
            #cor = t_im * xfit[0] + t2_im * xfit[1]
            # try a different approach
            a,b = fitting.fit_linear(sim, real)
            # apply this
            for ii in range(column[0], column[1]):
                im = edf.read('%s/fp/fp_%06d.edf' % (scanname,ii)).getData()
                #im = im + cor
                im = im*a[0] + a[1]
                edf.setData(np.float32(im))
                edf.write('%s/corrected/corrected%06d.edf' % (scanname,ii))           

        # clean up volumes
        if cleanup:
            # keep only the latest volumes
            os.system('mv %s/%s/%s_iteration_%d.vol %s/%s/%s_iteration_last.vol' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_reg_%d.vol %s/%s/%s_iteration_reg_last.vol' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_%d.vol.info %s/%s/%s_iteration_last.vol.info' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_reg_%d.vol.info %s/%s/%s_iteration_reg_last.vol.info' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('rm -f  %s/%s/*.xml' % (expdir, scanname))
            os.system('rm -f  %s/%s/histogram*' % (expdir, scanname))

        pylab.figure(500)
        #pylab.plot(sim +t*xfit[0] + t2*xfit[1])
        pylab.plot(sim*a[0] + a[1])
        pylab.draw()
        pylab.show()
        # save output
        edf_read_dirty.vol_write(np.float32(output), '%s/%s/my_%d_interations.vol' % (expdir, scanname, nsteps))
        edf_read_dirty.vol_write(np.float32(output2), '%s/%s/my_%d_interations_reg.vol' % (expdir, scanname, nsteps))

################ SUBFUNCTIONS FROM LEO TURPIN ####################

def PFR_config(configfilename=None):
    """ To create the .ini file containing computation parameters"""
    config = dict()
    config['Path'] = {'SPath': ''} # saving path
    config['Options'] = {'Nbit': '30', # Number of regularization iteration - was 30
                        'GrLvlSample': '2000', # Grey level sampling (to compute histogram)
                        'AirThreeshold': '0', # Grey level value of air
                        'FlotingBoxSize': '0.02', # Size of the running mean filter used to smooth histogram
                        'MaxGlvlVar':'0.8', # Maximum grey level correction in an iteration (to compute alpha) - was 0.8... 2.4 and 30 is almost seg.
                        'Epsilon':'0.5'} # Regularization length (for the gradient term), if 0, no gradient term - andy try 1 -
                                            # prob not very useful if agressively sharpening the histogram
    config['PlotOptions'] = {'stud_sl': '1', # Slide which is plotted
                            'histo': '1'} # Plotting or not
    if configfilename!=None:
        pickle.dump(config, open(configfilename, 'wb'))
    return config

# from this... it might be possible to get threshold values automatically from the histogram...  
# for example fit with N gaussians, and then use centres, inter-centres, and sigmas for the pores/particles

def Threshold_regul(volname, scale, thresholds):
    """Brutal thresholding regularisation as a starting point..."""
    f = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = f * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = f * 1.
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg

def Threshold_dilate_regul(volname, step, thresholds):
    """ Have not used this version so far !!!!!
        Brutal thresholding regularisation as a starting point...
        Dilate the high and low ranges to include fringes"""
    f = edf_read_dirty.vol_read_dirty(volname)
    if step == 0:
        fch = f * 2. # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = f * 1.
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    # before we threshold, make a map of the out of range areas
    mask = np.zeros(f.shape, dtype=np.bool)
    mask[np.nonzero(np.logical_or(f<thresholds[0][0], f>thresholds[-1][1]))]=True
    mask = morphology.binary_dilation(mask, np.ones((3,3,3)))
    # threshold
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    # replace masked regions 
    if step == 0:
        fch[mask] = f[mask] * 2.
    else:
        fch[mask] = f[mask]
    # extract the slice
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg


def Phase_field_regul(volname, configfile):
    """ Perform the phase field regularization on the reconstruction
        pass in the name of a pyhst volume to work on, generate mask automatically
        f is the rectonstructed volume
        mask is the missing data domain (binary volume, same size as f)
        mask = 0 when data is missing, else 1 
        (note mask is applied to the reconstructed volume)
        configfile is the option file as generated by Phase_field_regul_config.py """

    # parsing option
    if type(configfile)==str:
        config = pickle.load(open(configfile, 'rb'))
        savePath = config.get('Path', 'SPath') # saving path
    elif type(configfile)==dict:
        config = configfile
        print('parameters passed in as dictionary')
    else:
        print('A bit lost...')

    opt = config['Options'] # general computation parameters 
    gr_sampl = int(opt['GrLvlSample'])
    airthr = float(opt['AirThreeshold'])
    plotop = config['PlotOptions'] # palot options
    # read the volume and info; make the mask
    f = edf_read_dirty.vol_read_dirty(volname)
    info = par_tools.par_read(volname+'.info')
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    #mask = np.ones(f.shape)
    #mask[np.nonzero(f==0)]=0
    mask = '' # do we need a mask?
    # Re-adusting grey level 
    # here we do a histogram without using the mask... why ?
    # get this from the pyhst histogram and info file!!! - histogram on 1E6 channels, with max and min which are the absolute max and min

    # read the histogram - use edf_read_dirty to force int64
    ndx = volname.rfind(os.sep)
    histoname = volname[0:ndx] + os.sep + 'histogram_' + volname[(ndx+1):]
    histo = edf_read_dirty.edf_read_dirty(histoname, 1024, [1000000, 1], [0, 0, 1000000, 1], np.int64).flatten()
    histo_min = float(info['ValMin'][0])
    histo_max = float(info['ValMax'][0])
    historange = np.linspace(histo_min, histo_max, len(histo)+1)
    # kill the bin(s) with the zeros (padding)
    zndx = np.nonzero(histo==histo.max())[0][0]
    histo[(zndx-1):(zndx+2)] = (histo[zndx-2] + histo[zndx+2])/2

    # by default, leo makes a histogram on 20,000 levels; pyhst gives 1,000,000
    #ngl,glrange = np.histogram(f,10*gr_sampl)         
    ngl = histo.reshape((20000,50)).sum(1)
    # this is used to get the 0.01% limits
    glrange = np.linspace(histo_min, histo_max, len(ngl)+1)
    cum_ngl = np.cumsum(ngl)
    nb_px = cum_ngl[-1]
    # here we slightly clip the histogram (to 0.01%)
    # note that if we remove the zero values above, we slightly change the result
    mingl = glrange[np.searchsorted(cum_ngl,1./10000.*nb_px)]
    maxgl = glrange[np.searchsorted(cum_ngl,9999./10000.*nb_px)]
    opt['intgl'] = [mingl,airthr,maxgl]
    # to avoid having to calculate it again, pass histo, historange into function
    fch,mask = Histogram_relax_poly(f,mask,opt,plotop, histo, historange)

    # optional Laplacian part
    if float(opt['Epsilon']) != 0:
        fch = Grad_reg(fch,mask,opt,plotop)

    # read a slice reg
    myslicereg = fch[:,:,int(fch.shape[2]/2)]

    return fch, mask, myslice, myslicereg


def Histogram_relax_poly(f,mask,opt,plotop, histo=None, historange=None):
    ''' Phase field relaxation step '''

    # parameters
    fl_box_sz = float(opt['FlotingBoxSize']) # box used to filtre the histo. by a floating mean
    ngmx = float(opt['MaxGlvlVar']) # maximum variation of g.lvl in one iteration (used for alpha)
    gr_sampl = int(opt['GrLvlSample']) # number of g.lvl interval for the histogram

    # discretizing f - f is converted to integer values: mingl - maxgl becomes 0 - gr_sampl
    gr_lvl = np.arange(gr_sampl+1)
    #f = np.floor((f-opt.intgl[0])/(opt.intgl[2]-opt.intgl[0])*gr_sampl)
    if histo==None:
        # discretise f to calc histogram...
        f = np.floor((f-opt['intgl'][0])/(opt['intgl'][2]-opt['intgl'][0])*gr_sampl)
        # computing the histogram of f
        mask = np.logical_and(mask,np.logical_and(f > 0, f < gr_sampl)) # define a mask to suppress values out of interest for histogram computation
        ngl,_ = np.histogram(f[mask == True],gr_lvl)
    else:
        # we pass in the pyhst histogram
        ndx1 = np.nonzero(historange>opt['intgl'][0])[0][0]
        ndx2 = np.nonzero(historange>opt['intgl'][2])[0][0]
        step = 1.*(ndx2-ndx1)/gr_sampl
        ngl = np.zeros(gr_sampl)
        for ii in range(gr_sampl):
            ndxA = ndx1+int(ii*step)
            ngl[ii] = histo[ndxA:(ndxA+int(step))].sum()

    int_box_sz = int(np.floor(fl_box_sz*gr_sampl))
    ngl_fl = running_mean(ngl, int_box_sz) # filtering

    # computing of histogram derivative
    dngl = np.gradient(ngl_fl)
    dngl_fl = running_mean(dngl,int_box_sz) # filtering

    # computing alpha
    alpha = 2*ngmx/max(abs(np.gradient(ngl_fl)))

    # grey level correspondance (relaxation)
    gr_lvl_ch = gr_lvl
    dngl_fl = np.concatenate((dngl_fl, [0]), axis=0)

    for ii in range(int(opt['Nbit'])):
        gr_lvl_ch = gr_lvl_ch + alpha*np.interp(gr_lvl_ch,gr_lvl,dngl_fl)


    ################### this part can probably be multiprocessed
    # computation of the relaxed image
    if False:
        t0 = time.time()
        fch = f[mask == True]
        fm = fch.astype(int)
        fch = gr_lvl_ch[fm]
        sys.stdout.write('calc new image\n')
        sys.stdout.flush()

        # building and rescaling f_chap
        #fchap = np.ones(f.shape)
        fchap = f * 1 # start from the unregularised f... to avoid very wrong values where out of range.
        fchap[mask == True] = fch
        fchap = fchap/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
        sys.stdout.write('build and rescale new image\n')
        sys.stdout.flush()
        t1 = time.time()
        print('original: %f secs' % (t1-t0))
    ################## this part can probably be multiprocessed
    if True:
        t0 = time.time()
        # multiprocessing to apply the new grey levels
        # split the volume f - into subblocks
        p = mp.Pool(opt['ncores'])
        # job details
        joblength = int(f.shape[2] / opt['ncores'])
        starts = np.arange(0, f.shape[2]-joblength+1, joblength)
        ends = starts + joblength
        ends[-1] = f.shape[2]
        myinfo = []
        for ii in range(opt['ncores']):
            myinfo.append((f[:,:,starts[ii]:ends[ii]], opt, gr_lvl_ch))
        print("calc new image using multiprocess with %d processes to go faster" % opt['ncores'])
        blocks = p.map(update_grey_levels, myinfo)
        print("close the pool")
        # close the pool?
        p.close()
        t1 = time.time()
        # assemble fchap
        fchap = np.zeros(f.shape)
        for ii in range(opt['ncores']):
            fchap[:,:,starts[ii]:ends[ii]] = blocks[ii]
        print('multiprocess: %f secs' % (t1-t0))

    ##################

    # figure - does another histogram so slow!
    if testbool(plotop['histo']):
        nglchap,_ = np.histogram(fch,gr_lvl_ch) # initially this used gr_lvl_ch... 
        nglchap_fl = running_mean(nglchap, int_box_sz) # filtering

        fig, ([ax1,ax2],[ax3,ax4]) = plt.subplots(2,2)
        ax1.plot(gr_lvl,np.concatenate((ngl,[0]), axis=0))
        ax1.plot(gr_lvl,np.concatenate((ngl_fl,[0]), axis=0))
        ax1.plot(gr_lvl_ch,np.concatenate((nglchap,[0]), axis=0))
        ax1.plot(gr_lvl_ch,np.concatenate((nglchap_fl,[0]), axis=0))
        ax1.set_title('histogram')
        ax2.plot(gr_lvl,gr_lvl_ch)
        ax2.set_title('Link between gl')
        ax3.plot(gr_lvl,np.concatenate((dngl,[0]), axis=0))
        ax3.plot(gr_lvl,dngl_fl)
        ax3.set_title('derivative')
        ax4.plot(gr_lvl,np.concatenate((dngl,[0]), axis=0))
        ax4.plot(gr_lvl,np.concatenate((ngl_fl,[0]), axis=0))
        ax4.set_title('derivative')
        plt.show()
        #plt.close()

    return fchap, mask

        

def update_grey_levels(info):
    # this applies Leo's histogram update to a block
    # info : f (raw values, discretise here), opt, gr_lvl_ch
    f = info[0]
    opt = info[1]
    gr_lvl_ch = info[2]
    # mask zero values
    mask = np.logical_not(f==0)
    # discretise 
    gr_sampl = int(opt['GrLvlSample'])   
    f = np.floor((f-opt['intgl'][0])/(opt['intgl'][2]-opt['intgl'][0])*gr_sampl)
    # mask out of range values
    mask = np.logical_and(mask, np.logical_and(f > 0, f < gr_sampl)) # define a mask to suppress values out of interest for histogram computation
    # computation of the relaxed image
    fch = f[mask == True]
    fm = fch.astype(int)
    #sys.stdout.write('calc new image\n')
    #sys.stdout.flush()
    fch = gr_lvl_ch[fm]
    # building and rescaling f_chap - write directly into f?
    #fchap = np.ones(f.shape)
    #fchap = f * 1 # start from the unregularised f... to avoid very wrong values where out of range.
    #fchap[mask == True] = fch
    #fchap = fchap/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
    #write directly
    #sys.stdout.write('build and rescale new image\n')
    #sys.stdout.flush()
    f[mask == True] = fch
    # scale back to original gray levels
    f = f/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
    return f



def running_mean(x, N):
    csum = np.cumsum(x)
    mean = np.zeros(x.shape)
    if N%2 == 0:
        mean[N/2:-N/2] = (csum[N:] - csum[:-N]) / float(N)
    else:
        mean[N/2+1:-N/2] = (csum[N:] - csum[:-N]) / float(N)
    return mean

    

def Grad_reg(f,mask,opt,plotop):
    '''Gradient regularization'''
    # this needs to get the multiprocess treatment too...

    # parameter
    eps = float(opt['Epsilon'])

    # regularization
    sys.stdout.write('start del2\n')
    sys.stdout.flush()
    Laplf = del2(f)
    fchap = f*1 # needs to be a copy
    sys.stdout.write('start filter\n')
    sys.stdout.flush()
    fchap[mask == True] = f[mask == True] + 2.*eps**2*Laplf[mask == True]
    sys.stdout.write('done!\n')
    sys.stdout.flush()

    return fchap


def circl_mask(sizeim,radius):
    ''' Build a mask to crop the part of the tomography which is not in the circle centered on the rotation axis'''
    x = np.arange(sizeim[0])
    y = np.arange(sizeim[1])
    X,Y = np.meshgrid(x,y)
    mask = np.zeros((sizeim[0],sizeim[1]))
    mask[np.transpose((X-sizeim[0]/2.)**2+(Y-sizeim[1]/2.)**2 < radius**2,[1,0])] = 1
    return np.dstack([mask]*sizeim[2])

def del2(f):
    ''' This function is the discrete Laplacian as defined in the matlab function del2
        because I haven't found it in numpy 
        f is a 3D image'''
    f.astype(float)

    # interior points
    L = (np.roll(f,1,0)+np.roll(f,-1,0)+np.roll(f,1,1)+np.roll(f,-1,1)+np.roll(f,1,2)+np.roll(f,-1,2))/6 - f

    # edges (just a gross nearest-neighbourg interpolation)
    L[0,:,:] = L[1,:,:]
    L[-1,:,:] = L[-2,:,:] 
    L[:,0,:] = L[:,1,:]
    L[:,-1,:] = L[:,-2,:]
    L[:,:,0] = L[:,:,1]
    L[:,:,-1] = L[:,:,-2]

    return L

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False


if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="iterative reconstruction for utopec data, using Leo Turpin\'s concept")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-n", "--nsteps", help="How many iterations? [30]", default=30, type=int)
    parser.add_argument("-nh", "--histosteps", help="How many histogram sharpening iterations? [25]", default=25, type=int)
    parser.add_argument("-e", "--epsilon", help="Epsilon for lapacian smoothing [0.0]", default=0.0, type=float)
    parser.add_argument("-z", "--initialise", help="Initialise - start from zero False/[True]", default=True, type=testbool)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    utopec_iterative(args.scanname, nsteps=args.nsteps, histo_sharpening_steps=args.histosteps, epsilon=args.epsilon, initialise=args.initialise, interactive=args.interactive)

    # ----------------------------------------------------- #
