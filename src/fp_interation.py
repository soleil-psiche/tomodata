import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from scipy import signal
import glob
import pickle

def fp_iteration(scanname, iteration=1, interactive=True):
    if interactive:
        tmp = input('iteration number:[%d] ' % iteration)
        i = iteration if tmp == '' else int(tmp)
    # read the par file
    parname_c = scanname + os.sep + scanname + "_crop.par"
    pars_c = par_tools.par_read(parname_c)

    parname_f = scanname + os.sep + scanname + "_crop_fp.par"
    pars_f = par_tools.par_read(parname_f)
    expdir = os.getcwd()


    nproj = int(pars_c['NUM_LAST_IMAGE'][0]) - int(pars_c['NUM_FIRST_IMAGE'][0]) + 1
    myfolder = expdir + os.sep + scanname + os.sep + ('main_%d' %(i))
    if not os.path.exists(myfolder):
        os.mkdir(myfolder)

    for ii in range(nproj):
        os.system('cp %s/crop/crop_%s_%06d.edf %s/main_%d/%s_%06d.edf' % (scanname, scanname, ii, scanname,i, scanname, ii))

    pars_c = par_tools.par_mod(pars_c, 'OUTPUT_FILE', expdir + os.sep + scanname + os.sep + ('%s_crop_%d.vol' % (scanname, i)))
    par_tools.par_write(parname_c, pars_c)

    os.system('PyHST2_2015b %s PSICHEGPU,0,1' % parname_c)

    pars_f = par_tools.par_mod(pars_f, 'OUTPUT_FILE', expdir + os.sep + scanname + os.sep + ('%s_crop_%d.vol' % (scanname, i)))
    par_tools.par_write(parname_f, pars_f)

    os.system('PyHST2_2015b %s PSICHEGPU,0,1' % parname_f)
 
    bad = pickle.load(open('%s/utopec_replaced.pickle' % scanname, 'rw'))
    print('bad projections:',bad)
    # replace all the bad ones - slower but more safer
    for ii in bad:
        os.system('cp %s/fp/fp_%s_%06d.edf %s/main_%d/%s_%06d.edf' % (scanname, scanname, ii, scanname,i, scanname, ii))

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="interation for forward projection")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=True, type=testbool)
    parser.add_argument("-c", "--iteration", help="iteration number", default=1, type=int)
    
    args = parser.parse_args()
    
    print(args)

    # now, call with these arguments
    fp_iteration(args.scanname, iteration=args.iteration, interactive=args.interactive)

    # ----------------------------------------------------- #
       
    
    
