
# python function to derive a movement correction
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# should be possible to import movement correction into an ipython session, 
# or to run as a stand alone
# in both cases, handle arguments and on/off of correction
# 
# try...
# interactive mode - just give the scanname
# batch mode - give all arguments needed

import sys
import numpy as np
ifft2 = np.fft.ifft2
fft2 = np.fft.fft2 
from correlateImages import correlateImages
import os
from fabio.edfimage import edfimage
reader = edfimage()
from scipy import signal
from scipy import interpolate
import subprocess
import par_tools
import pylab
pylab.ion()
import argparse
import glob
import fitting
from tkinter import filedialog

# make the movcor filename include the scan name, so it can be backed up easily.

def movement_correction(scanname, filt=0, off=False, movcor="", linear=False, interactive=True):

    # start in the experiment directory
    expdir = os.getcwd()
    # define the movcor filename
    movcorname = '%s/%s/%s_movcor.txt' % (expdir,scanname,scanname)

    # if interactive, get/confirm arguments
    if interactive:
        off = input("Switch off movement correction y/[n]  :")
        off = False if off.lower() in ["", "n", "no", "false"] else True
        if not off:
            filt = input("high-pass filter length (slow! 0=no filter) [7] :")
            filt = 7 if filt=="" else int(filt)    

    # different degrees of complexity!

    # read the par file
    parfilename = expdir + os.sep + scanname + os.sep + scanname + '.par'
    pars = par_tools.par_read(parfilename)

    # are we switching off?    
    if off:
        pars = par_tools.par_mod(pars, "DO_AXIS_CORRECTION", "NO")
        par_tools.par_write(parfilename, pars)
        print("Switch off movement correction in %s" % scanname)
        print("reconstruct to see the effect"        )
        return

    # use a pre-existing correction?
    if interactive & os.path.exists(pars["AXIS_CORRECTION_FILE"][0]):
        print("dataset %s already has a movement correction (%s)" % (scanname, pars["AXIS_CORRECTION_FILE"][0]))
        b = input('Use pre existing correction ? [y]/n :')
        if (b=="") | (b=="y"):
            pars = par_tools.par_mod(pars, "DO_AXIS_CORRECTION", "YES")
            par_tools.par_write(parfilename, pars)
            print("Use pre-existing movement correction in %s" % scanname)
            print("reconstruct to see the effect")
            return                    

    if interactive:
        b = input("Use an existing movement correction? y/[n] :")
        if b.lower() in ['y', 'yes']:
            movcorfile = filedialog.askopenfilename(initialdir=expdir)
            # copy this file to local directory
            os.system('cp %s %s' % (movcorfile,movcorname))
            # modify the parfile
            pars = par_tools.par_mod(pars, "DO_AXIS_CORRECTION", "YES")
            pars = par_tools.par_mod(pars, "AXIS_CORRECTION_FILE", movcorname)
            par_tools.par_write(parfilename, pars)
            print("copied to %s, updated par file to use it" % (movcorname.split(os.sep)[-1]))
            print("reconstruct to see the effect")
            return 

    # we have supplied a movcor file
    if movcor != "":
        # copy this file
        os.system('cp %s %s' % (movcor,movcorname))
        # modify the parfile
        pars = par_tools.par_mod(pars, "DO_AXIS_CORRECTION", "YES")
        pars = par_tools.par_mod(pars, "AXIS_CORRECTION_FILE", movcorname)
        par_tools.par_write(parfilename, pars)
        print("copied to %s, updated par file to use it" % (movcorname.split(os.sep)[-1]))
        print("reconstruct to see the effect")
        return         

    # otherwise, do the movement correction
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    nref = int(pars['FF_NUM_LAST_IMAGE'][0])
    movcor = np.zeros((nproj, 2))        
    scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * (nproj+1)
    if np.allclose(scanrange, 180, atol=10):
        scanrange = 180
    elif np.allclose(scanrange, 360, atol=10):
        scanrange = 360
    else:
        print("scan range %f not understood - parfile problem? - quitting" % scanrange)
        return
    xsize = int(pars['NUM_IMAGE_1'][0])
    ysize = int(pars['NUM_IMAGE_2'][0])

    # get the references
    refA = reader.read('%s/%s_edfs/ref000000.edf' % (scanname,scanname)).data * 1.
    refB = reader.read('%s/%s_edfs/ref%06d.edf' % (scanname,scanname,(nref))).data * 1.
    dark = reader.read('%s/%s_edfs/dark.edf' % (scanname,scanname)).data * 1.

    # do we have calib images to use?
    filelist = glob.glob('%s/%s_edfs/calib*.edf' % (scanname,scanname))
    ncalib = len(filelist)

    # use the calib images?
    if interactive:
        if ncalib>0:
            b = input("Found %d calib images - use them ? [y]/n :" % ncalib)
            if (b=="") | (b=="y"):
                print("using the %d calib images" % ncalib)
            else:
                print("not using the calib images")
                ncalib = 0

    # simplest case - no calibration data at all, 180 degree case
    if (ncalib == 0 and scanrange == 180) or (linear and scanrange == 180):
        print("Will do simple linear vertical correction / 180 degrees only")

        # normal case is this there will be a series of calib images in the same folder.
        # these go from the end angle to the start angle in equal steps

        im180 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,int(pars['NUM_LAST_IMAGE'][0]))).data * 1.
        im0 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,0)).data * 1.
        # flat field
        im180 = (im180*1. - dark) / (refB - dark)
        im0 = (im0*1. - dark) / (refA - dark)
        # flip 180 degree image
        im180 = im180[:, ::-1]
        # correlate
        cx, cy = correlateImages(im0, im180, filt)
        # linear vertical movement correction
        movcor[:, 1] = np.squeeze(np.linspace(0, cy, nproj))
         

    elif ncalib == 0 and scanrange == 360:
        print("simple linear vertical and horizontal correction for 360 degree scans")
        im360 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,int(pars['NUM_LAST_IMAGE'][0]))).data * 1.
        im180 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,int(pars['NUM_LAST_IMAGE'][0])/2)).data * 1.
        im90 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,int(pars['NUM_LAST_IMAGE'][0])/4)).data * 1.
        im270 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,3*int(pars['NUM_LAST_IMAGE'][0])/4)).data * 1.
        im0 = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,0)).data * 1.
        # flat field
        im360 = (im360*1. - dark) / (refB - dark)
        im180 = (im180*1. - dark) / ((refA*1.+refB)/2. - dark)
        im0 = (im0*1. - dark) / (refA - dark)
        im90 = (im90*1. - dark) / ((refA*0.75+refB*0.25) - dark)
        im270 = (im270*1. - dark) / ((refA*0.25+refB*0.75) - dark)


        # correlate
        cx2, cy2 = correlateImages(im0, im360, filt)
        # crop and flip im180 as per rotation_axis
        cropoffset = 0.
        xcen = (xsize+1) /2.
        print("crop data for half acquisition case... if we already have a guess for the axis")
        curpos = float(pars['ROTATION_AXIS_POSITION'][0])
        if curpos < xcen:
            xedge = int(curpos*2)
            im0c = im0[:, 0:xedge]
            im180c = im180[:, 0:xedge]
            im90c = im90[:, 0:xedge]
            im270c = im270[:, 0:xedge]
        else:
            xedge = int((curpos*2) - xsize)
            im0c = im0[:, xedge:]
            im180c = im180[:, xedge:]
            im90c = im90[:, xedge:]
            im270c = im270[:, xedge:]
            cropoffset = xedge*1.
        # flip lr
        im180c = im180c[:, ::-1]
        im270c = im270c[:, ::-1]
        cx1, cy1 = correlateImages(im0c, im180c, filt)
        cx1b, cy1b = correlateImages(im90c, im270c, filt)
        # linear movement correction
        print('Try to fit a linear movement...')
        ang = np.linspace(0, 360, int(nproj), False)
        constA = cx2 / 360. # here, ang is proportional to time, so this is speed of movement
        constB = cx1b / 360. # guess this parameter ? 18 / 360.
        movcor[:, 0] = ang * (constA*np.cos(np.deg2rad(ang)) - constB*np.sin(np.deg2rad(ang)))  # taken care of by the rotation axis?
        movcor[0:int(nproj/2), 1] = np.squeeze(np.linspace(0, cy1, int(nproj/2)))
        movcor[int(nproj/2):, 1] = np.squeeze(np.linspace(cy1, cy2, nproj-int(nproj/2)))
        # in some cases this seems to work - not all...

    else:
        print("Will do movement correction using %d calibration images" % ncalib)
        # the images seem to be arranged strangely in the calib nxs file
        im_ndx = np.round(np.linspace(nproj-1, 0, ncalib, True))
        movcorshort = np.zeros((ncalib, 2))
        for ii in range(ncalib):
            # get the image
            im = reader.read('%s/%s_edfs/%s%06d.edf' % (scanname,scanname,scanname,im_ndx[ii])).getData()
            # flatfield
            f = im_ndx[ii]/nproj
            ref = refA*(1-f) + refB*f
            im = (im*1. - dark) / (ref - dark)
            # get the calib image
            imcalib = reader.read('%s/%s_edfs/calib%06d.edf' % (scanname,scanname,ii)).getData()*1.
            # flatfield with the final ref
            imcalib = (imcalib*1. - dark) / (refB - dark)
            # correlate
            cx, cy = correlateImages(im, imcalib, filt)
            # change the sign for the output
            movcorshort[ncalib-ii-1, :] = -cx,-cy
            sys.stdout.write('done one...\n')
            sys.stdout.flush()

        # should get rid of an average vertical displacement, 
        # and an offset in the x-y plane (sinosoid)
        # vertical
        movcorshort[:,1] = movcorshort[:,1] - movcorshort[:,1].mean()
        # horizontal
        ang_calib = np.linspace(0, scanrange, ncalib)
        xfit,hfit = fitting.fit_sine(ang_calib, movcorshort[:,0])
        movcorshort[:,0] = movcorshort[:,0] - hfit
        # interpolate up to the full number of images - reverse order of im_ndx
        fint = interpolate.interp1d(im_ndx[::-1], movcorshort, kind='cubic', axis=0)
        movcor = fint(np.arange(nproj))
        pylab.plot(movcor)

    # if interactive, display the result
    a = ""    
    if interactive:
        pylab.figure()
        pylab.plot(movcor)
        a = input('use this correction? [y]/n :')
        pylab.close()

    if (a=='') or (a=='y'):

        # write the txt file
        f = open(movcorname, 'wt')
        for ii in range(len(movcor)):
          f.write('%f %f\n' % (movcor[ii,0], movcor[ii,1]))
        f.close()

        # modify the parfile
        pars = par_tools.par_mod(pars, "DO_AXIS_CORRECTION", "YES")
        pars = par_tools.par_mod(pars, "AXIS_CORRECTION_FILE", movcorname)
        par_tools.par_write(parfilename, pars)
        print("written %s, updated par file to use it" % (movcorname.split(os.sep)[-1]))
        print("reconstruct to see the effect")
    else:
        print("not modifying par file, not writing correction file")


###########################################################################################################"
def correlateImages(im0, im1, filt=0):
    '''Correlate two images, return the shift. Optional filt is a high pass filter'''

    ysize,xsize = im0.shape

    if filt>0:
        im1s = signal.convolve2d(im1, np.ones((filt,filt))/(filt**2), 'same', 'symm')
        im0s = signal.convolve2d(im0, np.ones((filt,filt))/(filt**2), 'same', 'symm')
        im1 = im1 - im1s
        im0 = im0 - im0s

    # remove edges
    im1 = im1[filt:(ysize-filt), filt:(xsize-filt)]
    im0 = im0[filt:(ysize-filt), filt:(xsize-filt)]
    
    # correlate
    cor = np.real(ifft2(fft2((im1))*np.conj(fft2((im0)))))
    cy,cx = np.nonzero(cor == cor.max())
    if -(cx-(xsize-(2*filt))) < cx:
        cx = cx-(xsize-(2*filt))
    if -(cy-(ysize-(2*filt))) < cy:
        cy = cy-(ysize-(2*filt))

    return cx,cy
###########################################################################################################

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Tomography movement correction tool")
    parser.add_argument("scanname", help="The name of the scan you want to correct")
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-f", "--filt", help="high-pass filter length (slow!) [0]", default=0, type=int)
    parser.add_argument("-o", "--off", help="Force off movement correction [False]/True", default=False, type=testbool)
    parser.add_argument("-l", "--linear", help="Force linear 0-180 movement correction [False]/True", default=False, type=testbool)
    parser.add_argument("-m", "--movcor", help="Movement correction file", default="", type=str)
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    movement_correction(args.scanname, filt=args.filt, off=args.off, movcor=args.movcor, linear=args.linear, interactive=args.interactive)

    # ----------------------------------------------------- #
