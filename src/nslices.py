
import argparse
import par_tools
import os

def nslices(scanname, value=300, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if interactive:
        value = input("Value for NSLICESATONCE [300] : ")
        value = 0.1 if value == "" else int(value)

    # write this to the par file
    print("update NSLICESATONCE value in parfile - new value = %d" % value)
    pars = par_tools.par_mod(pars, 'NSLICESATONCE', value)
    par_tools.par_write(parname, pars)




def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set zero clip value for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-v", "--value", help="Specify NSLICESATONCE value", default=300, type=int)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    nslices(args.scanname, value=args.value, interactive=args.interactive)

    # ----------------------------------------------------- #
