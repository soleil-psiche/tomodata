
import argparse
import par_tools
import os
import multiprocessing as mp
from scipy.optimize import leastsq
from scipy.ndimage import morphology
import edf_read_dirty
import numpy as np
import pickle
from fabio import edfimage
edf = edfimage.edfimage()
import sys
#sys.path.append('/shared/Python/')
import fitting
import pylab
pylab.ion()
import time
import call_pyhst
import matplotlib.pyplot as plt
from skimage.filters import threshold_otsu

# if the best way to regularise is to threshold, maybe should have an interactive option to walk through a
# reconstruction iteraction by iteration to set the parameters.
# should we systematically finish by a Leo regularisation step? Thresholding might leave certain artefacts uncorrectable.

'''
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Pierre reminder :

scanname : should be a string and the name of the folder where are the data.

# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
'''



def utopec_iterative_Pierre(scanname, nsteps=2, histo_sharpening_steps=25, epsilon=0, initialise=True, interactive=True):
    
    # initialise - make the input corrected projections... otherwise continue from what we have already
    # parameters
    local_tomo_radius = 3000 # radius of the object in pixels
    #nsteps = 30 # how many iterations a
    ncores = 32 # for multiprocess histogram sharpening / smoothing
    # make a PFR config
    PFRconfig = PFR_config()
    PFRconfig['Options']['Epsilon']= str(epsilon) # start without this (laplacian thing)
    PFRconfig['Options']['Nbit']=str(histo_sharpening_steps) # start with quite strong sharpening
    PFRconfig['PlotOptions']['histo']='0'
    PFRconfig['PlotOptions']['stud_sl']='0'
    PFRconfig['Options']['ncores']= ncores # for multiprocess histogram sharpening / smoothing
    # for later steps, it probably makes sense to to gentle sharpening, but use the laplacian smoothing
    cleanup = True # keep only latest volumes
    timeout = 600 # for PyHST
    # need the zstart and zend for scans with bad ROIs
    zstart = 0 # 200 # 50
    zend = 0# redefined below # 165
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    # dimensions
    xsize = int(pars['NUM_IMAGE_1'][0])
    ysize =  int(pars['NUM_IMAGE_2'][0])#zend - zstart #
    zend = ysize
    midrow = int(ysize/2)
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1

    # output
    output = np.zeros((xsize, xsize, nsteps))
    output2 = np.zeros((xsize, xsize, nsteps))

    # need to scale to forward sim to account for local tomo
    # make the correction
    r = np.abs(np.arange(xsize)-(xsize-1)/2.)
    t2 = (local_tomo_radius**2 - r**2)**0.5 # the cylinder outside the reconstruction, which is missing
    #t2_im = np.tile(t2.reshape(1,xsize), (ysize,1))
    t2_im = np.tile(t2.reshape(1,xsize), (zend-zstart,1))
    t = ((xsize/2)**2 - r**2)**0.5 # the disk of the reconstruction, which _is_ forward projected
    #t_im = np.tile(t.reshape(1,xsize), (ysize,1))
    t_im = np.tile(t.reshape(1,xsize), (zend-zstart,1))
    # correction is a linear combination of these two...

    # fitting function for the correction
    def optfunc(x, real, sim, t, t2):
        dif = real - sim - x[0]*t -x[1]*t2
        return dif

    # get the colum positions
    cols = pickle.load(open('%s/utopec_replaced.pickle' % scanname,'rb'))
    #be carefull sometime it appear that are missing projection numbers in the middle of the utopec hidden projection that why I creat cols2 
    mycols = []
    start = cols[0]
    offset = 0
    for ii in range(1, len(cols)):
        if cols[ii]==(start+ii-offset):
            continue
        elif cols[ii]==(start+ii-offset+1):
            continue # skip over a single missing step
        else:
            end = cols[ii-1]+1 # for range
            mycols.append([start, end])
            start = cols[ii]
            offset = ii*1
    end = cols[ii]+1
    mycols.append([start, end])
    sys.stdout.write('columns are: '+str(mycols)+'\n')
    sys.stdout.flush()
    #cols2=np.arange(mycols[0][0], mycols[0][1],1)
    cols2=np.arange(mycols[0][0],mycols[-1][-1], 1)
    mycols = [[cols2[0], cols2[-1]+1]]


    if initialise or not os.path.exists('%s/corrected' % scanname):
        # only do this if starting from the "beginning"
        # we assume that the standard corrections have been done
        # make a folder for the corrected projections and the forward projections
        sys.stdout.write('make the initial projections...\n')
        sys.stdout.flush()
        if not os.path.exists('%s/corrected' % scanname):
            os.mkdir('%s/corrected' % scanname)
        if not os.path.exists('%s/fp' % scanname):
            os.mkdir('%s/fp' % scanname)
        dark = edf.read('%s/%s_edfs/dark.edf' % (scanname, scanname)).data * 1.
        refA = edf.read('%s/%s_edfs/ref000000.edf' % (scanname, scanname)).data * 1.
        refB = edf.read('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, lastref)).data * 1.
        if os.path.exists('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)):
            ff = edf.read('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)).data
        else:
            ff = np.zeros(refA.shape)
        mydark = np.ones((ysize, xsize)) * 20
        # write projections in corrected folder
        for ii in range(nproj):
            if ii in cols2:
                edf.data = np.float32(mydark)
            else:
                im = edf.read('%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, ii)).data
                ref = refB*(ii/float(lastref)) + refA*(1 - (ii/float(lastref)))
                flat = (im - dark) / (ref - dark)
                flat = flat * np.exp(ff)
                flat = -np.log(flat) 
                # crop if we're cropping
                flat = flat[zstart:zend, :]
                edf.data = np.float32(flat)
            # write the projection
            edf.write('%s/corrected/corrected%06d.edf' % (scanname, ii))
    
    # get everything ready in the par file
    pars = par_tools.par_mod(pars, 'FILE_PREFIX', '%s/%s/corrected/corrected' % (expdir, scanname))
    pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
    pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
    pars = par_tools.par_mod(pars, 'TAKE_LOGARITHM', 'NO')
    pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', zend-zstart)
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', 1)
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', zend-zstart)
    #pars = par_tools.par_mod(pars, 'END_VOXEL_3', ysize)
    pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION') # remove this if we have it
    
    # don't know if paganin etc matter here

    # now we have projections and a folder for the output - iterate !
    for step in range(nsteps):
        
        # reconstruct
        pars = par_tools.par_mod(pars, 'STEAM_INVERTER', 0)
        pars = par_tools.par_mod(pars, 'OUTPUT_FILE', '%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step))
        par_tools.par_write('%s/iteration.par' % scanname, pars)
        sys.stdout.write('step %d : reconstructing...\n' % step)
        sys.stdout.flush()
        #os.system('PyHST2_2015b %s/iteration.par PSICHEGPU,0,1 >> /dev/null' % scanname)
        call_pyhst.call_pyhst('%s/%s/iteration.par' % (expdir, scanname))

#'''
############Pierre Adds########
        if step <= 3 :
            Wmode = 'highonly'      
        else :
            #Wmode = 'otsu'
            Wmode = 'andy_otsu'
        #Wmode = 'otsu'

        thresholdsA = ite_threshold_ADAPT('%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step), scale=False, mode=Wmode)
        sys.stdout.write('scale = True - thresholding with thresholdsA which is Auto using Otsu method \n')
        sys.stdout.write(' Wmode is : %s threshold : %s\n' %(Wmode, thresholdsA))
        sys.stdout.flush()
        fch, myslice, myslicereg = Threshold_regul('%s/%s/%s_iteration_%d.vol' % (expdir, scanname, scanname, step), scale=True, thresholds=thresholdsA)

############end Adds###########
#'''


        # output
        output[:,:,step] = myslice
        output2[:,:,step] = myslicereg

        # save it
        sys.stdout.write('step %d : save result...\n' % step)
        sys.stdout.flush()
        edf_read_dirty.vol_write(fch, '%s/%s/%s_iteration_reg_%d.vol' % (expdir, scanname, scanname, step))

        # fp from this one
        pars = par_tools.par_mod(pars, 'STEAM_INVERTER', 1)
        pars = par_tools.par_mod(pars, 'PROJ_OUTPUT_FILE', '%s/%s/fp/fp_%%06d.edf' % (expdir, scanname))
        pars = par_tools.par_mod(pars, 'OUTPUT_FILE', '%s/%s/%s_iteration_reg_%d.vol' % (expdir, scanname, scanname, step))
        par_tools.par_write('%s/iteration.par' % scanname, pars)
        sys.stdout.write('step %d : forward projecting...\n' % step)
        sys.stdout.flush()
        #os.system('PyHST2_2015b %s/iteration.par PSICHEGPU,0,1 >> /dev/null' % scanname)
        call_pyhst.call_pyhst('%s/%s/iteration.par' % (expdir, scanname))

        # update projections    
        sys.stdout.write('step %d : update projections...\n' % step)
        sys.stdout.flush()
        for column in mycols:
            # make profiles before and after
            before = edf.read('%s/corrected/corrected%06d.edf' % (scanname, (column[0]-2))).data
            after = edf.read('%s/corrected/corrected%06d.edf' % (scanname, (column[1]+2))).data
            real = (before[(midrow-25):(midrow+25), :].mean(0) + after[(midrow-25):(midrow+25), :].mean(0))/2.
            midpos = int((column[0] + column[1])/2)
            sim = edf.read('%s/fp/fp_%06d.edf' % (scanname,midpos)).data
            sim = sim[(midrow-25):(midrow+25), :].mean(0)
            #fit, using these profiles
            #xfit = leastsq(optfunc, [1,1], (real, sim, t, t2))[0]
            #cor = t_im * xfit[0] + t2_im * xfit[1]
            # try a different approach
            a,b = fitting.fit_linear(sim, real)
            # apply this
            for ii in range(column[0], column[1]):
                im = edf.read('%s/fp/fp_%06d.edf' % (scanname,ii)).data
                #im = im + cor
                im = im*a[0] + a[1]
                edf.data = np.float32(im)
                edf.write('%s/corrected/corrected%06d.edf' % (scanname,ii))           

        # clean up volumes
        if cleanup:
            # keep only the latest volumes
            os.system('mv %s/%s/%s_iteration_%d.vol %s/%s/%s_iteration_last.vol' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_reg_%d.vol %s/%s/%s_iteration_reg_last.vol' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_%d.vol.info %s/%s/%s_iteration_last.vol.info' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('mv %s/%s/%s_iteration_reg_%d.vol.info %s/%s/%s_iteration_reg_last.vol.info' % (expdir, scanname, scanname, step, expdir, scanname, scanname))
            os.system('rm -f  %s/%s/*.xml' % (expdir, scanname))
            os.system('rm -f  %s/%s/histogram*' % (expdir, scanname))
        if interactive:
            pylab.figure(500)
            #pylab.plot(sim +t*xfit[0] + t2*xfit[1])
            pylab.plot(sim*a[0] + a[1])
            pylab.draw()
            pylab.show()
        # save output
        edf_read_dirty.vol_write(np.float32(output), '%s/%s/my_%d_interations.vol' % (expdir, scanname, nsteps))
        edf_read_dirty.vol_write(np.float32(output2), '%s/%s/my_%d_interations_reg.vol' % (expdir, scanname, nsteps))

#<<<<<<<<<<<<<<<<<< Pierre functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#<<<<<<<<<<<<<<<<<< Pierre functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#<<<<<<<<<<<<<<<<<< Pierre functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# we should read the histogram produced by PyHST2 - I think this saves a look of calculation time;

def ite_threshold_ADAPT(volname, scale, mode, limit=0.01):

    info = par_tools.par_read(volname+'.info')
    ndx = volname.rfind(os.sep)
    histoname = volname[0:ndx] + os.sep + 'histogram_' + volname[(ndx+1):-3] + 'edf'
    histo = edf_read_dirty.edf_read_dirty(histoname, 1024, [1000000, 1], [0, 0, 1000000, 1], np.int64).flatten()
    histo_min = float(info['ValMin'][0])
    histo_max = float(info['ValMax'][0])
    if scale:
        histo_max = histo_max * 1.5 # this is to be consistent with the scale argument where grey value f   
        histo_min = histo_min * 1.5 # of the volume is multiply by 1.5 to obtain fch.

    historange = np.linspace(histo_min, histo_max, len(histo)+1)
    # kill the bin(s) with the zeros (padding)
    zndx = np.nonzero(histo==histo.max())[0][0]
    histo[(zndx-1):(zndx+2)] = (histo[zndx-2] + histo[zndx+2])/2

    # by default, leo makes a histogram on 20,000 levels; pyhst gives 1,000,000
    #ngl,glrange = np.histogram(f,10*gr_sampl)         
    ht = histo.reshape((500,2000)).sum(1)
    # this is used to get the 0.01% limits
    bins = np.linspace(histo_min, histo_max, len(ht)+1)
    # no need to do this twice    
    #zndx = np.nonzero(ht==ht.max())[0][0]
    #ht[(zndx-1):(zndx+2)] = (ht[zndx-2] + ht[zndx+2])/2

    for j in range(len(bins)) : 
        lim=np.sum(ht[len(bins)-j:-1])/np.sum(ht)
        if lim >= limit:
            thrM = bins[len(bins)-j]
            break

    ####### Otsu determination ##########    
    # Calculate centers of bins
    bin_mids = (bins[:-1]+bins[1:])/2.
    # Iterate over all thresholds (indices) and get the probabilities w1(t), w2(t)
    w1 = np.cumsum(ht)
    w2 = np.cumsum(ht[::-1])[::-1]
    # Get the class means mu0(t)
    mean1 = np.cumsum(ht * bin_mids)/w1
    # Get the class means mu1(t)
    mean2 = (np.cumsum((ht * bin_mids)[::-1])/w2[::-1])[::-1]
    inter_class_variance = w1[1:] * w2[:-1] * (mean1[1:] - mean2[:-1]) ** 2
    # Maximize the inter_class_variance function val
    # Andy - I got nans in this...
    inter_class_variance[np.nonzero(np.isnan(inter_class_variance))]=0
    Otsu_index = np.argmax(inter_class_variance)
    thrOt = bin_mids[1:-1][Otsu_index]
    ######## END Otsu ###################

    # don't know why but fitting seems to work now?
    # could put this in a try/except with something more robust if it fails often
    # could impose limits on the values...
    # fit gaussian to values below Otsu
    xfitA,histofitA = fitting.fit_gaussian(bin_mids[1:-1][0:Otsu_index], ht[1:-1][0:Otsu_index])
    cenA = xfitA[1]
    sigmaA = xfitA[2]
    xfitB,histofitB = fitting.fit_gaussian(bin_mids[1:-1][Otsu_index:], ht[1:-1][Otsu_index:])
    cenB = xfitB[1]
    sigmaB = xfitB[2]

    if mode=='myThreshold' :
        threshold = [[np.min(bins), np.mean(bins), 0.1],[np.mean(bins), np.max(bins), np.max(bins)]] 
        sys.stdout.write('myThreshold is : %s\n' % threshold)
        sys.stdout.flush()

    if mode=='most' :
        for j in range(len(bins)) : 
            lim=np.sum(ht[len(bins)-j:-1])/np.sum(ht)
            if lim >= limit:
                thrM = bins[len(bins)-j]
                threshold = [[thrM, np.max(bins), np.mean(bins[np.where(bins >= thrM)])]]
                break
    
    if mode=='otsu':

        a1 = np.mean(bins[np.where(bins <= thrOt)])    #mean of bins value inferior to the threshold value thrV
        a2 = np.mean(bins[np.where(bins >= thrOt)])    # ------//--------- superior -------------//-----------
        a1 = 0.5
        # andy - this should be a weighted mean
        a1 = (bin_mids[np.where(bin_mids <= thrOt)]*ht[np.where(bin_mids <= thrOt)]).sum() / ht[np.where(bin_mids <= thrOt)].sum()
        a2 = (bin_mids[np.where(bin_mids >= thrOt)]*ht[np.where(bin_mids >= thrOt)]).sum() / ht[np.where(bin_mids >= thrOt)].sum()
        threshold = [[np.min(bins), thrOt, a1],[thrOt, np.max(bins), a2]] 

    if mode=='andy_otsu':
        # andy playing...
        print('ANDY - RETURNING ADAPTED THRESHODLS USING SIGMA DEFINITION')
        threshold = [[cenA-2*sigmaA, thrOt, cenA],[thrOt, cenB+6*sigmaB, cenB]] 

    if mode=='highonly':
        # one threshold, to return bright particles in a homogeneous background
        threshold = [[cenA-2*sigmaA, cenB+6*sigmaB, cenB+6*sigmaB]] 

    return threshold
'''
    dataVol = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = dataVol * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = dataVol * 1.
#'''
'''
    ht, bins_edges = np.histogram(fch, bins=500, density=True)
    for j in range(255) : 
        lim=np.sum(ht[255-j:-1])/np.sum(ht)
        if lim >= limit:
            thrM = bins_edges[255-j]
            break

    threshold = [[np.min(fch), thrM, 0.5]]
#'''
'''    
    if mode=='myThreshold' :
        threshold = [[np.min(fch), np.mean(fch), 0.1],[np.mean(fch), np.max(fch), np.max(fch)]] 
        sys.stdout.write('myThreshold is : %s\n' % threshold)
        sys.stdout.flush()
    if mode=='most' :
        thrOt = threshold_otsu(fch)
        ht, bins_edges = np.histogram(fch, bins=256, density=True)
        for j in range(255) : 
            lim=np.sum(ht[255-j:-1])/np.sum(ht)
            if lim >= limit:
                thrM = bins_edges[255-j]
                threshold = [[thrM, np.max(fch), np.mean(fch[np.where(np.logical_and(fch >= thrOt, fch <= thrM))])]]                                 
                #threshold = [[np.min(fch), thrM, 0.5]]
                #threshold = [[np.min(fch), 1.6, 0.5]]
                break
#'''
'''
    if mode=='less' :
        thrOt = threshold_otsu(fch)
        ht, bins_edges = np.histogram(fch, bins=256, density=True)
        for j in range(255) : 
            lim=np.sum(ht[0:j+1])/np.sum(ht)
            if lim >= limit:
                thrL = bins_edges[j]
                threshold = [[np.min(fch), thrL, np.mean(fch[np.where(np.logical_and(fch >= thrL, fch <= thrOt))])]]                  
                break
#'''

'''
    if mode=='otsu':
        thrOt = threshold_otsu(fch) #minimum arguments seems working well, so let like this, but look on             skimage.filter.threshold_otsu for more options
            #thrV = thrV - 0.2*thrV    

        a1 = np.mean(fch[np.where(fch <= thrOt)])    #mean of fch value inferior to the threshold value thrV
        a2 = np.mean(fch[np.where(fch >= thrOt)])    # ------//--------- superior -------------//-----------
        a1 = 0.5
        threshold = [[np.min(fch), thrOt, a1],[thrOt, np.max(fch), a2]] 
        #threshold = [[np.min(fch), thrOt, a1],[thrOt, thrM, a2]]   
#'''

 



#second attempt for auto threshold for quick implementation
def ite_threshold(volname, scale):
    dataVol = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = dataVol * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = dataVol * 1.

    thrV = threshold_otsu(fch) #minimum arguments seems working well, so let like this, but look on skimage.filter.threshold_otsu for more options
    #thrV = thrV - 0.2*thrV    

    a1 = np.mean(fch[np.where(fch <= thrV)])    #mean of fch value inferior to the threshold value thrV
    a2 = np.mean(fch[np.where(fch >= thrV)])    # ------//--------- superior -------------//-----------
    threshold = [[np.min(fch), thrV, a1],[thrV, np.max(fch), a2]] 

    return threshold


                                                                                                                                                                                                                                                                                                                                                                                                                                    
#first attempt for auto threshold
def auto_treshold(dataVol, scale, mode ):

    if scale:
        fch = dataVol * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = dataVol * 1.
    
    histoBIN = 256 # change histoBIN by the good thing corresponding of the data 
    histo, bin_edges = np.histogram(fch, bins=histoBIN) 
    
    #Otsu method
    Nhisto = histo/np.sum(histo)
    Va = np.zeros(histoBIN)

    for T in range (0,histoBIN) : 
        sys.stdout.write(str(T)+'\n')
        sys.stdout.flush()
        w1 = np.sum(Nhisto[:T]) 
        m = np.mean(Nhisto)
        w2 = np.sum(Nhisto[T:])
        m2 = np.mean(Nhisto[:T])
        #Va[T-1] = (m*w1 - m2)**2/(w1*w2)
        mm1 = np.mean(Nhisto[:T])
        mm2 = np.mean(Nhisto[T:])
        mm1 = np.sum(np.arange(0,T-1,1))
        mm2 = np.sum(np.arange(T,histoBIN,1))

        Va[T]= w1*w2*(mm1-mm2)**2    
    
    OtsuTreshold = np.argmax(Va)*(np.max(fch)-np.min(fch))/histoBIN + np.min(fch)
    a1 = 0*(np.max(fch)-np.min(fch))/histoBIN + np.min(fch)
    a2 = np.argmax(Nhisto[:np.argmax(Va)])*(np.max(fch)-np.min(fch))/histoBIN + np.min(fch)
    b1 = (histoBIN-1)*(np.max(fch)-np.min(fch))/histoBIN + np.min(fch)
    b2 = np.argmax(Nhisto[np.argmax(Va):])*(np.max(fch)-np.min(fch))/histoBIN + np.min(fch)

    Autothreshold=[[a1, OtsuTreshold, a2] , [OtsuTreshold, b1, b2]]
    #other potential method with a switch ...


    return Autothreshold

#>>>>>>>>>>>>>>>>>>>>>>>              <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#>>>>>>>>>>>>>>>>>>>>>>>              <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#>>>>>>>>>>>>>>>>>>>>>>>              <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

################ SUBFUNCTIONS FROM LEO TURPIN ####################

def PFR_config(configfilename=None):
    """ To create the .ini file containing computation parameters"""
    config = dict()
    config['Path'] = {'SPath': ''} # saving path
    config['Options'] = {'Nbit': '30', # Number of regularization iteration - was 30
                        'GrLvlSample': '2000', # Grey level sampling (to compute histogram)
                        'AirThreeshold': '0', # Grey level value of air
                        'FlotingBoxSize': '0.02', # Size of the running mean filter used to smooth histogram
                        'MaxGlvlVar':'0.8', # Maximum grey level correction in an iteration (to compute alpha) - was 0.8... 2.4 and 30 is almost seg.
                        'Epsilon':'0.5'} # Regularization length (for the gradient term), if 0, no gradient term - andy try 1 -
                                            # prob not very useful if agressively sharpening the histogram
    config['PlotOptions'] = {'stud_sl': '1', # Slide which is plotted
                            'histo': '1'} # Plotting or not
    if configfilename!=None:
        pickle.dump(config, open(configfilename, 'wb'))
    return config

# from this... it might be possible to get threshold values automatically from the histogram...  
# for example fit with N gaussians, and then use centres, inter-centres, and sigmas for the pores/particles





#******************************************************************
#******************************************************************
#******************************************************************
#'''
def Threshold_regul_ROITEST(volname, scale, thresholds):
    """Pierre like the TEST functions which is inspired from Threshold_regul() """
    f = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = f * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = f * 1.
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]

    fch[f.shape[0]//2 -150 : f.shape[0]//2+150,f.shape[0]//2 -150 : f.shape[0]//2+150] = thresholds[1][2]
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg


def Threshold_regul_MEANTEST(volname, scale, thresholds):
    """Pierre like the TEST functions which is inspired from Threshold_regul() """
    f = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = np.dstack([np.mean(f, axis=2) * 1.5]*f.shape[2]) # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = np.dstack([np.mean(f, axis=2) * 1.]*f.shape[2])
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]

    '''
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    '''
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg



#'''
#******************************************************************
#******************************************************************
#******************************************************************

# this can perhaps be parallelised for speed

def Threshold_regul(volname, scale, thresholds):
    """Brutal thresholding regularisation as a starting point..."""
    # Andy - there's a logical issue here - if the thresholds are calculated on the scaled values, 
    # then they should eb applied to scaled values.  
    f = edf_read_dirty.vol_read_dirty(volname)
    if scale:
        fch = f * 1.5 # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = f * 1.
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg

def Threshold_dilate_regul(volname, step, thresholds):
    """ Have not used this version so far !!!!!
        Brutal thresholding regularisation as a starting point...
        Dilate the high and low ranges to include fringes"""
    f = edf_read_dirty.vol_read_dirty(volname)
    if step == 0:
        fch = f * 2. # if we start with a copy, then the out of range values are maintained
        # multiple (by 2?) because we underestimate extreme values
    else:
        fch = f * 1.
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    # before we threshold, make a map of the out of range areas
    mask = np.zeros(f.shape, dtype=np.bool)
    mask[np.nonzero(np.logical_or(f<thresholds[0][0], f>thresholds[-1][1]))]=True
    mask = morphology.binary_dilation(mask, np.ones((3,3,3)))
    # threshold
    for thr in thresholds:
        fch[np.nonzero(np.logical_and(f>thr[0], f<thr[1]))] = thr[2]
    # replace masked regions 
    if step == 0:
        fch[mask] = f[mask] * 2.
    else:
        fch[mask] = f[mask]
    # extract the slice
    myslicereg = fch[:,:,int(f.shape[2]/2)]
    return fch, myslice, myslicereg


def Phase_field_regul(volname, configfile):
    """ Perform the phase field regularization on the reconstruction
        pass in the name of a pyhst volume to work on, generate mask automatically
        f is the rectonstructed volume
        mask is the missing data domain (binary volume, same size as f)
        mask = 0 when data is missing, else 1 
        (note mask is applied to the reconstructed volume)
        configfile is the option file as generated by Phase_field_regul_config.py """

    # parsing option
    if type(configfile)==str:
        config = pickle.load(open(configfile, 'rb'))
        savePath = config.get('Path', 'SPath') # saving path
    elif type(configfile)==dict:
        config = configfile
        sys.stdout.write('parameters passed in as dictionary\n')
        sys.stdout.flush()
    else:
        sys.stdout.write('A bit lost...\n')
        sys.stdout.flush()

    opt = config['Options'] # general computation parameters 
    gr_sampl = int(opt['GrLvlSample'])
    airthr = float(opt['AirThreeshold'])
    plotop = config['PlotOptions'] # palot options
    # read the volume and info; make the mask
    f = edf_read_dirty.vol_read_dirty(volname)
    info = par_tools.par_read(volname+'.info')
    # read a slice
    myslice = f[:,:,int(f.shape[2]/2)]
    #mask = np.ones(f.shape)
    #mask[np.nonzero(f==0)]=0
    mask = '' # do we need a mask?
    # Re-adusting grey level 
    # here we do a histogram without using the mask... why ?
    # get this from the pyhst histogram and info file!!! - histogram on 1E6 channels, with max and min which are the absolute max and min

    # read the histogram - use edf_read_dirty to force int64
    ndx = volname.rfind(os.sep)
    histoname = volname[0:ndx] + os.sep + 'histogram_' + volname[(ndx+1):]
    histo = edf_read_dirty.edf_read_dirty(histoname, 1024, [1000000, 1], [0, 0, 1000000, 1], np.int64).flatten()
    histo_min = float(info['ValMin'][0])
    histo_max = float(info['ValMax'][0])
    historange = np.linspace(histo_min, histo_max, len(histo)+1)
    # kill the bin(s) with the zeros (padding)
    zndx = np.nonzero(histo==histo.max())[0][0]
    histo[(zndx-1):(zndx+2)] = (histo[zndx-2] + histo[zndx+2])/2

    # by default, leo makes a histogram on 20,000 levels; pyhst gives 1,000,000
    #ngl,glrange = np.histogram(f,10*gr_sampl)         
    ngl = histo.reshape((20000,50)).sum(1)
    # this is used to get the 0.01% limits
    glrange = np.linspace(histo_min, histo_max, len(ngl)+1)
    cum_ngl = np.cumsum(ngl)
    nb_px = cum_ngl[-1]
    # here we slightly clip the histogram (to 0.01%)
    # note that if we remove the zero values above, we slightly change the result
    mingl = glrange[np.searchsorted(cum_ngl,1./10000.*nb_px)]
    maxgl = glrange[np.searchsorted(cum_ngl,9999./10000.*nb_px)]
    opt['intgl'] = [mingl,airthr,maxgl]
    # to avoid having to calculate it again, pass histo, historange into function
    fch,mask = Histogram_relax_poly(f,mask,opt,plotop, histo, historange)

    # optional Laplacian part
    if float(opt['Epsilon']) != 0:
        fch = Grad_reg(fch,mask,opt,plotop)

    # read a slice reg
    myslicereg = fch[:,:,int(fch.shape[2]/2)]

    return fch, mask, myslice, myslicereg


def Histogram_relax_poly(f,mask,opt,plotop, histo=None, historange=None):
    ''' Phase field relaxation step '''

    # parameters
    fl_box_sz = float(opt['FlotingBoxSize']) # box used to filtre the histo. by a floating mean
    ngmx = float(opt['MaxGlvlVar']) # maximum variation of g.lvl in one iteration (used for alpha)
    gr_sampl = int(opt['GrLvlSample']) # number of g.lvl interval for the histogram

    # discretizing f - f is converted to integer values: mingl - maxgl becomes 0 - gr_sampl
    gr_lvl = np.arange(gr_sampl+1)
    #f = np.floor((f-opt.intgl[0])/(opt.intgl[2]-opt.intgl[0])*gr_sampl)
    if histo==None:
        # discretise f to calc histogram...
        f = np.floor((f-opt['intgl'][0])/(opt['intgl'][2]-opt['intgl'][0])*gr_sampl)
        # computing the histogram of f
        mask = np.logical_and(mask,np.logical_and(f > 0, f < gr_sampl)) # define a mask to suppress values out of interest for histogram computation
        ngl,_ = np.histogram(f[mask == True],gr_lvl)
    else:
        # we pass in the pyhst histogram
        ndx1 = np.nonzero(historange>opt['intgl'][0])[0][0]
        ndx2 = np.nonzero(historange>opt['intgl'][2])[0][0]
        step = 1.*(ndx2-ndx1)/gr_sampl
        ngl = np.zeros(gr_sampl)
        for ii in range(gr_sampl):
            ndxA = ndx1+int(ii*step)
            ngl[ii] = histo[ndxA:(ndxA+int(step))].sum()

    int_box_sz = int(np.floor(fl_box_sz*gr_sampl))
    ngl_fl = running_mean(ngl, int_box_sz) # filtering

    # computing of histogram derivative
    dngl = np.gradient(ngl_fl)
    dngl_fl = running_mean(dngl,int_box_sz) # filtering

    # computing alpha
    alpha = 2*ngmx/max(abs(np.gradient(ngl_fl)))

    # grey level correspondance (relaxation)
    gr_lvl_ch = gr_lvl
    dngl_fl = np.concatenate((dngl_fl, [0]), axis=0)

    for ii in range(int(opt['Nbit'])):
        gr_lvl_ch = gr_lvl_ch + alpha*np.interp(gr_lvl_ch,gr_lvl,dngl_fl)


    ################### this part can probably be multiprocessed
    # computation of the relaxed image
    if False:
        t0 = time.time()
        fch = f[mask == True]
        fm = fch.astype(int)
        fch = gr_lvl_ch[fm]
        sys.stdout.write('calc new image\n')
        sys.stdout.flush()

        # building and rescaling f_chap
        #fchap = np.ones(f.shape)
        fchap = f * 1 # start from the unregularised f... to avoid very wrong values where out of range.
        fchap[mask == True] = fch
        fchap = fchap/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
        sys.stdout.write('build and rescale new image\n')
        sys.stdout.flush()
        t1 = time.time()
        sys.stdout.write('original: %f secs\n' % (t1-t0))
    ################## this part can probably be multiprocessed
    if True:
        t0 = time.time()
        # multiprocessing to apply the new grey levels
        # split the volume f - into subblocks
        p = mp.Pool(opt['ncores'])
        # job details
        joblength = int(f.shape[2] / opt['ncores'])
        starts = np.arange(0, f.shape[2]-joblength+1, joblength)
        ends = starts + joblength
        ends[-1] = f.shape[2]
        myinfo = []
        for ii in range(opt['ncores']):
            myinfo.append((f[:,:,starts[ii]:ends[ii]], opt, gr_lvl_ch))
        sys.stdout.write("calc new image using multiprocess with %d processes to go faster\n" % opt['ncores'])
        blocks = p.map(update_grey_levels, myinfo)
        sys.stdout.write("close the pool\n")
        # close the pool?
        p.close()
        t1 = time.time()
        # assemble fchap
        fchap = np.zeros(f.shape)
        for ii in range(opt['ncores']):
            fchap[:,:,starts[ii]:ends[ii]] = blocks[ii]
        sys.stdout.write('multiprocess: %f secs\n' % (t1-t0))
        sys.stdout.flush()

    ##################

    # figure - does another histogram so slow!
    if testbool(plotop['histo']):
        nglchap,_ = np.histogram(fch,gr_lvl_ch) # initially this used gr_lvl_ch... 
        nglchap_fl = running_mean(nglchap, int_box_sz) # filtering

        fig, ([ax1,ax2],[ax3,ax4]) = plt.subplots(2,2)
        ax1.plot(gr_lvl,np.concatenate((ngl,[0]), axis=0))
        ax1.plot(gr_lvl,np.concatenate((ngl_fl,[0]), axis=0))
        ax1.plot(gr_lvl_ch,np.concatenate((nglchap,[0]), axis=0))
        ax1.plot(gr_lvl_ch,np.concatenate((nglchap_fl,[0]), axis=0))
        ax1.set_title('histogram')
        ax2.plot(gr_lvl,gr_lvl_ch)
        ax2.set_title('Link between gl')
        ax3.plot(gr_lvl,np.concatenate((dngl,[0]), axis=0))
        ax3.plot(gr_lvl,dngl_fl)
        ax3.set_title('derivative')
        ax4.plot(gr_lvl,np.concatenate((dngl,[0]), axis=0))
        ax4.plot(gr_lvl,np.concatenate((ngl_fl,[0]), axis=0))
        ax4.set_title('derivative')
        plt.show()
        #plt.close()

    return fchap, mask

        

def update_grey_levels(info):
    # this applies Leo's histogram update to a block
    # info : f (raw values, discretise here), opt, gr_lvl_ch
    f = info[0]
    opt = info[1]
    gr_lvl_ch = info[2]
    # mask zero values
    mask = np.logical_not(f==0)
    # discretise 
    gr_sampl = int(opt['GrLvlSample'])   
    f = np.floor((f-opt['intgl'][0])/(opt['intgl'][2]-opt['intgl'][0])*gr_sampl)
    # mask out of range values
    mask = np.logical_and(mask, np.logical_and(f > 0, f < gr_sampl)) # define a mask to suppress values out of interest for histogram computation
    # computation of the relaxed image
    fch = f[mask == True]
    fm = fch.astype(int)
    #sys.stdout.write('calc new image\n')
    #sys.stdout.flush()
    fch = gr_lvl_ch[fm]
    # building and rescaling f_chap - write directly into f?
    #fchap = np.ones(f.shape)
    #fchap = f * 1 # start from the unregularised f... to avoid very wrong values where out of range.
    #fchap[mask == True] = fch
    #fchap = fchap/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
    #write directly
    #sys.stdout.write('build and rescale new image\n')
    #sys.stdout.flush()
    f[mask == True] = fch
    # scale back to original gray levels
    f = f/gr_sampl*(opt['intgl'][2]-opt['intgl'][0]) + opt['intgl'][0]
    return f



def running_mean(x, N):
    csum = np.cumsum(x)
    mean = np.zeros(x.shape)
    if N%2 == 0:
        mean[N/2:-N/2] = (csum[N:] - csum[:-N]) / float(N)
    else:
        mean[N/2+1:-N/2] = (csum[N:] - csum[:-N]) / float(N)
    return mean

    

def Grad_reg(f,mask,opt,plotop):
    '''Gradient regularization'''
    # this needs to get the multiprocess treatment too...

    # parameter
    eps = float(opt['Epsilon'])

    # regularization
    sys.stdout.write('start del2\n')
    sys.stdout.flush()
    Laplf = del2(f)
    fchap = f*1 # needs to be a copy
    sys.stdout.write('start filter\n')
    sys.stdout.flush()
    fchap[mask == True] = f[mask == True] + 2.*eps**2*Laplf[mask == True]
    sys.stdout.write('done!\n')
    sys.stdout.flush()

    return fchap


def circl_mask(sizeim,radius):
    ''' Build a mask to crop the part of the tomography which is not in the circle centered on the rotation axis'''
    x = np.arange(sizeim[0])
    y = np.arange(sizeim[1])
    X,Y = np.meshgrid(x,y)
    mask = np.zeros((sizeim[0],sizeim[1]))
    mask[np.transpose((X-sizeim[0]/2.)**2+(Y-sizeim[1]/2.)**2 < radius**2,[1,0])] = 1
    return np.dstack([mask]*sizeim[2])

def del2(f):
    ''' This function is the discrete Laplacian as defined in the matlab function del2
        because I haven't found it in numpy 
        f is a 3D image'''
    f.astype(float)

    # interior points
    L = (np.roll(f,1,0)+np.roll(f,-1,0)+np.roll(f,1,1)+np.roll(f,-1,1)+np.roll(f,1,2)+np.roll(f,-1,2))/6 - f

    # edges (just a gross nearest-neighbourg interpolation)
    L[0,:,:] = L[1,:,:]
    L[-1,:,:] = L[-2,:,:] 
    L[:,0,:] = L[:,1,:]
    L[:,-1,:] = L[:,-2,:]
    L[:,:,0] = L[:,:,1]
    L[:,:,-1] = L[:,:,-2]

    return L

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False


if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="iterative reconstruction for utopec data, using Leo Turpin\'s concept")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-n", "--nsteps", help="How many iterations? [30]", default=30, type=int)
    parser.add_argument("-nh", "--histosteps", help="How many histogram sharpening iterations? [25]", default=25, type=int)
    parser.add_argument("-e", "--epsilon", help="Epsilon for lapacian smoothing [0.0]", default=0.0, type=float)
    parser.add_argument("-z", "--initialise", help="Initialise - start from zero False/[True]", default=True, type=testbool)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    utopec_iterative_Pierre(args.scanname, nsteps=args.nsteps, histo_sharpening_steps=args.histosteps, epsilon=args.epsilon, initialise=args.initialise, interactive=args.interactive)

    # ----------------------------------------------------- #
