# python function to launch reconstruction with the current parameters
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

import argparse
import par_tools
import os
import numpy as np
import sys
import call_pyhst

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def launch_reconstruction(scanname, blocksize=0, useroi=False, timeout=0, interactive=True):
    # no reason for not being interactive... keep default to false outside ipython to be consistent

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    if interactive:
        selection = input("Split reconstruction into blocks of X GB ? [no]/X : ")
        blocksize = 0 if selection == "" else int(selection)
        selection = input("Use the ROI from the par file? y/[n - whole volume] : ")
        useroi = False if (selection == '' or selection == 'n') else True

    # read the current roi
    curroi = [None]*6
    # cast to float and then to int to avoid error if there is a decimal
    curroi[0] = int(float(pars['START_VOXEL_1'][0]))
    curroi[1] = int(float(pars['START_VOXEL_2'][0]))
    curroi[2] = int(float(pars['START_VOXEL_3'][0]))
    curroi[3] = int(float(pars['END_VOXEL_1'][0]))
    curroi[4] = int(float(pars['END_VOXEL_2'][0]))
    curroi[5] = int(float(pars['END_VOXEL_3'][0]))
    if useroi:
        print("using the current ROI")
    else: # do the full size of the volume
        # do we have a half acquisition ? 
        curroi[0] = np.min([1, curroi[0]])
        curroi[1] = np.min([1, curroi[1]])
        curroi[2] = 1
        xsize = int(pars['NUM_IMAGE_1'][0])
        curroi[3] = np.max([xsize, curroi[3]])
        curroi[4] = np.max([xsize, curroi[4]])
        curroi[5] = int(pars['NUM_IMAGE_2'][0])
        # update the pars for the X and Y
        pars = par_tools.par_mod(pars,'START_VOXEL_1', curroi[0])
        pars = par_tools.par_mod(pars,'START_VOXEL_2', curroi[1])
        pars = par_tools.par_mod(pars,'END_VOXEL_1', curroi[3])
        pars = par_tools.par_mod(pars,'END_VOXEL_2', curroi[4])

    tot = 4. * (curroi[3]-curroi[0]+1) * (curroi[4]-curroi[1]+1) *(curroi[5]-curroi[2]+1) / 2**30 # 2**30 = 1GB
    print("Complete reconstruction will be %0.1f GB" % tot)

    if interactive:
        # last chance to quit without launching
        selection = input("Launch reconstruction? [y]/n : ")
        quit = True if (selection == 'n' or selection == 'no') else False
        if quit:
            return

    # do we need to divide into blocks?
    nslices = curroi[5]-curroi[2]+1
    if (blocksize != 0):
        # how big should a block be?
        slicesperblock = np.floor((blocksize * 2**30) / (4. * (curroi[3]-curroi[0]+1) * (curroi[4]-curroi[1]+1)))
        nblocks = int(np.ceil(nslices/slicesperblock))
    else:
        slicesperblock = nslices
        nblocks = 1
    print("Reconstruction will be made up of %d subvolumes" % nblocks)

    # is this a paganin reconstruction?
    pag = False
    if ('DO_PAGANIN' in pars.keys()) and (pars['DO_PAGANIN'][0] == "1"):
        pag = True

    for ii in range(nblocks):
        sys.stdout.write("reconstruct BLOCK %d... -- using GPUs ! \n" % ii)
        # write pars for BLOCK N

        suf = '_pag' if pag else ''

        pars = par_tools.par_mod(pars,'OUTPUT_FILE', '%s/%s/%s_%d%s.vol' % (expdir, scanname, scanname, ii, suf))
        # replace the z range (respect the original ROI in the par file)
        start = (ii*slicesperblock)+curroi[2]
        finish = min(start+slicesperblock-1, curroi[5])
        pars = par_tools.par_mod(pars,'START_VOXEL_3', start)
        pars = par_tools.par_mod(pars,'END_VOXEL_3', finish)

        # display the block size for the user
        sys.stdout.write("Block size will be :  %d x %d x %d \n" % ((curroi[3]-curroi[0]+1), (curroi[4]-curroi[1]+1),(finish-start+1)))
        sys.stdout.flush()

        # open and write the new parfile
        parname = scanname + os.sep + scanname + '_' + str(ii) + '.par'
        par_tools.par_write(parname, pars) 
        parfullname = expdir + os.sep + parname

        # CALL WITH THE HELPER FUNCTION FOR BEAMLINE SPECIFIC BEHAVIOUR
        call_pyhst.batch_pyhst(parfullname, timeout)

    # end of reconstruction messages
    if beamline['BEAMLINE'][0] == 'PSICHE':
        print("finished reconstruction scan %s" % scanname)
    elif beamline['BEAMLINE'][0] == 'PSICHE_NEW':
        print("Reconstruction of %s submitted to queue" % scanname)
    elif beamline['BEAMLINE'][0] == 'ANATOMIX':
        print("Reconstruction of %s submitted to queue" % scanname)
    else:
        print("Beamline name not recognised")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Launch reconstruction of volume with current parameters")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-b", "--blocksize", help="Max blocksize in GB for 32bit .vol", default=0, type=int)
    parser.add_argument("-r", "--roi", help="Use ROI defined in par file True/[False]", default=False, type=testbool)    
    parser.add_argument("-t", "--timeout", help="kill stuck pyhst job after N seconds", default=0, type=int)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    launch_reconstruction(args.scanname, blocksize=args.blocksize, useroi=args.roi, timeout=args.timeout, interactive=args.interactive)

    # ----------------------------------------------------- #
