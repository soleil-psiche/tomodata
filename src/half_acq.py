# python function to manage haldf acquisition scans
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

import argparse
import par_tools
import os
import numpy as np
import pylab

def half_acq(scanname, activate=True, pad=0, interactive=True):

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    # if disactivating the half acquisition
    if not activate:
        print("Disactive half acquisition")
        pars = par_tools.par_mod(pars,'OPTIONS', '{ \'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'Y\'}')
        par_tools.par_write(parname, pars)
        return

    # check it's a 360 scan
    nproj = int(pars['NUM_LAST_IMAGE'][0])-int(pars['NUM_FIRST_IMAGE'][0])+1
    scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * nproj
    if scanrange > 370:
        print("scan range is greater than 370 degrees - ok for half acq, but something probably wrong in scan config!!!")
    elif scanrange < 350:
        print("This isn't a 360 degree scan - sorry")
        return
    else:
        print("This is a 360 degree scan")

    # otherwise, fix the padding
    rotaxis = float(pars["ROTATION_AXIS_POSITION"][0])
    xsize = float(pars['NUM_IMAGE_1'][0])
    delta = np.abs((xsize/2)-rotaxis)
    print("Rotation axis appears to be offset by %0.1f pixels" % delta)
    if delta<50:
        autopad = 0
        print("Very small offset... assume not a real half acquisition")
    else:
        autopad = 50*np.ceil(delta/50)
    print("Automatic padding would add %d pixels" % autopad)
    if interactive: # if interactive, check with user
        selection = input("Enter the padding to apply in pixels [%d] : " % autopad)
        pad = autopad if selection=="" else int(selection)
    elif pad==0: # if not interactive, and no pad supplied 
        pad = autopad
    print("Pad scan by %d pixels" % pad)

    # apply this 
    newsize = xsize + (2*pad)
    if pad>0:
        print("reconstructing extended slice %d x %d" % (newsize, newsize))
        pars = par_tools.par_mod(pars,'OPTIONS', '{ \'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'N\'}')
    else:
        print("pad = 0, so disactive half acquisition")
        pars = par_tools.par_mod(pars,'OPTIONS', '{ \'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'Y\'}')
    pars = par_tools.par_mod(pars,'START_VOXEL_1', int(-(pad-1)))
    pars = par_tools.par_mod(pars,'START_VOXEL_2', int(-(pad-1)))
    pars = par_tools.par_mod(pars,'END_VOXEL_1', int(xsize+pad))
    pars = par_tools.par_mod(pars,'END_VOXEL_2', int(xsize+pad))

    # we need to tweak the angle step so that PyHST recognises half-acq scan
    trueangle = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0])
    nprojeff = np.round(360. / trueangle)
    lastprojeff = int(nprojeff + int(pars['NUM_FIRST_IMAGE'][0]) - 1)
    angleeff = 360. / nprojeff    
    comment = 'initially ' + str(trueangle)
    pars = par_tools.par_mod(pars,'ANGLE_BETWEEN_PROJECTIONS', angleeff, comment)
    comment = 'initially ' + str(nproj)
    pars = par_tools.par_mod(pars,'NUM_LAST_IMAGE', lastprojeff, comment)
        
    print("Par file updated")
    par_tools.par_write(parname, pars)


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Manage half acquisition / 360 degree scan reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to treat", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-a", "--activate", help="Activate or disactivate an existing correction [True]/False", default=True, type=testbool)
    parser.add_argument("-p", "--pad", help="Padding to reconstruction size [default Auto]", default=0, type=int)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    half_acq(args.scanname, activate=args.activate, pad=args.pad, interactive=args.interactive)

    # ----------------------------------------------------- #
