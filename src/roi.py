
import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys

# globals for figure function
global cid, fig, coords
coords = []

def onclick(event):
    ix, iy = event.xdata, event.ydata
    if len(coords)!=0:
        sys.stdout.write("\b"*3)
        sys.stdout.flush()
    sys.stdout.write("(%d)" % (len(coords)+1))
    sys.stdout.flush()    coords.append((ix, iy))    if len(coords)==4:
        fig.canvas.mpl_disconnect(cid)
        pylab.close(fig)
        return

def roi(scanname, roi=[None]*6, gui=True, reset=False, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    # read the current roi
    curroi = [None]*6
    curroi[0] = int(pars['START_VOXEL_1'][0])
    curroi[1] = int(pars['START_VOXEL_2'][0])
    curroi[2] = int(pars['START_VOXEL_3'][0])
    curroi[3] = int(pars['END_VOXEL_1'][0])
    curroi[4] = int(pars['END_VOXEL_2'][0])
    curroi[5] = int(pars['END_VOXEL_3'][0])
    print("Current ROI is " + str(curroi))

    # work out the scan range
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    nref = int(pars['FF_NUM_LAST_IMAGE'][0])
    movcor = np.zeros((nproj, 2))        
    scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * (nproj+1)
    if np.allclose(scanrange, 180, atol=10):
        scanrange = 180
    elif np.allclose(scanrange, 360, atol=10):
        scanrange = 360
    else:
        print("scan range %f not understood - parfile problem? - quitting" % scanrange)
        return

    if interactive:
        # interactive - reset ROI ?
        tmp = input('Reset the ROI? y/[n] : ')
        tmp = False if tmp.lower() in ['', 'n', 'no', 'false'] else True
        if tmp:
            reset = True

    if reset:
        # just reset to reconstruct everything
        print("Reset ROI in par file")
        sizex = int(pars["NUM_IMAGE_1"][0])
        sizez = int(pars["NUM_IMAGE_2"][0])
        pars = par_tools.par_mod(pars, 'START_VOXEL_1', 1)
        pars = par_tools.par_mod(pars, 'START_VOXEL_2', 1)
        pars = par_tools.par_mod(pars, 'START_VOXEL_3', 1)
        pars = par_tools.par_mod(pars, 'END_VOXEL_1', sizex)
        pars = par_tools.par_mod(pars, 'END_VOXEL_2', sizex)
        pars = par_tools.par_mod(pars, 'END_VOXEL_3', sizez)
        print("Reset options")
        if scanrange == 180:
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'Y\'}')
        else:
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'N\'}')
        par_tools.par_write(parname, pars)
        return       

    if interactive:
        selection = input("Use GUI to select ROI ? [y]/n : ")
        selection = "y" if selection=="" else selection
        
        if selection=="y":
            # GUI to select ROI
            # get the 0 and 90 projections
            nproj = int(pars['NUM_LAST_IMAGE'][0])
            lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
            scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * (nproj+1)
            if np.allclose(scanrange, 180, atol=10):
                scanrange = 180
            elif np.allclose(scanrange, 360, atol=10):
                scanrange = 360
            else:
                print("scan range %f not understood - parfile problem? - quitting" % scanrange)
                return

            edfpath = scanname + os.sep + scanname + "_edfs" + os.sep     
            # references
            dark = 1.0*edf.read(edfpath + "dark.edf").getData()
            refA = 1.0*edf.read(edfpath + "ref000000.edf").getData()
            refB = 1.0*edf.read(edfpath + "ref%06d.edf" % (lastref)).getData()
            # images
            imA = 1.0*edf.read(edfpath + scanname + "000000.edf").getData()
            ndx = int(nproj/2) if scanrange == 180 else int(nproj/4)
            imB = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndx).getData()
            # flatfield
            imA = (imA - dark) / (refA - dark)
            if scanrange == 180:
                imB = (imB - dark) / (((refA+refB)/2) - dark)
            else:
                imB = (imB - dark) / (((refA*0.75)+(refB*0.25)) - dark) 
            # show these, and let user select
            myminA = imA.mean()-(3*imA.std())
            mymaxA = imA.mean()+(3*imA.std())
            myminB = imB.mean()-(3*imB.std())
            mymaxB = imB.mean()+(3*imB.std())
            global fig, cid, coords
            repeat = True
            while repeat:
                coords = []
                fig = pylab.figure(1)            
                pylab.subplot(1,2,1)
                pylab.imshow(imA)
                pylab.clim(myminA, mymaxA)
                pylab.title('First: click top-left, bottom-right')
                pylab.gray()
                pylab.subplot(1,2,2)
                pylab.imshow(imB)
                pylab.clim(myminB, mymaxB)
                pylab.title('Second: click top-left, bottom-right')
                pylab.gray()
                # get clicks from the image
                cid = fig.canvas.mpl_connect('button_press_event', onclick) 
                pylab.show()
                input('Click figure to select ROI, then enter to continue...')                pylab.close(1)
                repeat = False
                tmp = np.array(coords).flatten()
                for item in tmp:
                    if item==None:
                        repeat=True
                if repeat == True:
                    print("try again - make sure to click INSIDE the images")
            # if the rotation axis is the centre?
            sizex = int(pars["NUM_IMAGE_1"][0])
            roi[0] = int(coords[0][0])
            roi[1] = int(sizex - coords[3][0])#int(coords[2][0])#
            roi[2] = int(np.min([coords[0][1],coords[2][1]]))
            roi[3] = int(coords[1][0])
            roi[4] = int(sizex - coords[2][0])#int(coords[3][0])#
            roi[5] = int(np.max([coords[1][1],coords[3][1]]))
            
        else:
            # manual input
            # if no input ROI, get it from the user
            if np.all(elem in None for elem in roi):
                roi[0] = input('X start [%d]: ' % curroi[0])
                roi[1] = input('Y start [%d]: ' % curroi[1])
                roi[2] = input('Z start [%d]: ' % curroi[2])
                roi[3] = input('X end [%d]: ' % curroi[3])
                roi[4] = input('Y end [%d]: ' % curroi[4])
                roi[5] = input('Z end [%d]: ' % curroi[5])
                for ii in range(len(roi)):
                    if roi[ii].strip('-').isdigit():
                        roi[ii] = int(roi[ii])
                    elif roi[ii] == "":
                        roi[ii] = curroi[ii]
                    else:
                        print("Did not understand roi - try again...")
                        return

        # interactive - options
        print("Options:")
        centreaxis = input('Axis to centre of reconstruction? y/[n] : ')
        centreaxis = False if centreaxis.lower() in ['', 'false', '0', 'n', 'no'] else True
        if scanrange == 360:
            double = input('Half/double acquisition? [y]/n : ')
            double = True if double.lower() in ['', 'true', '1', 'y', 'yes'] else False
        else:
            double = input('Half/double acquisition? y/[n] : ')
            double = False if double.lower() in ['', 'false', '0', 'n', 'no'] else True            
        if centreaxis and double:
            print("using axis to centre = True and half acquisition = True")
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'N\'}')
        elif centreaxis and not double:
            print("using axis to centre = True and half acquisition = False")
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'Y\'}')
        elif not centreaxis and double:
            print("using axis to centre = False and half acquisition = True")
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'N\' , \'avoidhalftomo\':\'N\'}')
        else:
            print("using axis to centre = False and half acquisition = False")
            pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'N\' , \'avoidhalftomo\':\'Y\'}')
        print('updated par file with options...')
        par_tools.par_write(parname, pars)


    # write this to the par file - roi was either passed in, or given interactively
    if np.any(roi != curroi):
        pars = par_tools.par_mod(pars, 'START_VOXEL_1', roi[0])
        pars = par_tools.par_mod(pars, 'START_VOXEL_2', roi[1])
        pars = par_tools.par_mod(pars, 'START_VOXEL_3', roi[2])
        pars = par_tools.par_mod(pars, 'END_VOXEL_1', roi[3])
        pars = par_tools.par_mod(pars, 'END_VOXEL_2', roi[4])
        pars = par_tools.par_mod(pars, 'END_VOXEL_3', roi[5])
        par_tools.par_write(parname, pars)
        print("Updated par file with new ROI")
    else:
        print("No change to ROI")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set the reconstruction ROI for a tomo scan")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-r", "--roi", help="roi to reconstruct [x1, y1, z1, x2, y2, z2]", default=[None]*6,nargs=6, type=int)
    parser.add_argument("-x", "--reset", help="Reset roi [False]", default=False, type=testbool)
    parser.add_argument("-g", "--gui", help="Use GUI selection of roi [False]", default=False, type=testbool)
    
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    roi(args.scanname, roi=args.roi, gui=args.gui, reset=args.reset, interactive=args.interactive)

    # ----------------------------------------------------- #
