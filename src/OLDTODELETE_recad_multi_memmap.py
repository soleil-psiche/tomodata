# python function to launch reconstruction with the current parameters
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# maybe needs a try catch finally structure to close the pool

# -*- coding: utf-8 -*-
"""
test to try reading a file from multi processes
tested on a 624 x 624 x 800 volume
ncores time
1   28.1
2   17
4   11.8
8   8.7
16  7.4
32  6.7
64  5.2
note that the single process reordering and writing is about 2 seconds
looks as if 8 cores is useful speed up :-)

Use HST reading and writing tools heavily inspired by / copied from Henry Proudhon...

python function to recad N subvolumes into a single 8bit raw
launch from the experiment directory
scan name is argument - so look for all scanname/scanname+_N.raw OR scanname+_N_pag.raw

"""

import multiprocessing as mp
import sys
import numpy as np
import struct
import time
import os
import glob
import argparse


def recad_multi_all(scanname, low=None, high=None, format='8bit', suffix='', ncores=8, remove=False, interactive=True):

    if (not interactive and ((low==None) | (high==None))):
        print("In none interactive mode, you must supply limits")
        return 

    if interactive:
        print("Will recad volumes from %s" % scanname)
        selection = input("Low limit for recad : ")
        low = float(selection)
        selection = input("High limit for recad : ")
        high = float(selection)
        selection = input("Output format [8]/16 bit : ")
        format = '8bit' if selection in ['', '8', '8bit'] else '16bit'
        selection = input("Volume name suffix? _pag / [None] : ")
        suffix = '' if selection == '' else '_pag'        
        selection = input("Remove 32bit .vol after recad? y/[n] : ")
        remove = True if selection.lower() in ['y', 'yes', 'true'] else False        

    # format?
    if format not in ['8bit', '16bit']:
        print("format %s not understood - should be 8bit or 16bit" % format)

    # display what we are going to do !
    print("_____________________________________")
    print("convert all 32bit %s/%s_?%s.vol" % (scanname, scanname, suffix))
    print("to %s %s/%s.raw files" % (format, scanname, scanname))
    print("using %0.2f - %0.2f boundaries" % (low, high))
    print("using %d cores to go faster..." % ncores)
    print("_____________________________________")

    if interactive:
        # last chance to quit without launching
        selection = input("Launch recad? [y]/n : ")
        quit = True if (selection == 'n' or selection == 'no') else False
        if quit:
            return

    # filenames and handles
    filenames = sorted(glob.glob(scanname+os.sep+scanname+'_?'+suffix+'.vol'))
    # remove slices...
    for filename in filenames:
        if filename.find('slice')!=-1:
            filenames.remove(filename)
    print("Will recad and combine:")
    for filename in filenames:
        print(filename)

    # Now the processing part. Get the pool...
    p = mp.Pool(ncores)        
    totalz = 0
    t00 = os.times()[4]
    # now, use a try / except / finally loop
    try:
    #if True:
        #print('NOT USING THE TRY')

        # loop over the vol files
        for jj in range(len(filenames)):
           
            filename = filenames[jj]
            filenameraw = filename.replace(".vol", ".raw")
 
            # read the vol.info file
            pars = {}
            f2 = open(filename+'.info', 'rt')
            lines = f2.readlines()
            f2.close()
            for line in lines:
                parts = line.split("=")
                if len(parts)==2:
                    name = parts[0].strip()
                    try:
                        val = float(parts[1].strip())
                        pars.update({name:val})
                    except:
                        print("failed to read %s" % name)

            nx = int(pars['NUM_X'])
            ny = int(pars['NUM_Y'])
            if pars.has_key('NUM_Z'):
                nz = int(pars['NUM_Z'])
            else:
                nz = 1
            print('volume ', filename, ' size is ', nx, 'x', ny, 'x', nz)
            totalz+=nz        

            slicesperjob = 64
            # build the list [[firstslice, nslices], [], []]
            firstslices = np.arange(0, nz, slicesperjob)
            nslices = firstslices[1::]-firstslices[0:-1:]
            nslices = np.append(nslices, nz-firstslices[-1])
            n = len(firstslices)
            mylist = zip(firstslices, nslices, [filename]*n, [nx]*n,[ny]*n,[format]*n,[low]*n, [high]*n)

            # for the output
            if format=='8bit':
                #datarecad = np.empty((nz, ny, nx), dtype=np.uint8)
                mm = np.memmap(filenameraw, np.uint8, 'w+', 0, (nx, ny, nz), 'F')
            else:
                #datarecad = np.empty((nz, ny, nx), dtype=np.uint16)
                mm = np.memmap(filenameraw, np.uint16, 'w+', 0, (nx, ny, nz), 'F')
            
            for start in range(0, n, ncores):

                end = int(np.min((start+ncores, n)))
                print("start %d jobs, for slices %d to %d" % ((end-start), mylist[start][0], (mylist[end-1][0]+mylist[end-1][1])))
        
                t0 = os.times()[4]
                blocks = p.map(readblock, mylist[start:end])
                t1 = os.times()[4]
                
                for ii in range(len(mylist[start:end])):
                    print("writing a chunk to memory map and flush... ")
                    mm[:,:,mylist[ii+start][0]:(mylist[ii+start][0]+mylist[ii+start][1])] = blocks[ii]
                    mm.flush() 

            t2 = os.times()[4]
            
            print("FINISHED... delete the memmap" )
            #fraw.close()
            del mm

            print('writing .info file')
            f = open(filenameraw + '.info', 'w')
            f.write('! PyHST_SLAVE VOLUME INFO FILE\n')
            f.write('NUM_X = ' + str(nx) + '\n')
            f.write('NUM_Y = ' + str(ny) + '\n')
            f.write('NUM_Z = ' + str(totalz) + '\n')
            if format=='8bit':
                f.write('! DATA FORMAT : UINT8 (unsigned integer 8 bit) \n')
            else:
                f.write('! DATA FORMAT : UINT16 (unsigned integer 16 bit) \n')
            f.write('! RECAD FROM 32bit with limits %f to %f\n' % (low, high))
            f.close()
            print("combined volume is ", nx, 'x', ny, 'x', totalz)
            
            if remove:
                os.system('rm -f %s*' % filename)
                print('Removed the 32bit .vol')

        #print("total: %0.1f" % (t3-t00))
    except BaseException as e:
        print("there was a problem with the recad...")
        print(str(e))
        print(e.message)
    finally:
        print("close the pool")
        # close the pool?
        p.close()
        try:
            del mm # del teh mm if it's still open
        except:
            print('memory map already closed')

# subfunction definitions
def recad16(data, min, max):
    low_values_indices = data < min
    data[low_values_indices] = min
    large_values_indices = data > max
    data[large_values_indices] = max
    data_uint16 = (65535 * (data - min) / (max - min)).astype(np.uint16)
    return data_uint16

# subfunction definitions
def recad8(data, min, max):
    low_values_indices = data < min
    data[low_values_indices] = min
    large_values_indices = data > max
    data[large_values_indices] = max
    data_uint8 = (255 * (data - min) / (max - min)).astype(np.uint8)
    return data_uint8



# subfunction for multiprocessing - try with memory map
def readblock(info):
    # filename is the third argument
    offsetlines = info[0]
    myblocklines = info[1]
    nx = info[3]
    ny = info[4]
    mm = np.memmap(info[2], np.float32, 'r', 0, (nx, ny, offsetlines+myblocklines), 'F')
    low = info[6]
    high = info[7]
    # prepare the output 
    if info[5]=='8bit':
        data = np.empty((nx, ny, myblocklines), dtype=np.uint8)
    else:
        data = np.empty((nx, ny, myblocklines), dtype=np.uint16)
    #read a chunk and recad
    for zz in range(offsetlines, (offsetlines+myblocklines)):
        chunk = np.float32(mm[:,:,zz] * 1.)
        if info[5]=='8bit':
            chunk = recad8(chunk, low, high)
        else:
            chunk = recad16(chunk, low, high)
        data[:,:,zz-offsetlines] = chunk
    # remove the memmap object  
    del mm
    print('DONE BLOCK %d - %d' % (info[0], info[0]+info[1])          )
    return data


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Launch recad (32 -> 8 or 16 bit) of reconstructed volumes")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-a", "--limitlow", help="recad limit low", default=None, type=float)
    parser.add_argument("-z", "--limithigh", help="recad limit high", default=None, type=float)
    parser.add_argument("-f", "--format", help="convert to 8bit or 16bit [8bit]", default='8bit', type=str)
    parser.add_argument("-s", "--suffix", help="suffix on .vol filenames _pag/[none]", default='', type=str)
    parser.add_argument("-n", "--ncores", help="multiprocess with how many cores [8]", default=8, type=int)
    parser.add_argument("-r", "--remove", help="remove the .vol after recad [False]/True", default=False, type=testbool)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call recad_multi_all with these arguments
    recad_multi_all(args.scanname, low=args.limitlow, high=args.limithigh, format=args.format, suffix=args.suffix, ncores=args.ncores, remove=args.remove, interactive=args.interactive)

    # ----------------------------------------------------- #
