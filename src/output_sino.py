# python function to check paganin parameters
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# could add that if no ROI is selected, the roi from the par file is used

import argparse
import par_tools
import os

def output_sino(scanname, off=False, interactive=True):

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if interactive:
        off = input("Sinogram mode: Output sinograms(y) or volumes(n) [y]/n : ")
        off = True if off.lower() in ["n", "no", 'false'] else False

    if not off:
        print('Will output sinograms.  Disactivate sinograms to reconstruct volumes')
        pars = par_tools.par_mod(pars,'OUTPUT_SINOGRAMS', 'YES')
    else:
        print('Disactivating sinograms to reconstruct volumes')
        pars = par_tools.par_mod(pars,'OUTPUT_SINOGRAMS', 'NO')
    par_tools.par_write(parname, pars)
    return


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Export sinograms")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-o", "--off", help="switch off sinos for normal volume reconstruction [False]/True", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    output_sino(args.scanname, off=args.off, interactive=args.interactive)

    # ----------------------------------------------------- #
