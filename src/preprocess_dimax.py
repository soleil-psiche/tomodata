
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# if no scanname given, user select folder

# handle either 32 bit edfs or 16 bit hdf5 - current options
# use either in ipython or from outside for batch processing

# convert 32 to 16 bit for pyHST, median references, mean darks

# this version is for the acquisition with no Init allowed on the camera, so all image names are the same ('orca')

# should get rid of the fast option
# add an on the fly mean image
# is reading with multiprocess faster?

from tkinter import filedialog
from fabio.edfimage import edfimage
edf = edfimage()
import glob
import sys
import numpy as np
import os
import h5py
import argparse
from scipy.ndimage.filters import convolve1d
import par_tools
from scipy import signal
from view_only import view_only
import maxime_tifffile as tif

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def preprocess_dimax(scanname=None, spooldir="", bin=False, interactive=True):
    # preprocess... in interactive mode with no scanname, format tells where to start looking for data
    # otherwise, it's a choice

    if interactive:
        import pylab
        pylab.ion()

    # get the beamline default spool directory
    if spooldir == "":
        spooldir = beamline['DEFAULT_SPOOL_DIRECTORY'][0]
    # get the scanname interactively
    if scanname == None:
        firsttifname = filedialog.askopenfilename(initialdir=spooldir, title='open one of the tif files')
        if len(firsttifname) == 0: # we cancelled
            return firsttifname
        else:
            firstname = firsttifname.split(os.sep)[-1]
            spooldir = firsttifname[0:firsttifname.rfind(os.sep)]
            # get the name
            if firstname.find('@')==-1:
                nameroot = firstname.split('.')[0]
            else:
                nameroot = firstname.split('@')[0]

    filelist = glob.glob(spooldir + os.sep + nameroot + '*.tif')
    nfiles = len(filelist)
    # these go name.tif, name@0001.tif, etc
    # all 2GB expect the last one
    # read the first one
    data = tif.imread(spooldir + os.sep + nameroot + '.tif')
    xsize = data.shape[2]
    ysize = data.shape[1]
    nperfile = data.shape[0]

    step = input('What is the angular rotation step? [0.1882] for part A, 0.2 part B : ')
    step = 0.1882 if step == '' else float (step)
    nproj = int(np.ceil(180. / step))
    interval = input('Interval between tomos in number of projections if not continuous ? [auto = %d] : ' % nproj)
    interval = nproj if interval == '' else int(interval)

    nscans = nperfile * nfiles / interval
    nscans = int(np.floor(nscans))
    print('will unpack %d tomos' % nscans)

    #bin = input('bin the images 2x2 ? y/[n] : ')
    #bin = True if bin.lower() in ['y', 'yes', 'true', '1'] else False

    masterparname = filedialog.askopenfilename(initialdir=os.getcwd(), title='open a parfile from this sample')
    masterpars = par_tools.par_read(masterparname)
    # update the import parameters in master pars
    masterpars = par_tools.par_mod(masterpars, "NUM_LAST_IMAGE", int(nproj-1))
    masterpars = par_tools.par_mod(masterpars, "ANGLE_BETWEEN_PROJECTIONS", step)
    masterpars = par_tools.par_mod(masterpars, "ANGLE_OFFSET", 0.0, "dimax series")
    if bin:
        print("account for binning in par file")
        pixsize = float(masterpars['IMAGE_PIXEL_SIZE_1'][0]) * 2 # for binninig
        masterpars = par_tools.par_mod(masterpars, 'IMAGE_PIXEL_SIZE_1', pixsize, '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'IMAGE_PIXEL_SIZE_2', pixsize, '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'NUM_IMAGE_1', int(xsize/2), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'NUM_IMAGE_2', int(ysize/2), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'NUM_LAST_IMAGE', (nproj-1))
        masterpars = par_tools.par_mod(masterpars, 'ROTATION_AXIS_POSITION', float(xsize/4), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_1', int(xsize/2), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_2', int(xsize/2), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_3', int(ysize/2), '(binned)')
    else:
        print("No binning in par file")
        masterpars = par_tools.par_mod(masterpars, 'NUM_IMAGE_1', int(xsize), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'NUM_IMAGE_2', int(ysize), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'NUM_LAST_IMAGE', (nproj-1))
        masterpars = par_tools.par_mod(masterpars, 'ROTATION_AXIS_POSITION', float(xsize/2), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_1', int(xsize), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_2', int(xsize), '(binned)')
        masterpars = par_tools.par_mod(masterpars, 'END_VOXEL_3', int(ysize), '(binned)')
    masterpars = par_tools.par_mod(masterpars, 'FF_NUM_LAST_IMAGE', nproj)
    masterpars = par_tools.par_mod(masterpars, 'FF_FILE_INTERVAL', nproj)

    # references
    lastscanrefs = input('Get reference images from last tomo? y/[n] : ')
    lastscanrefs = True if lastscanrefs.lower() in ['y', 'yes', 'true', '1'] else False

    # references
    if lastscanrefs: # make references from the last scan
        # the last 'interval' images (in the last or last two tif files) include references        
        tmpB = tif.imread(spooldir + os.sep + nameroot + '@%04d.tif' % (nfiles-1))
        profile = tmpB[:, int(ysize/2), int(xsize/2)]
        lenfinal = len(profile)
        if tmpB.shape[0] < nproj:
            tmpA = tif.imread(spooldir + os.sep + nameroot + '@%04d.tif' % (nfiles-2))
            profileA = tmpA[:, int(ysize/2), int(xsize/2)]
            profile = np.concatenate((profileA, profile))
            tmpB = np.concatenate((tmpA, tmpB), 0)
        pylab.figure()
        pylab.plot(profile)
        pylab.title('intensity profile')
        # get ref position
        ndxref = input('give ndx of ref/bright images : ')
        ndxref = int(ndxref)
        ref = np.uint16(np.median(tmpB[ndxref:(ndxref+21), :, :], 0))
        print('copy a dark .edf from a previous scan')
        darkname = filedialog.askopenfilename(title='copy a dark .edf')
        dark = edf.read(darkname).data
    else: # take references from the master scan
        master_edfdir = masterparname[0:masterparname.rfind('.')] + '_edfs'
        ref = edf.read(master_edfdir + os.sep + 'ref000000.edf').data
        dark = edf.read(master_edfdir + os.sep + 'dark.edf').data
    if bin and (ref.shape[0]==2000):
        print("apply binning to refs")
        ref = np.uint16(ref.reshape(1000, 2, 1000, 2).mean(3).mean(1))
        dark = np.uint16(dark.reshape(1000, 2, 1000, 2).mean(3).mean(1))

    nameout = nameroot + '_dimax'

    for scan in range(int(nscans)):
        scanname = '%s_%02d' % (nameout, scan)
        # make folders
        if not os.path.exists(scanname):
            os.mkdir(scanname)
        if not os.path.exists(scanname+os.sep+scanname+"_edfs"):
            os.mkdir(scanname+os.sep+scanname+"_edfs")
        # write the par file
        # update the par file to reflect the new location
        expdir = os.getcwd() + os.sep + scanname
        parname = scanname + os.sep + scanname + ".par"
        pars = par_tools.par_mod(masterpars, "OUTPUT_FILE", expdir + os.sep + scanname + "_slice.vol")
        pars = par_tools.par_mod(pars, "FILE_PREFIX", expdir + os.sep + scanname + "_edfs" + os.sep + scanname)
        pars = par_tools.par_mod(pars, "BACKGROUND_FILE", expdir + os.sep + scanname + "_edfs" + os.sep + "dark.edf")
        pars = par_tools.par_mod(pars, "FF_PREFIX", expdir + os.sep + scanname + "_edfs" + os.sep + "ref")
        if np.mod(scan,2)==0:
            pars = par_tools.par_mod(pars, "ANGLE_OFFSET", 0.0)
        else:
            pars = par_tools.par_mod(pars, "ANGLE_OFFSET", 180.0)

        par_tools.par_write(parname, pars)
        # copy refs and darks
        #os.system('cp %s/ref* %s/dark* %s/%s_edfs/' % (master_edfdir, master_edfdir, scanname, scanname))
        edf.data = ref
        edf.write('%s/%s_edfs/ref000000.edf' % (scanname, scanname))
        edf.write('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, nproj))        
        edf.data = dark
        edf.write('%s/%s_edfs/dark.edf' % (scanname, scanname))
        

    # export the first one
    # this must assume that nproj > number of images in a multi tif
    count = 0
    scan = 0
    scanname = '%s_%02d' % (nameout, scan)
    print("exporting %s" % scanname)
    for ii in range(data.shape[0]):
        im = data[ii, :, :]
        if bin:
            im = np.uint16(im.reshape(int(ysize/2), 2, int(xsize/2), 2).mean(3).mean(1))
        edf.data = im
        edf.write(scanname+os.sep+scanname+"_edfs"+os.sep+'%s%06d.edf' % (scanname, count))
        count += 1
        if count == interval:
            # reached the end of a dataset
            count = 0 # reset to zero
            scan += 1 # increment the scan number
            scanname = '%s_%02d' % (nameout, scan)
            print("exporting %s" % scanname)

        sys.stdout.write(6*"\b"+("%d" % count).rjust(6))
        sys.stdout.flush()

    stop = False
    for jj in range(1, nfiles):
        if not stop:
            data = tif.imread(spooldir + os.sep + nameroot + '@%04d.tif' % jj)
            for ii in range(0, data.shape[0]):
                if not stop:
                    im = data[ii, :, :]
                    if bin:
                        im = np.uint16(im.reshape(1000, 2, 1000, 2).mean(3).mean(1))
                    edf.data = im
                    edf.write(scanname+os.sep+scanname+"_edfs"+os.sep+'%s%06d.edf' % (scanname, count))
                    count += 1        
                    sys.stdout.write(6*"\b"+("%d" % count).rjust(6))
                    sys.stdout.flush()

                    if count == interval:
                        # reached the end of a dataset
                        count = 0 # reset to zero
                        scan += 1 # increment the scan number
                        if scan == nscans:
                            print("reached the end...")
                            stop = True
                        else:
                            scanname = '%s_%02d' % (nameout, scan)
                            print("exporting %s" % scanname)

    print("DONE :-)")
    return scanname

############## PREPROCESS HDF5 DATA ###############

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Tomography data preprocessing tool for PyHST")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-b", "--bin", help="Bin projections 2x2", default=False, type=testbool)
    parser.add_argument("-d", "--spooldir", help="Where to look for the raw data", default="", type=str)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    preprocess(args.scanname, accfac=args.acc, format=args.format, bin=args.bin, spooldir=args.spooldir, interactive=args.interactive)

    # ----------------------------------------------------- #
