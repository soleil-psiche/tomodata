
# use ordered dict to preserve order
from collections import OrderedDict
import os

def par_read(parname):
    """ par_read(par_filename) - read a par file as a python dict """

    # read the parfile
    f = open(parname, 'rt')
    lines = f.readlines()
    f.close()

    # unpack into dictionary
    pars = OrderedDict()

    for line in lines:
        
        # get the key
        parts = line.split("=")
        
        if len(parts)==1:
            # this is a comment - preserve it
            # strip the new line
            commentline = parts[0].strip("\n")
            pars.update({commentline:[None, None]})
            
        else:
            # key = something case
            # strip off white spaces from the key
            key = parts[0].strip(" ")
            
            # do we also have a comment?
            parts2 = parts[1].split("#")
            
            if len(parts2)==1:
                # no comment case
                val = parts2[0].strip(" ").strip("\n")
                comment = None

            else: 
                # value and a comment
                val = parts2[0].strip(" ")
                comment = parts2[1].strip("\n").strip(" ")
        
            pars.update({key:[val, comment]}) 

    return pars

def par_mod(pars, key, val, newcomment=None):
    """given a dictionary, update with the new value
    Preserves comment lines, but you can add more info """
    
    if key in pars.keys():
        origcomment = pars[key][1]
    else:
        origcomment = None

    if (newcomment != None) and (origcomment != None):
        comment = newcomment + ' ' + origcomment
    elif newcomment != None:
        comment = newcomment   
    else:
        comment = origcomment

    pars.update({key:[val, comment]})

    return pars

def par_remove(pars, key):
    """removes the item from the par disctionary """
    if key in pars.keys():
        pars.pop(key)
    return pars


def par_write(parname, pars):
    """write the PyHST parfile from the par dictionary"""

    f = open(parname, 'wt')
    items = list(pars.items())
    
    for item in items:
        # write the key = value 
        if item[1][0] != None:
            f.write(item[0] + " = " + str(item[1][0]))
            if item[1][1] != None:
            # add the comment if present
                f.write(" # " + str(item[1][1]))
        else:
            # extra new line for comment lines
            f.write("\n" + item[0])
        f.write("\n")
        
    f.close()
    
def par_change_image_dir(pars, new_image_dir):
    """change path of the image files"""
    
    new_image_dir = new_image_dir.rstrip(os.sep)
    # images
    prefix = pars["FILE_PREFIX"][0].split(os.sep)[-1]
    pars = par_mod(pars, "FILE_PREFIX", new_image_dir+os.sep+prefix)
    # ref
    prefix = pars["FF_PREFIX"][0].split(os.sep)[-1]
    pars = par_mod(pars, "FF_PREFIX", new_image_dir+os.sep+prefix)
    # dark
    prefix = pars["BACKGROUND_FILE"][0].split(os.sep)[-1]
    pars = par_mod(pars, "BACKGROUND_FILE", new_image_dir+os.sep+prefix)
    
    return pars


