import multiprocessing as mp
import argparse
import par_tools
import os
import numpy as np
from fabio.edfimage import edfimage
edf = edfimage()
import time

def remove_spikes(scanname, ncores=16, keep_originals=True, interactive=True):
    '''
    Stack direction median filter over three images to remove spikes 
    Adapted from Maxime, 27032019_correct_spikes.py
    '''

    if interactive:
        tmp = input('Keep original images? [y]/n : ')
        keep_originals = False if tmp.lower() in ['n', 'no', 'false'] else True

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()
    sizex = int(pars['NUM_IMAGE_1'][0])
    sizey = int(pars['NUM_IMAGE_2'][0])
    nproj = int(pars['NUM_LAST_IMAGE'][0])

    # make a directory for new images
    outdir = '%s/%s_edfs_bis' % (scanname, scanname)
    origdir = '%s/%s_edfs_orig' % (scanname, scanname)
    edfdir = '%s/%s_edfs' % (scanname, scanname)
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    p = mp.Pool(ncores)
    # job details
    joblength = int(nproj / ncores)
    starts = np.arange(0, nproj-joblength+1, joblength)
    ends = starts + joblength
    ends[-1] = nproj+1 # make sure we get the last image
    n = len(starts)
    myinfo = zip([scanname]*n, starts, ends, [sizex]*n, [sizey]*n)
    # read the images
    print("Filtering using multiprocess with %d processes to go faster" % ncores)
    t0 = time.time()
    blocks = p.map(filter_images, myinfo)
    print("close the pool")
    # close the pool?
    p.close()
    t1 = time.time()
    print('time was %0.1f seconds' % (t1-t0))

    # rename / delete if deleting
    # copy over any other files from the edfs folder
    os.system('cp %s/ref*.edf %s' % (edfdir, outdir))
    os.system('cp %s/dark.edf %s' % (edfdir, outdir))
    os.system('cp %s/mean.edf %s' % (edfdir, outdir))
    os.system('cp %s/*ff.edf %s' % (edfdir, outdir))

    if keep_originals:
        # rename the original folder
        os.system('mv %s %s' % (edfdir, origdir))
    else:
        # delete the original folder
        os.system('rm -rf %s' % edfdir)
    # rename the out folder to activate
    os.system('mv %s %s' % (outdir, edfdir))

# to be used by multiprocess
def filter_images(info):
    # info : scanname, first, last
    scanname = info[0]
    first = info[1]
    last = info[2]    
    xsize = info[3]
    ysize = info[4]
    stack = np.zeros((3, ysize, xsize), np.uint16)
    # prefill the stack
    for ii in range(first-1, first+1): # first two images
        if ii==-1:
            next = '%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, 0) # image to add to stack
        else:
            next = '%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, ii) # image to add to stack
        stackndx = np.mod(ii, 3) # where in the stack
        stack[stackndx, :, :] = np.uint16(edf.read(next).data)

    for ii in range(first, last):
        next = '%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, (ii+1)) # image to add to stack
        stackndx = np.mod(ii, 3) # where in the stack
        if not os.path.exists(next): # case of final image
            next = '%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, ii)
        stack[stackndx, :, :] = np.uint16(edf.read(next).data)
        med = np.uint16(np.median(stack, 0))
        edf.data = med
        edf.write('%s/%s_edfs_bis/%s%06d.edf' % (scanname, scanname, scanname, ii))
        
def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Remove spikes with a 3 projection stackwise moving median filter")
    parser.add_argument("scanname", help="The name of the scan you want to treat", default=None)
    parser.add_argument("-n", "--ncores", help="Number of cores to use [16]", default=16, type=int)
    parser.add_argument("-k", "--keep_originals", help="Keep original images False/[True]", default=True, type=testbool)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    remove_spikes(args.scanname, ncores=args.ncores, keep_originals=args.keep_originals, interactive=args.interactive)

    # ----------------------------------------------------- #
