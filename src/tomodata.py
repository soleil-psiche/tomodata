######## edit by Joel ########
### keep this, because it could be useful
# thanks Joel
# to use - at command prompt type n, space, and then tab to complete

import readline
from glob import glob
from sys import stdout

def str_root(strs):
    nc = min(len(x) for x in strs)
    t = [len(set(x)) for x in zip(*[x[:nc] for x in strs])]
    nc = 0
    for x in t:
        if x == 1:
            nc += 1
        else:
            break
    if nc > 0:
        return strs[0][:nc]
    else:
        return ''

def show_prompt(info=None):
    if info is not None:
        print
        print(info)
    print(prompt + readline.get_line_buffer(),)
    readline.redisplay()

def completer(*args):
    lb = readline.get_line_buffer()
    if not lb.startswith('n '):
        return
    name = lb[2:]
    choices = glob('*')
    choices = [c for c in choices if c.startswith(name)]
    if len(choices) == 1:
        readline.insert_text(choices[0][len(name):])
    elif len(choices) > 1:
        root = str_root(choices)
        if len(root) > len(name):
            readline.insert_text(root[len(name):])
        else:
            show_prompt('\n'.join(choices))

readline.parse_and_bind('tab: complete')
readline.set_completer(completer)

###### end edit by Joel ######


# Wrapper function to handle all preprocessing tasks
# Change name to tomodata.py, as this is what we call it


# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

# need to import all the sub functions
# preprocessing different types of scan
from preprocess_all import preprocess
from preprocess_spiral import preprocess_spiral
from preprocess_fast import preprocess_fast
from preprocess_dimax import preprocess_dimax
# basic functions
from rotation_axis import rotation_axis
from rotation_axis_by_reconstruction import rotation_axis_by_reconstruction
from paganin import paganin
from roi import roi
from auto_crop import auto_crop
from double_ff import double_ff
from half_acq import half_acq
from angle_offset import angle_offset
from pentezone import pentezone
# movement corrections
from movement_correction import movement_correction
from mercedes_movement_correction import mercedes_movement_correction
# special things
from zero_clip import zero_clip
from double_ff_special import double_ff_special
from nonconstant_rings import nonconstant_rings
from utopec import utopec
from remove_spikes import remove_spikes
# reconstruction
from reconstruct_and_view import reconstruct_and_view
from launch_reconstruction import launch_reconstruction
from recad_multi_memmap import recad_multi_all
from reconstruct_all import reconstruct_all
from live_preview2 import live_preview
# utilities
from select_name_only import select_name_only
from copy_parameters import copy_parameters
from view_only import view_only
# non tomo things
import os
import sys
import pylab
pylab.ion()

scanname = ""
units = "mm"
myspooldir = ""

# define all the options
def do_quit():
    print("quitting")
    sys.exit()

def do_help():
    print("------------------------------------------------------------")
    if scanname != "":
        print("You are working on dataset %s at %s" % (scanname, beamline['BEAMLINE'][0]))
    else:
        print("No dataset selected (use s to select)")
    print("Options are: ")
    print("----------------------   SELECT    -------------------------")
    print("(s) select : select and preprocess a dataset from the spool")
    print("(n) nselect : select a dataset from the local directory")
    print("----------------------   PROCESS   -------------------------")
    print("(a) rotation axis : rotation axis options")
    print("(b) rotation axis : new rotation axis by reconstruction")
    print("(p) paganin : set paganin phase parameters")
    print("(r) roi : reconstruction roi options")
    print("(f) double flatfield : correction for ring artefacts")
    print("---------------------- RECONSTRUCT -------------------------")
    print("(v) view  : reconstruct and view a slice with current parameters")
    print("(l) launch : launch reconstruction of volume")
    print("(x) recad : recad of reconstructed volume")
    print("----------------------    OTHER    -------------------------")
    print("(k) kill : close all figures")
    print("(q) quit")
    print("(h) display this help")
    print("(hh) display extended help - more options")
    print("------------------------------------------------------------")

def do_long_help():
    do_help()
    print("____________________________________________________________")
    print("---------------------    EXTRA SELECT  ---------------------")
    print("(sp)   select spiral : select a spiral scan from the spool")
    print("(sf)   select fast : select a scan from a fast series from the spool")
    print("(sd)   select dimax : select a dimax ring buffer series from the spool")
    print("--------------------    EXTRA PROCESS   --------------------")
    print("(m)    movement correct : calculate a movement correction")
    print("(mb)   mercedes : Wolfgang Ludwig\'s Mercedes movement correction")
    print("(d)    half acquisition : manage offset rotation axis scan")
    print("(r)    ROI : manage region of interest in reconstruction")
    print("(crop) crop : crop the reconstruction size")
    print("(w)    angle : adjust the rotation angle offset")
    print("(c)    copy : copy parameters from another scan")
    print("(z)    zeroclip : adjust the zeroclip in the reconstruction")
    print("(pz)   pentezone : adjust the pentezone for offset axis acquisitions")
    print("(y)    UToPEC : Remove partially truncated projections")
    print("(rs)   remove spikes : moving median filter projections")
    print("(nc)   nonconstant rings : ring correction for difficult rings")
    print("(ffs)  flatfield special : double flatfield using forward projection (slow!)")
    print("------------------    EXTRA RECONSTRUCT   ------------------")
    print("(A)    All : batch processing options")
    print("(live) live preview : auto processing options")
    print("--------------------    EXTRA DISPLAY   --------------------")
    print("(u)    units : toggle between pixels and mm units")
    print("____________________________________________________________")


def do_movcor():
    print("#### Movement correction ####")
    movement_correction(scanname)

def do_mercedes():
    print("#### Mercedes movement correction ####")
    mercedes_movement_correction(scanname)

def do_preprocess():
    print("#### Preprocessing ####")
    scanname = ""
    newscanname = preprocess(spooldir=myspooldir)
    if len(newscanname) != 0:
        print("Selected dataset is %s" % newscanname)
        scanname = newscanname
    else:
        print("No dataset selected"      )
    return scanname

def do_rotation():
    print("#### Rotation axis options ####")
    rotation_axis(scanname)

def do_rotation_by_reconstruction():
    print("#### Rotation axis by reconstruction ####")
    rotation_axis_by_reconstruction(scanname)

def do_paganin():
    print("#### Paganin options ####")
    paganin(scanname)

def do_roi():
    print("#### ROI options ####")
    roi(scanname)

def do_double_ff():
    print("#### Double flatfield correction ####")
    double_ff(scanname)

def do_half_acq():
    print("#### Half acquisition options ####")
    half_acq(scanname)

def do_reconstruct_slice():
    print("#### Reconstruct a slice ####")
    reconstruct_and_view(scanname)

def do_launch_reconstruction():
    print("#### Launch reconstruction of volume ####")
    launch_reconstruction(scanname)

def do_reconstruct_all():
    print("#### Reconstruct ALL volumes ####")
    reconstruct_all()

def do_live_preview():
    print("#### Live preview! ####")
    live_preview()

def do_recad():
    print("#### Launch reconstruction of volume ####")
    recad_multi_all(scanname)

def do_copy():
    print("#### Copy parameters from another dataset ####")
    copy_parameters(scanname)

def do_ff_special():
    print("#### double ff special... ####")
    double_ff_special(scanname)

def do_angle():
    print("#### Change angle offset ####")
    angle_offset(scanname)

def do_zeroclip():
    print("#### Change zero clip value ####")
    zero_clip(scanname)

def do_pentezone():
    print("#### Change pentezone value ####")
    pentezone(scanname)

def do_spiral():
    print("#### Preprocess a spiral dataset ####")
    scanname = ""
    newscanname = preprocess_spiral(spooldir=myspooldir)
    if len(newscanname) != 0:
        print("Selected dataset is %s" % newscanname)
        scanname = newscanname
    else:
        print("No dataset selected"      )
    return scanname

def do_fast():
    print("#### Preprocess a fast dataset ####")
    scanname = ""
    newscanname = preprocess_fast(spooldir=myspooldir)
    if len(newscanname) != 0:
        print("Selected dataset is %s" % newscanname)
        scanname = newscanname
    else:
        print("No dataset selected"      )
    return scanname

def do_dimax():
    print("#### Preprocess Dimax datasets ####")
    scanname = ""
    newscanname = preprocess_dimax(spooldir=myspooldir)
    if len(newscanname) != 0:
        print("Selected dataset is %s" % newscanname)
        scanname = newscanname
    else:
        print("No dataset selected"      )
    return scanname

## Attention, edit by Joel
#def do_select_name_only():
def do_select_name_only(sname):
    print("####  Pick a dataset from the current directory  ####")
    scanname = ""
    if sname.strip() == '':
        newscanname = select_name_only()
    else:
        newscanname = sname
    if len(newscanname) != 0:
        print("Selected dataset is %s" % newscanname)
        scanname = newscanname
        view_only(scanname, units)
    else:
        print("No dataset selected"      )
    return scanname

def do_units(units):
    if units == 'mm':
        units = 'pixel'
        print("Will display slices with pixel units")
    else:
        units = 'mm'    
        print("Will display slices with millimeter units")
    view_only(scanname, units)
    return units

def do_utopec():
    print("####  Remove partially obscured projections  #### ")
    utopec(scanname)

def do_remove_spikes():
    print("####  Remove spikes by moving median filtering projections  #### ")
    remove_spikes(scanname)

def do_nonconstant():
    print("####  Correct difficult rings  #### ")
    nonconstant_rings(scanname)

def do_auto_crop():
    print("####  Crop the reconstruction  #### ")
    auto_crop(scanname)


# before starting, check that we are in a good directory - i.e. on /psichestockage, not /home
curdir = os.getcwd()
pieces = curdir.split(os.sep)
expname = pieces[-1]
# test - is the start of the curdir out reconstruction directory?
if curdir[0:len(beamline['RECONSTRUCTION_PATH'][0])] != beamline['RECONSTRUCTION_PATH'][0]:
    print("\n\n\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("  You should launch tomodata from your experiment directory")
    print("  Usually %s/.../Name_MMYY" % beamline['RECONSTRUCTION_PATH'][0])
    print("  Currently in : %s" % curdir)
    print("  quitting..."    )
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n")
    sys.exit()
    print("NOT QUITTING, FIX THIS LATER")
else:
    print("\n\n\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    print("  tomodata reconstruction for experiment %s on %s" % (expname,  beamline['BEAMLINE'][0]))
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n")
myspooldir = beamline['DEFAULT_SPOOL_DIRECTORY'][0] + curdir[len(beamline['RECONSTRUCTION_PATH'][0]):]

# while loop for processing
# show the help first
do_help()
option = ""

prompt = 'option: s|n|a|b|p|r|f|v|l|x|k|q|[h=help] : '

while option!='q':

    # get input
    option = input(prompt)

    # use input
    if option in ['', 'h']:
        do_help()
    elif option == "hh":
        do_long_help()
    elif option == "s":
        scanname = do_preprocess()
    elif option == 'nc':
        do_nonconstant()


    ## edit by Joel
    #elif option == "n":
    #    scanname = do_select_name_only()
    elif option.startswith("n"):
        scanname = do_select_name_only(option[1:].strip())


    elif option == "m":
        do_movcor()
    elif option == "mb":
        do_mercedes()
    elif option == "a":
        do_rotation()
    elif option == "b":
        do_rotation_by_reconstruction()
    elif option == "p":
        do_paganin()
    elif option == "r":
        do_roi()
    elif option == "f":
        do_double_ff()
    elif option == "d":
        do_half_acq()
    elif option == "v":
        do_reconstruct_slice()
    elif option == "l":
        do_launch_reconstruction()
    elif option == "x":
        do_recad()
    elif option == "w":
        do_angle()
    elif option == "c":
        do_copy()
    elif option == "z":
        do_zeroclip()
    elif option == "pz":
        do_pentezone()
    elif option == "k":
        print("Closing all figures...")
        pylab.close('all')
    elif option == "q":
        do_quit()
    elif option == "sp":
        scanname = do_spiral()
    elif option == "sf":
        scanname = do_fast()
    elif option == "sd":
        scanname = do_dimax()
    elif option == "ffs":
        do_ff_special()
    elif option == "A":
        do_reconstruct_all()
    elif option == "live":
        do_live_preview()
    elif option == 'u':
        units = do_units(units)
    elif option == 'y':
        do_utopec()
    elif option == 'rs':
        do_remove_spikes()
    elif option == 'crop':
        do_auto_crop()
    else:
        print("did not recognise option: %s" % option)
        do_help()
    

