import argparse
import par_tools
import os
from edf_read_dirty import vol_read_dirty
from fabio.edfimage import edfimage
edf = edfimage()
import numpy as np
import sys
import time
import fitting
import call_pyhst
import reconstruct_and_view
from skimage import morphology
import glob
# globals for gui
global new_ax, fa, w1, stack, cur_ax, fh, mymin, mymax, pylab, shifts


def update(toto):
    global fa, w1, stack, mymin, mymax, pylab
    pylab.axes(fa)
    ndx = int(np.round(w1.val))
    pylab.imshow(stack[:,:,ndx])
    pylab.title('image %d' % ndx)
    pylab.clim(mymin, mymax)

def selected(toto):
    global new_ax, w1, fh, pylab, shifts
    val = shifts[int(np.round(w1.val))]
    new_ax = cur_ax-val
    print("Selection suggests that rotation axis should be %0.2f" % new_ax)
    print("Hit enter to continue")
    pylab.close(fh)

def rotation_axis_by_reconstruction(scanname, slicendx=0, timeout=0, mode='std', justrun=False, justread=False, wide=False, interactive=True):
    '''
    To start with, keep this separate... eventually need to update the regular version
    Make a sinogram (from utopec.py)
    From this sinogram, build a set of images in which the rotation centre is shifted relative
    Write these images, and a parfile
    Launch the small reconstruction
    Read the result, and do something with it
    Interestingly - for the carbon fibre it works beautifully
    For other things - taking the gradient of the image helps - but often there's no maximum, but rather a 
    step around the right value
    And in half acquisition, a very strong ring opens up at once you go past the good position.
    There is a very small peak (in the std / std(grad)) at the right position, but the bad ring dominates everything.
    Need to renormalise at least: std/mean is cleaner.
    The ring might actually be something we can identify - the step is about the right place.
    Otherwise, if one can avoid the ring the std(grad) signal then works...
    One could stay inside the ring for half acq, but it depends on the image having some structure there.
    Main remaining issue is the cropping for the half acquisition case... and eventually the ROI case
    Could extend the range covered by doing some steps of 2 pixels
    justread - dont reconstruct, just read existing data
    justrun - dont apply results, just run the reconstruction for later
    mode = 'std' / 'edges'  - edges tries to make sharp edges, std maximises std dev of gradients
    '''
    global new_ax, fa, w1, stack, cur_ax, fh, mymin, mymax, pylab, shifts

    if interactive:
        import pylab
        pylab.ion()
    else:
        print("not importing pylab as not interactive")

    # extended edges
    if wide:
        shifts = np.arange(-40, 41, 2)
        crop = 40 
    else:
        shifts = np.arange(-20, 21, 1)
        crop = 20 
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    cur_ax = float(pars["ROTATION_AXIS_POSITION"][0])
    expdir = os.getcwd()
    xsize = int(float(pars['NUM_IMAGE_1'][0]))
    zsize = int(float(pars['NUM_IMAGE_2'][0]))

    # the eventual output vol name
    output_vol = expdir + os.sep + scanname + os.sep + "tmp" + os.sep + "axis.vol"

    # prepare a tmp directory
    if not os.path.exists(scanname + os.sep + 'tmp'):
        os.mkdir(scanname + os.sep + 'tmp')
    # tmp parfile
    tmpparname = expdir + os.sep + scanname + os.sep + "tmp" + os.sep + "tmp.par"
    tmppars = pars.copy()

    if os.path.exists(output_vol + '.info') & interactive:
        tmp = input('An axis.vol file exists! Just read this file? [y]/n : ')
        justread = True if tmp.lower() in ['', 'y', 'yes', 'true', '1'] else False
        if justread:
            # get the cur_axis from the existing tmp par file
            tmppars = par_tools.par_read(tmpparname)
            cur_ax = float(tmppars["ROTATION_AXIS_POSITION"][0]) + crop # +30 to account for crop
            print("read rotation axis position from the tmpparfile (%0.1f)" % cur_ax)

    # have we already made the tmp edfs?
    edflist = glob.glob(scanname + os.sep + 'tmp' + os.sep + '*.edf')
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    if justread:
        make_projections = False
    else:        
        make_projections = True
        if len(edflist) >= nproj:
            if interactive:       
                tmp = input('re-create the special rotation axis input data ? y/[n] : ')
                make_projections = True if tmp.lower() in ['y', 'yes', 'true', '1'] else False
            else:
                print("Re-creating special rotation axis input data")
                make_projections = True
    
    if make_projections:
        # make a sinogram of a slice
        midslice = int(zsize / 2)
        if interactive and (slicendx == 0):
            tmp = input('which slice to use ? [%d] : ' % midslice)
            zslice = midslice if tmp == '' else int(tmp)
        elif slicendx == 0:
            zslice = midslice
        else:
            zslice = int(slicendx)
        
        # remove any existing sinograms
        print('removing any existing sinograms')
        os.system('rm -f %s/*sinogram*edf' % scanname)
        pars = par_tools.par_mod(pars, 'OUTPUT_SINOGRAMS', 'YES')
        par_tools.par_write(parname, pars)
        print("build the sinogram")
        reconstruct_and_view.reconstruct_and_view(scanname, slicendx=zslice, timeout=timeout, interactive=False)
        # put the par file back to normal !
        pars = par_tools.par_mod(pars, 'OUTPUT_SINOGRAMS', 'NO')
        par_tools.par_write(parname, pars)

        # read the sinogram
        sinolist = glob.glob(scanname+os.sep+'*sinogram*edf')
        sino = edf.read(sinolist[0]).data * 1

        # build the shifted sinograms - assume +-15?
        sino2 = np.zeros((sino.shape[0], sino.shape[1], len(shifts)), np.float32)
        for ii in range(len(shifts)):
            sino2[:, :, ii] = np.roll(sino, shifts[ii], 1)

        # crop this to avoid the roll artefacts
        sino2 = sino2[:, crop:-crop,:]

        # write out these as little images
        for ii in range(sino.shape[0]):
            edf.data = sino2[ii, :, :].T
            edf.write(scanname + os.sep + 'tmp' + os.sep + 'tmp%06d.edf' % ii)
    

    if not justread:
        # prepare the tmp parfile
        tmppars = par_tools.par_mod(tmppars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + 'tmp' + os.sep + 'tmp')
        tmppars = par_tools.par_mod(tmppars, 'SUBTRACT_BACKGROUND', 'NO')
        tmppars = par_tools.par_mod(tmppars, 'CORRECT_FLATFIELD', 'NO')
        tmppars = par_tools.par_mod(tmppars, 'TAKE_LOGARITHM', 'NO')
        tmppars = par_tools.par_mod(tmppars, 'NUM_IMAGE_1', xsize-(2*crop)) # account for crop
        tmppars = par_tools.par_mod(tmppars, 'ROTATION_AXIS_POSITION', cur_ax-crop) # account for crop
        tmppars = par_tools.par_mod(tmppars, 'NUM_IMAGE_2', 41)
        tmppars = par_tools.par_mod(tmppars, 'START_VOXEL_3', 1)
        tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_3', 41) 
        tmppars = par_tools.par_mod(tmppars, 'ANGLE_OFFSET', 0.0) # try ensuring the characterisitic features don't more in fast series...
        # flatfield and paganin already dealt with in the sinogram, and not meaningful on this "volume"
        tmppars = par_tools.par_mod(tmppars, 'DO_PAGANIN', 0)
        tmppars = par_tools.par_remove(tmppars, 'DOUBLEFFCORRECTION')
        #tmppars = par_tools.par_mod(tmppars, 'DO_AXIS_CORRECTION', 'NO') # seems like if this exists we need to use it
        # output volume name
        tmppars = par_tools.par_mod(tmppars, 'OUTPUT_FILE', output_vol)
        par_tools.par_write(tmpparname, tmppars)
        
        # launch the reconstruction of the block
        sys.stdout.write('start block for axis... %0.2f to %0.2f \n' % (cur_ax-crop, cur_ax+crop))
        sys.stdout.flush()
        # use a helper function to treat multiple beamlines
        call_pyhst.call_pyhst(tmpparname, timeout)

    # and read the result
    stack = vol_read_dirty(output_vol)

    # use the standard deviation to find the max

    # need to crop this in a sensible way - 
    # to avoid the zero edges, and avoid the ring from the double acquisition.
    # this also depends if there is an ROI...
    # if non-acq and non ROI:
    #xsize = stack.shape[1]
    #xstart = int(xsize*0.15)
    #xend = int(xsize*0.85)
    #ysize = stack.shape[0]
    #ystart = int(ysize*0.15)
    #yend = int(ysize*0.85)
    #stack = stack[ystart:yend, xstart:xend, :]
    # special case - if only the edges of a sample are useful, then with no angle offset,
    # the blurring happens top and bottom
    if interactive:
        tmp = input('Which contrast mode to optimise [Y]:std / N:edges : ')
        mode = 'std' if tmp.lower() in ['', 'y', 'yes', 'true', '1'] else 'edges'
    
    std = np.zeros(41)
    if mode=='edges':
        xsize = stack.shape[1]
        xstart = int(xsize*0.45)
        xend = int(xsize*0.55)
        stack = stack[:, xstart:xend, :]       
        # this is a strip, containing the part where we see the rotation axis effect on the edges
        mask = stack.sum(2)!=0 # mask out zeros
        # need to erode this mask
        mask = morphology.binary_erosion(mask, np.ones((5, 5)))
        maskndx = np.nonzero(mask)
        edges = np.abs(np.gradient(stack.mean(2))[0]) * mask
        # threshold this
        a,b = np.histogram(edges[maskndx], 2000)
        # find the strongest 1.5 * 41 x 2 x (xsize*0.1) pixels
        c = np.cumsum(a)
        ndx = np.nonzero(c>(len(maskndx[0]) - 1.5*41*2*xsize*.1))[0][0]
        # these are the pixels to look at:
        zone = np.nonzero(edges>b[ndx])
        # use std of gray levels 
        for ii in range(41):
            im = stack[:,:,ii]
            std[ii] = im[zone].std()

    else:
        # use gradients 
        for ii in range(41):
            a = np.gradient(stack[:,:,ii])
            b = a[0]**2 + a[1]**2 # this is the modulus of the gradient... not sure that's right
            std[ii] = b.std()

        # get rid of some of the edge values...?
        std[0] = std[2]
        std[1] = std[2]
        std[-1] = std[-3]
        std[-2] = std[-3]



    ndx = np.nonzero(std==std.max())[0][0]
    if (ndx>1) & (ndx<39):
        # fit on three points around the maximum
        xdata = shifts[(ndx-1):(ndx+2)]
        print(ndx)
        print(xdata)
        print(std[(ndx-1):(ndx+2)])
        a,b = fitting.fit_poly(xdata, std[(ndx-1):(ndx+2)])
        shiftmax = -a[1] / (2*a[2])
        new_ax = cur_ax-shiftmax # reverse the sign here? - yes!
        print("std(grad(im)) suggests that rotation axis should be %0.2f" % new_ax)
        if interactive:
            # if interactive, plot the profile and the fit.
            pylab.figure()
            pylab.plot(shifts, std, 'rx')
            pylab.plot(xdata, b, 'r-')
            pylab.plot(shiftmax, a[0]+a[1]*shiftmax+a[2]*shiftmax**2, 'm*')


    else:
        print("failed to find a new axis - outside the +-%d30 pixel scan range?" % crop)
        new_ax = cur_ax


    # update the par file with this value
    if interactive:
        tmp = input('use this value? [y]/n : ')
        tmp = True if tmp.lower() in ['', 'y', 'yes', 'true'] else False
        if not tmp:
            
            print("Try interactively...")
            # open a figure
            fh = pylab.figure()
            fa = pylab.subplot2grid((24, 24), (0, 2), 20, 20)
            sa = pylab.subplot2grid((24, 24), (22, 0), 1, 20)
            ba = pylab.subplot2grid((24, 24), (22, 22), 2, 2)
            ysize,xsize,zsize = stack.shape
            myslice = stack[:,:,int(zsize/2)]
            mymin = myslice.mean()-(3*myslice.std())
            mymax = myslice.mean()+(3*myslice.std())
            pylab.axes(fa)
            pylab.imshow(myslice)
            pylab.gray()
            pylab.clim(mymin, mymax)


            w1 = pylab.Slider(ax=sa, label='Slide: ',valmin=0, valmax=zsize, valinit=20, valfmt='%0.1f')
            cid_w1 = w1.on_changed(update)

            b1 = pylab.Button(ax=ba, label='Select')
            cid_b1 = b1.on_clicked(selected)

            input()#block the command line during the GUI phase
            print("new_ax = " + str(new_ax))
            tmp = True
             

    else:
        if justrun:
            tmp = False # dont change, even if we found a new value
        else:
            tmp = True 
    if tmp:
        pars = par_tools.par_mod(pars, 'ROTATION_AXIS_POSITION', new_ax, 'was %f' % cur_ax)
        par_tools.par_write(parname, pars)
    else:
        print("not changing axis")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Find rotation axis by reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-m", "--mode", help="mode to optimise [std]/edges", default='std', type=str)
    parser.add_argument("-n", "--slicendx", help="slice to use", default=0, type=int)
    parser.add_argument("-j", "--just_run", help="Just run the reconstruction, dont change anything [False]/True", default=False, type=testbool)
    parser.add_argument("-r", "--read", help="Just read the current reconstruction, dont reconstruct [False]/True", default=False, type=testbool)
    parser.add_argument("-w", "--wide", help="Wide range (+-40 rather than +-20) [False]/True", default=False, type=testbool)
    parser.add_argument("-t", "--timeout", help="timeout for PyHST in seconds [0]", default=0, type=int)
    args = parser.parse_args()
    
    # now, call movement_correction with these arguments
    rotation_axis_by_reconstruction(args.scanname, slicendx=args.slicendx, timeout=args.timeout, mode=args.mode, justrun=args.just_run, justread=args.read, wide=args.wide, interactive=args.interactive)

    # ----------------------------------------------------- #



    
