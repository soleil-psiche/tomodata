

# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# handle 16 bit hdf5 

from tkinter import filedialog
from fabio.edfimage import edfimage
edf = edfimage()
import sys
import numpy as np
import os
import h5py
import argparse
import par_tools
import glob
import pylab
pylab.ion()

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def preprocess_spiral(scannameroot=None, dzpix=None, dxpix=None, spooldir='/nfs/spool1/FILETRANSFER/com-psiche/', skip=[], interactive=True):

    if interactive:
        # get the scanname interactively - all data saved in the flyscan-data spool on srv3
        # get the beamline default spool directory
        if spooldir == "":
            spooldir = beamline['DEFAULT_SPOOL_DIRECTORY'][0]
        if scannameroot == None:
            scandir = filedialog.askdirectory(initialdir=spooldir, title='double click into the spiral directory')
            if len(scandir) == 0: # we cancelled
                return scandir
            else:
                scannameroot = scandir.split(os.sep)[-1]
                spooldir = scandir[0:scandir.rfind(os.sep)]

    # get the list of scans
    fullscannames = glob.glob(spooldir + os.sep + scannameroot + os.sep + scannameroot + "_*")
    fullscannames = sorted(fullscannames)

    # read the first and second parfiles
    scanname0 = fullscannames[0].split(os.sep)[-1]
    parname = spooldir + os.sep + scannameroot + os.sep + scanname0 + os.sep + scanname0 + ".par"
    pars = par_tools.par_read(parname)
    scanname1 = fullscannames[1].split(os.sep)[-1]
    parname = spooldir + os.sep + scannameroot + os.sep + scanname1 + os.sep + scanname1 + ".par"
    pars1 = par_tools.par_read(parname)

    # guess the z step...
    if '#ptomotz' in pars.keys():
        g1 = (float(pars1['#ptomotz'][0]) - float(pars['#ptomotz'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
        key1 = '#ptomotz'
    elif '#mtomotz' in pars.keys():
        g1 = (float(pars1['#mtomotz'][0]) - float(pars['#mtomotz'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
        key1 = '#mtomotz'
    else:
        g1 = 0
    if '#ptz' in pars.keys():
        g2 = (float(pars1['#ptz'][0]) - float(pars['#ptz'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
    else:
        g2 = 0
    if '#table2z' in pars.keys():
        g3 = (float(pars1['#table2z'][0]) - float(pars['#table2z'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
    else:
        g3 = 0
    if g1 == np.max([g1, g2, g3]):
        flag1 = True
    else:
        flag1 = False
    guessdz = int(np.round(np.max([g1, g2, g3])))
    # or the x step
    if '#ptomorefx' in pars.keys():
        guessdx = (float(pars1['#ptomorefx'][0]) - float(pars['#ptomorefx'][0])) / float(pars['IMAGE_PIXEL_SIZE_1'][0])*1000
    elif '#mtomorefx' in pars.keys():
        guessdx = (float(pars1['#mtomorefx'][0]) - float(pars['#mtomorefx'][0])) / float(pars['IMAGE_PIXEL_SIZE_1'][0])*1000
    else:
        guessdx = 0
    guessdx = int(np.round(guessdx))
    # if interactive, confirm the zstep if we are using motor positions
    if (guessdx > guessdz) & (dxpix==None):
        print("This looks like a spiral in x")
        if interactive:
            dxpix = input('Enter the x step in pixels [%d] : ' % guessdx)
            dxpix = guessdx if dxpix == '' else int(dxpix)
        else:
            dxpix = guessdx
        zspiral = False
    elif (guessdz > guessdx) & (dzpix==None):
        print("This looks like a spiral in z")
        if interactive:
            dzpix = input('Enter the z step in pixels [%d] : ' % guessdz)
            dzpix = guessdz if dzpix == '' else int(dzpix)
        else:
            dzpix = guessdz
        zspiral = True


    if interactive:
        # work out the scan range
        nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
        nref = int(pars['FF_NUM_LAST_IMAGE'][0])
        movcor = np.zeros((nproj, 2))        
        scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * (nproj+1)
        if np.allclose(scanrange, 180, atol=10):
            scanrange = 180
            ndx90deg = int(nproj/2)
        elif np.allclose(scanrange, 360, atol=10):
            scanrange = 360
            ndx90deg = int(nproj/4)
        else:
            print("scan range %f not understood - parfile problem? - quitting" % scanrange)
            return
        pylab.figure() # open a figure for the radiographs
        pylab.gray()

    if interactive and (skip==None):
        tmp = input('Are there any sub-scans that should be skipped? y/[n]')
        tmp = True if tmp.lower() in ['y', 'yes', 'true', '1'] else False
        if tmp:
            tmp2 = input('Enter the numbers to skip, separated by spaces (e.g.: 0 5 6) : ')
            if tmp2 != '':
                skip = [int(elem) for elem in tmp2.split(' ')]
        else:
            skip = []

    nameout = scannameroot
    # make the directories
    if not os.path.exists(nameout):
        os.mkdir(nameout)
    if not os.path.exists(nameout+os.sep+nameout+"_edfs"):
        os.mkdir(nameout+os.sep+nameout+"_edfs")    

    # get the dimensions 
    xsize = int(pars['NUM_IMAGE_1'][0])
    ysize = int(pars['NUM_IMAGE_2'][0])
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1

    # how many scans?
    nspi = len(fullscannames) # how many files!
    nameA = fullscannames[0].split(os.sep)[-1]
    nameB = fullscannames[-1].split(os.sep)[-1]
    lenroot = len(scannameroot)+1 # to include the _
    nsteps = int(nameB[lenroot:(lenroot+2)]) - int(nameA[lenroot:(lenroot+2)]) + 1 # how many steps!
    if zspiral:
        ysizeout = int(ysize + ((nsteps-1) * dzpix))
        xsizeout = int(xsize)
    else:
        ysizeout = int(ysize)
        xsizeout = int(xsize + ((nsteps-1) * dxpix))

    print('here')
    if zspiral:
        print('here here')
        if flag1:
            print('here here here')
            # spiral in ptomotz or mtomotz
            g1mm = g1 * float(pars['IMAGE_PIXEL_SIZE_2'][0]) /1000 
            newzpos = float(pars[key1][0]) + ((nsteps-1) * g1mm / 2.) # each extra scan shifts the centre by half the step size
            # virtual motor position
            pars = par_tools.par_mod(pars, key1, newzpos, 'virtual motor position of spiral')

    # memory calc
    maxmem = 9000000000. # was... 18000000000.
    maxproj = np.floor(maxmem / (xsizeout*ysizeout)) # do in several blocks to limit memory use...
    print("do blocks of %d images..." % maxproj)
    nblocks = int(np.ceil(nproj/maxproj))
    startblock = np.floor(np.linspace(0, nproj, nblocks, False))
    endblock = np.zeros(nblocks)
    endblock[0:-1] = startblock[1:]
    endblock[-1] = nproj
    maxblocksize = int((endblock - startblock).max())


    # make a stack for the output
    print("allocate memory for the output - references")
    refAout = np.zeros((ysizeout, xsizeout), np.float32)
    refBout = np.zeros((ysizeout, xsizeout), np.float32)
    darkout = np.zeros((ysizeout, xsizeout), np.float32)
    countout = np.zeros((ysizeout, xsizeout), np.float32)
    
    # deal with all references first - if there are references with every scan
    print("treat the references images")
    # does the second scan have references? If not, it's a fast spiral (refs at the beginning and end, darks at end)
    fastspiral = True
    if os.path.isfile(fullscannames[1] + os.sep + "pre_ref.nxs"):
        fastspiral = False
        print("this is a normal spiral scan - references and darks at every step")
    else:
        print("this is a fast spiral scan - less references and darks")

    if fastspiral==False:
        for scanname in fullscannames:
            shortname = scanname.split(os.sep)[-1]
            #ndx = int(scanname[-2:])
            ndx = int(shortname[lenroot:(lenroot+2)])
            if ndx in skip:
                sys.stdout.write('skipping scan: %s\n' % scanname)
                continue
            else:
                sys.stdout.write('working on scan: %s\n' % scanname)
            sys.stdout.flush()
            # zshift for this scan
            if zspiral:
                endz = ysizeout - (ndx*dzpix)
                startz = ysizeout - ysize - (ndx*dzpix)
                endx = xsize
                startx = 0
            else:
                endz = ysize 
                startz = 0
                endx = xsizeout - (ndx*dxpix)
                startx = xsizeout - xsize - (ndx*dxpix)

            # treat the references and the darks
            # do the first refs
            f = h5py.File(scanname + os.sep + "pre_ref.nxs", 'r')
            daq = list(f.keys())[0]
            data = f[daq]
            # take the median
            im = np.median(data, 0)
            refAout[startz:endz, startx:endx] = refAout[startz:endz, startx:endx] + im
            f.close()
            # do the last refs
            f = h5py.File(scanname + os.sep + "post_ref.nxs", 'r')
            daq = list(f.keys())[0]
            data = f[daq]
            # take the median
            im = np.median(data, 0)
            refBout[startz:endz, startx:endx] = refBout[startz:endz, startx:endx] + im
            f.close()
            # do the darks
            f = h5py.File(scanname + os.sep + "post_dark.nxs", 'r')
            daq = list(f.keys())[0]
            data = f[daq]
            # take the median
            im = np.mean(data, 0)
            darkout[startz:endz, startx:endx] = darkout[startz:endz, startx:endx] + im
            f.close()        
            # finally, count the number of images per position
            countout[startz:endz, startx:endx] = countout[startz:endz, startx:endx] + 1
    else:
        # fast spiral scan 
        # first step - references
        ndx = 0
        scanname = fullscannames[0]
        # zshift for this scan
        if zspiral:
            endz = ysizeout - (ndx*dzpix)
            startz = ysizeout - ysize - (ndx*dzpix)
            endx = xsize
            startx = 0
        else:
            endz = ysize 
            startz = 0
            endx = xsizeout - (ndx*dxpix)
            startx = xsizeout - xsize - (ndx*dxpix)
        # treat the references and the darks
        # do the first refs
        f = h5py.File(scanname + os.sep + "pre_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        im = np.median(data, 0)
        refAout[startz:endz, startx:endx] = refAout[startz:endz, startx:endx] + im
        f.close()
        # do the last refs
        f = h5py.File(scanname + os.sep + "post_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        imA = np.median(data, 0) # this is used subsequently
        refBout[startz:endz, startx:endx] = refBout[startz:endz, startx:endx] + imA
        f.close()

        # last step
        scanname = fullscannames[-1]
        ndx = int(scanname[-2:])
        # zshift for this scan
        if zspiral:
            endz = ysizeout - (ndx*dzpix)
            startz = ysizeout - ysize - (ndx*dzpix)
            endx = xsize
            startx = 0
        else:
            endz = ysize 
            startz = 0
            endx = xsizeout - (ndx*dxpix)
            startx = xsizeout - xsize - (ndx*dxpix)
        # treat the references and the darks
        # do the first refs
        f = h5py.File(scanname + os.sep + "pre_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        imB = np.median(data, 0) # this is used subsequently
        refAout[startz:endz, startx:endx] = refAout[startz:endz, startx:endx] + imB
        f.close()
        # do the last refs
        f = h5py.File(scanname + os.sep + "post_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        im = np.median(data, 0)
        refBout[startz:endz, startx:endx] = refBout[startz:endz, startx:endx] + im
        f.close()
        # do the darks
        f = h5py.File(scanname + os.sep + "post_dark.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        imdark = np.mean(data, 0)
        f.close()        

        # complete the intermediate references
        for ii in range(1, nspi-1):
            scanname = fullscannames[ii]
            #ndx = int(scanname[-2:])
            shortname = scanname.split(os.sep)[-1]
            #ndx = int(scanname[-2:])
            ndx = int(shortname[lenroot:(lenroot+2)])
            # zshift for this scan
            if zspiral:
                endz = ysizeout - (ndx*dzpix)
                startz = ysizeout - ysize - (ndx*dzpix)
                endx = xsize
                startx = 0
            else:
                endz = ysize 
                startz = 0
                endx = xsizeout - (ndx*dxpix)
                startx = xsizeout - xsize - (ndx*dxpix)
            # fraction
            fA = ((ndx*1.) - 1) / (nspi - 2)
            fB = (ndx*1.) / (nspi - 2)
            # add to the cumulating refA
            im = (imB * fA) + (imA * (1-fA))
            refAout[startz:endz, startx:endx] = refAout[startz:endz, startx:endx] + im
            # add to the cumulating refB
            im = (imB * fB) + (imA * (1-fB))
            refBout[startz:endz, startx:endx] = refBout[startz:endz, startx:endx] + im
            
        # complete the darks
        for scanname in fullscannames:
            shortname = scanname.split(os.sep)[-1]
            #ndx = int(scanname[-2:])
            ndx = int(shortname[lenroot:(lenroot+2)])
            # zshift for this scan
            if zspiral:
                endz = ysizeout - (ndx*dzpix)
                startz = ysizeout - ysize - (ndx*dzpix)
                endx = xsize
                startx = 0
            else:
                endz = ysize 
                startz = 0
                endx = xsizeout - (ndx*dxpix)
                startx = xsizeout - xsize - (ndx*dxpix)
            darkout[startz:endz, startx:endx] = darkout[startz:endz, startx:endx] + imdark
            # finally, count the number of images per position
            countout[startz:endz, startx:endx] = countout[startz:endz, startx:endx] + 1

    # we can now divide by the number of scans
    refAout = refAout / countout 
    refBout = refBout / countout 
    darkout = darkout / countout 
    
    # write the references and the darks
    name = nameout + os.sep + nameout + '_edfs' + os.sep + 'ref000000.edf'
    im = np.uint16(refAout)
    edf.data = im
    edf.write(name)  
    name = nameout + os.sep + nameout + '_edfs' + os.sep + ('ref%06d.edf' % nproj)
    im = np.uint16(refBout)
    edf.data = im
    edf.write(name)
    name = nameout + os.sep + nameout + '_edfs' + os.sep + 'dark.edf'
    im = np.uint16(darkout)
    edf.data = im
    edf.write(name)
    
    print("Done all reference images... now do projections in %d block(s) to save memory" % nblocks)
    stackout = np.zeros((maxblocksize, ysizeout, xsizeout), np.float32)
    # do a block of projections
    for jj in range(nblocks):

        # which images ?
        nimages = int(endblock[jj] - startblock[jj])
        # reset stackout
        stackout[:,:,:] = 0 
        startim = int(startblock[jj])       
        endim = int(endblock[jj])       

        # go through all scans for this block
        for fullscanname in fullscannames:
            scanname = fullscanname.split(os.sep)[-1]
            shortname = scanname.split(os.sep)[-1]
            #ndx = int(scanname[-2:])
            ndx = int(shortname[lenroot:(lenroot+2)])
            #ndx = int(scanname[-2:])
            if ndx in skip:
                sys.stdout.write('skipping scan: %s\n' % scanname)
                sys.stdout.flush()
                continue
            # zshift for this scan
            if zspiral:
                endz = ysizeout - (ndx*dzpix)
                startz = ysizeout - ysize - (ndx*dzpix)
                endx = xsize
                startx = 0
            else:
                endz = ysize 
                startz = 0
                endx = xsizeout - (ndx*dxpix)
                startx = xsizeout - xsize - (ndx*dxpix)

            # read the data from the .nxs fileson the spool
            filename = fullscanname + os.sep + scanname + '.nxs'
            f = h5py.File(filename, 'r')
            daq = list(f.keys())[0]
            daq_grp = f[daq]
            scan_data = daq_grp["scan_data"]
            if 'orca' in scan_data.keys():
                orca_data = scan_data["orca"]
            elif 'orca_image' in scan_data.keys():
                orca_data = scan_data["orca_image"]
            elif 'after_' in scan_data.keys():
                orca_data = scan_data["after_"]
            else:
                print("can\'t find the data in the .nxs file!")
                print('will try to skip a block gracefully')
                continue

            # add the images to stackout        
            print("\ntreating the images for %s, block %d" % (scanname, jj))
            stackout[0:nimages, startz:endz, startx:endx] = stackout[0:nimages, startz:endz, startx:endx] + orca_data[startim:endim]
    
            # close the hdf5 file
            f.close()
        
        # write the images - can divide by countout in the same loop
        print("\nwriting images in %s\n" % nameout  )
        for ii in range(nimages):
            im = stackout[ii, :, :] / countout
            im = np.uint16(im)
            edf.data = im
            ndx = ii+startblock[jj]
            name = nameout + os.sep + nameout + '_edfs' + os.sep + nameout + ('%06d.edf' % ndx)
            edf.write(name)
            sys.stdout.write(6*"\b"+("%d" % ndx).rjust(6))
            sys.stdout.flush()
            # show the 0 and 90 degree images
            if interactive:
                if ndx == 0:
                    pylab.subplot(1,2,1)
                    flat = ((im*1.) - darkout) / (refAout - darkout)
                    pylab.imshow(flat)
                    pylab.title('0 degree projection')
                elif ndx == ndx90deg: 
                    pylab.subplot(1,2,2)
                    flat = ((im*1.) - darkout) / (refAout - darkout)
                    pylab.imshow(flat)
                    pylab.title('90 degree projection')


    # save the parfile
    expdir = os.getcwd() + os.sep + nameout
    parname = nameout + os.sep + nameout + '.par'
    pars = par_tools.par_mod(pars,'NUM_IMAGE_1', xsizeout)
    pars = par_tools.par_mod(pars,'NUM_IMAGE_2', ysizeout)
    # deal with the ROI
    pars = par_tools.par_mod(pars, 'START_VOXEL_1', 1)
    pars = par_tools.par_mod(pars, 'START_VOXEL_2', 1)
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', 1)
    pars = par_tools.par_mod(pars, 'END_VOXEL_1', xsizeout)
    pars = par_tools.par_mod(pars, 'END_VOXEL_2', xsizeout)
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', ysizeout)
    pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", (xsizeout+1)/2.)
    pars = par_tools.par_mod(pars, "FILE_PREFIX", expdir + os.sep + nameout + "_edfs" + os.sep + nameout)
    pars = par_tools.par_mod(pars, "BACKGROUND_FILE", expdir + os.sep + nameout + "_edfs" + os.sep + "dark.edf")
    pars = par_tools.par_mod(pars, "FF_PREFIX", expdir + os.sep + nameout + "_edfs" + os.sep + "ref")
    pars = par_tools.par_mod(pars, "OUTPUT_FILE", expdir + os.sep + nameout + "_slice.vol")

    
    par_tools.par_write(parname, pars)

    return scannameroot


                   



    




