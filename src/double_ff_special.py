import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from scipy import signal
import roi
import launch_reconstruction
import pickle
import call_pyhst



def double_ff_special(scanname, nsteps=18, cenr=200., thresholds=None, soft=False, substep=1, utopec=False, interactive=True):
    """
    Special double flatfield for homogenous samples, based on forward projection of a thresholded volume.
    nsteps - length of the moving mean - acquistion is divided by nsteps
    thresholds - to threshold the reconstruction before forward projection [value1, delta1m, delta1p, ...]
    soft - for two thresholds, can try to preserve soft edges
    substep - forward project only every nth images 
    utopec - substitute real projections obscured by columns by forward projections
    """

    if thresholds==None:
        print("need to supply the threshold data")
        print("thresholds = [value1, delta1m, delta1p, value2, delta2m, delta2p, etc...]")
        print("all gray levels between value-deltam and value+deltap of value become value")
        if interactive:
            tmp = input('Default values : [0, 100, 2, 5.1, 2, 100]. Use these [y]/n : ')
            tmp = True if tmp.lower() in ['', 'y', 'yes'] else False
            if tmp:
                thresholds = [0, 100, 2, 5.1, 2, 100]
            else:
                print('Add thresholds, starting from the lowest (least attenutating)')
                addthreshold = True
                thresholds = []
                while addthreshold:
                    val = float(input('Threshold value? : '))
                    thresholds.append(val)
                    d1 = np.abs(float(input('Lower range (i.e. %0.2f - X) ? : ' % val)))
                    thresholds.append(d1)
                    d2 = np.abs(float(input('Upper range (i.e. %0.2f + X) ? : ' % val)))
                    thresholds.append(d2)
                    tmp = input('Add another threshold? [y]/n : ')
                    addthreshold = True if tmp.lower() in ['', 'y', 'yes'] else False                    
                    print("threshold data: " + str(thresholds))
        else:
            print("rerun with threshold argument - quitting")
            return

    if interactive and utopec==False:
        tmp = input('Is this a Utopec experiment (with columns) [y]/n : ')
        utopec = True if tmp.lower() in ['', 'y', 'yes'] else False      

    #if utopec and nsteps!=1:
    #    print("For the utopec, nsteps should be 1 - forcing this")
    #    nsteps = 1

    if interactive:
        tmp = input('Divide the correction into N angualr increments [%d] : ' % nsteps)
        nsteps = nsteps if tmp == '' else int(tmp)    
        if nsteps>1:
            tmp = input('Avoid losing contrast in the centre - radius of the centre zone [200] :')
            cenr = 200. if cenr == '' else float(cenr)

    if soft and (len(thresholds)==6):
        print("\"soft\" thresholding between two values")
        print("<value1+delta1p --> value1+delta1p, >value2-delta2m --> value2-delta2m, then rescaled to give value1 and value2")
    elif soft:
        print("soft thresholding only possible for two values, deltas")
        return
    else:
        print("normal (not \"soft\") thresholding")

    if interactive:
        tmp = input('Step size when building the mean images [%d] : ' % substep)
        substep = substep if tmp == '' else int(tmp)    


    # so... I have 180 forward projected images in andy/andy_000000.edf
    # some strange stuff regarding values - I think because of big/little enedian vols - could maybe fix this by writing differently - edfs? or own vol write?
    # looking at the first volume, get some problems when we deviate from a centred cylinder.
    # probably need to go by blocks of angle - moving mean
    # concept seems good tho
    # ideally would be able to assign multiple phases
    # could probably do the same thing by fitting an off centre cone... but this is eventually more versatile.
    
    # so - pretty good - but
    # forward project is sharp at the edges, so we make a fringe
    # to get rid of this, could either clip the real projections, or add something to the fwd projections
    # second, the off centre nature of the object means that the edges of the mean image are not quite right
    # already, a true moving mean correction should help

    # need the moving mean
    # would be good to segment the large dendrites - is there a way without complicated filtering?
    # either fit the variation in gray level, else have to correct beam hardening first.  At this point we could...


    # start by resetting the ROI if any
    roi.roi(scanname, reset=True, interactive=False)

    # now, read the par file
    parname = scanname + os.sep + scanname + ".par"
    # pars for normal stuff
    pars = par_tools.par_read(parname)    
    # fpars for fancy stuff
    fpars = par_tools.par_read(parname)
    expdir = os.getcwd()
    # useful things    
    nproj = int(pars['NUM_LAST_IMAGE'][0])-int(pars['NUM_FIRST_IMAGE'][0])+1
    nprojfp = int(np.ceil(nproj/float(substep))) 
    myfolder = expdir + os.sep + scanname + os.sep + 'andy'
    xsize = int(pars['NUM_IMAGE_1'][0])
    ysize = int(pars['NUM_IMAGE_2'][0])
    rotaxis = float(pars['ROTATION_AXIS_POSITION'][0])
    if not os.path.exists(myfolder):
        os.mkdir(myfolder)

    check = True
    if os.path.exists(expdir + os.sep + scanname + os.sep + 'andy'+ os.sep + 'andy_0000.edf'):
        if interactive:
            print("at least one reconstructed slice exists!")
            tmp = input('Repeat reconstruction? y/[n] : ')
            tmp = True if tmp.lower() in ['y', 'yes', 'true'] else False
            if tmp:
                check = False # repeating this, so no need to check later steps
        else:
            tmp = True # when in doubt repeat?
            check = False
    else:
        tmp = True
        check = False

    if tmp:
        # Normal parameters for the first reconstruction
        # image paths    
        fpars = par_tools.par_mod(fpars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + scanname)    
        fpars = par_tools.par_mod(fpars, 'SUBTRACT_BACKGROUND', 'YES')
        fpars = par_tools.par_mod(fpars, 'CORRECT_FLATFIELD', 'YES')
        # is there an existing ff.edf ?
        flatfieldname = expdir + os.sep + scanname + os.sep + scanname + "_edfs" + os.sep + scanname + "_ff.edf"
        if os.path.exists(flatfieldname):
            fpars = par_tools.par_mod(fpars, 'DOUBLEFFCORRECTION', '\"%s\"' % flatfieldname)

        # we need to reconstruct first, but write to edf in the subfolder andy
        print("reconstructing as edf...")
        fpars = par_tools.par_mod(fpars, 'OUTPUT_FILE', myfolder+os.sep+'andy')
        fname = expdir + os.sep + scanname + os.sep + 'andy.par'
        par_tools.par_write(fname, fpars)
        call_pyhst.call_pyhst(fname) # call - interactively
    else:
        print("skipping... go to thresholding")

    if check:
        if os.path.exists(expdir + os.sep + scanname + os.sep + 'andy'+ os.sep + 'andy_thr_0000.edf'):
            if interactive:
                print("at least one thresholded slice exists!")
                tmp = input('Repeat thresholding? y/[n] : ')
                tmp = True if tmp.lower() in ['y', 'yes', 'true'] else False
                if tmp:
                    check = False
            else:
                tmp = True # when in doubt repeat?
                check = False
        else:
            tmp = True
            check = False

    if tmp:
        # now, we need to segment these images to remove the rings
        sys.stdout.write('thresholding... ....')
        #print('for maxime...')
        #thresholds = [0.0, 0.06, 0.33, 0.1]        
        for ii in range(ysize):
            myslice = edf.read(myfolder + os.sep + 'andy_%04d.edf' % ii).getData()
            if soft:
                # soft thresholding strategy
                # lower threshold
                myslice[np.nonzero(myslice<(thresholds[0]+thresholds[2]))] = thresholds[0]+thresholds[2]
                # upper threshold
                myslice[np.nonzero(myslice>(thresholds[3]-thresholds[4]))] = thresholds[3]-thresholds[4]
                # offset
                myslice = myslice - (thresholds[0]+thresholds[2])
                # rescale
                myslice = myslice * (thresholds[3] - thresholds[0]) / (thresholds[3] - thresholds[0] - thresholds[2] - thresholds[4])
            else:
                # go through the thresholds
                for tt in range(0, len(thresholds), 3):
                    # set everything within deltaN of valueN to valueN
                    mp = thresholds[tt] - (thresholds[tt+1]/2) + (thresholds[tt+2]/2)
                    delta = (thresholds[tt+1]+thresholds[tt+2])/2
                    myslice[np.nonzero(np.abs(myslice-mp) < delta)] = thresholds[tt]
            # write the thresholded data
            edf.setData(myslice)
            edf.write(myfolder + os.sep + 'andy_thr_%04d.edf' % ii)  
            sys.stdout.write('\b\b\b\b%04d' % ii)
            sys.stdout.flush()
    else:
        print("skipping... go to create correction")

    if check:
        if os.path.exists(expdir + os.sep + scanname + os.sep + 'andy'+ os.sep + 'fp_000000.edf'):
            if interactive:
                print("at least one forward projection exists!")
                tmp = input('Repeat forward projection? y/[n] : ')
                tmp = True if tmp.lower() in ['y', 'yes', 'true'] else False
                if tmp:
                    check = False
            else:
                tmp = True # when in doubt repeat?
                check = False
        else:
            tmp = True
            check = False

    if tmp:
        # now, forward project using this data as the input
        # make sure that the ROI et al is good
        # do 1 in 3 images
        print('forward projecting... ....')
        # read teh thresholded images
        fpars = par_tools.par_mod(fpars, 'OUTPUT_FILE', myfolder+os.sep+'andy_thr')
        step = substep*float(pars['ANGLE_BETWEEN_PROJECTIONS'][0])
        fpars = par_tools.par_mod(fpars, 'NUM_LAST_IMAGE', nprojfp-1)
        fpars = par_tools.par_mod(fpars, 'ANGLE_BETWEEN_PROJECTIONS', step)
        fpars = par_tools.par_mod(fpars, 'STEAM_INVERTER',1,'for forward projection')
        fpars = par_tools.par_mod(fpars, 'PROJ_OUTPUT_FILE', myfolder+os.sep+'fp_%06d.edf')

        fname = expdir + os.sep + scanname + os.sep + 'andy.par'
        par_tools.par_write(fname, fpars)
        call_pyhst.call_pyhst(fname)

    else:
        print("skipping... go to create correction")
    

    # read the references
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    refA = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'ref000000.edf').getData()*1.
    refB = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).getData()*1.
    dark = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'dark.edf').getData()*1.

    # make the corrections - work in the fp step size for simplicity (?)
    meanrange = nproj / substep / nsteps # how long is the moving mean
    sys.stdout.write('make the correction... ....')
    sys.stdout.flush()
    # do a proper moving mean 
    perfectsum = np.zeros((refA.shape[0], refA.shape[1]))
    realsum = np.zeros((refA.shape[0], refA.shape[1]))

    validimages = np.zeros(int(meanrange)) # this is zero for bad, one for good
    # if its utopec need to find columns, if it's nsteps>1 need a correction for the centre
    if nsteps>1 or utopec:
        # how many pixels are illuminated in the first image?
        im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s000000.edf' % scanname)).getData()*1.
        im = (im*1. - dark) / (refA - dark)
        ndark = len(np.nonzero(im < 0.05)[0]) # Number of dark pixels 
        nbright = (refA.shape[0]*refA.shape[1]) - ndark
        utopectest = ndark + (nbright * 0.05) # if 5% of the remaining pixels go dark... it's a column
        columnlist = []
        # fill in the column list before doing anything else - could perhaps be improved
        for pp in range(int(nproj / substep)):
            # real - flatfield
            ii = pp*substep
            realim = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
            f = float(ii)/nproj
            ref = refB*f + refA*(1-f)
            realim = (realim-dark)/(ref-dark)
            if len(np.nonzero(realim<0.05)[0]) > utopectest:
                # this is dark, so skip it
                columnlist.append(ii)
            else:
                # this is good, use it to build a master correction
                realsum += realim
                # perfect - calc flatfield
                perfectim = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % pp).getData()*1.
                perfectim = np.exp(-perfectim)
                perfectsum += perfectim
        # build and save master correction      
        ffcen = np.float32(np.log(perfectsum/realsum))
        edf.setData(ffcen)
        edf.write(scanname + os.sep + 'andy'  + os.sep + 'ff_master.edf')        
        # extend the colum list a little
        if utopec:
            columnstart = columnlist[0] - 3
            columnend = columnlist[-1] + 3
            perreal = 100 * (1 - ((columnend - columnstart)*1. / nproj))
            print("getting %0.1f percent of real projections" % perreal)


#### original first image #####
#    # build the perfect mean image - first image
#    for pp in range(0, meanrange):
#        sys.stdout.write('\b\b\b\b%04d' % pp)
#        sys.stdout.flush()
#        # real - flatfield
#        ii = pp*substep
#        realim = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
#        f = float(ii)/nproj
#        ref = refB*f + refA*(1-f)
#        realim = (realim-dark)/(ref-dark)
#        ## check for dark images
#        if utopec and (ii>columnstart and ii<columnend):
#            # this is dark, so skip it
#            validimages[pp] = 0
#        else:
#            realstack[:,:,pp] = realim # place in the stack
#            # perfect - calc flatfield
#            perfectim = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % pp).getData()*1.
#            perfectim = np.exp(-perfectim)
#            perfectstack[:,:,pp] = perfectim # place in the stack
#            validimages[pp] = 1
#    # this gives the first correction, corresponding to the centre of the first block
#    if utopec:
#        print("avoid columns...")
#        validndx = np.nonzero(validimages)[0]
#        ff = np.float32(np.log(np.sum(perfectstack[:,:,validndx], 2)/np.sum(realstack[:,:,validndx], 2)))
#    else:
#        ff = np.float32(np.log(perfectstack.mean(2)/realstack.mean(2)))
#    edf.setData(ff)
#    ndx0 = int(meanrange / 2)
#    edf.write(scanname + os.sep + 'andy'  + os.sep + 'ff_%06d.edf' % ndx0)


    # make the corrections - work in the fp step size for simplicity (?)
    meanrange = int(nproj / substep / nsteps) # how long is the moving mean
    sys.stdout.write('make the correction... ....')
    sys.stdout.flush()
    # do a proper moving mean 
    if nsteps>1:
        print("Define the stacks for the moving mean")
        perfectstack = np.zeros((refA.shape[0], refA.shape[1], int(meanrange)))
        realstack = np.zeros((refA.shape[0], refA.shape[1], int(meanrange)))
    else:
        # just add into the sum image
        print("Only one step, so no need to define a stack")
        realsum = np.zeros((refA.shape[0], refA.shape[1]))
        perfectsum = np.zeros((refA.shape[0], refA.shape[1]))
### if there is only one correction, don't need to build a stack!!!

#### new first image - think this is equivilent #####
    # build the initial stacks for the first block of images
    print("build the initial stacks... "        )
    for pp in range(0, int(meanrange)):
        sys.stdout.write('\b\b\b\b%04d' % pp)
        sys.stdout.flush()
        # real index
        ii = pp*substep
        # check for dark images
        if utopec and (ii>columnstart and ii<columnend):
            # this is dark, so skip it
            # this is dark, so set the image to zero
            realim = np.zeros((ysize, xsize))
            perfectim = np.zeros((ysize, xsize))
            #validimages[np.mod(pp, meanrange)] = 0
        else:
            # read real
            realim = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
            # perfect - calc flatfield
            perfectim = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % pp).getData()*1.
            perfectim = np.exp(-perfectim)
            if nsteps > 1: # flatfield now. Otherwise, one single flatfield at the end
                # flatfield real
                f = float(ii)/nproj
                ref = refB*f + refA*(1-f)
                realim = (realim-dark)/(ref-dark)
            #validimages[np.mod(pp, meanrange)] = 1
        if nsteps>1:
            realstack[:,:,int(np.mod(pp, meanrange))] = realim # place in the stack
            perfectstack[:,:,int(np.mod(pp, meanrange))] = perfectim # place in the stack
        else:
            realsum += realim
            perfectsum += perfectim
    if nsteps == 1:
        # one flatfield operation
        if utopec:
            nvalid = nproj - (columnend - columnstart)
        else:
            nvalid = nproj
        realsum = realsum / nvalid
        ref = (refA + refB) / 2.
        realsum = (realsum - dark) / (ref - dark)
        perfectsum = perfectsum / nvalid
        ff = np.float32(np.log(perfectsum/realsum))
    else:
        # this gives the first correction, corresponding to the centre of the first block
        # no need to avoid the columns here, because their images are set to zero
        ff = np.float32(np.log(perfectstack.sum(2)/realstack.sum(2)))
        realsum = realstack.sum(2)
        perfectsum = perfectstack.sum(2)
    edf.setData(ff)
    ndx0 = int(meanrange / 2)
    edf.write(scanname + os.sep + 'andy'  + os.sep + 'ff_%06d.edf' % ndx0)

##### new correction code, faster but currently wrong! #########
    # Now... run through the scan, updating the sum images 
    print("roll through the scan"   )
    for pp in range(int(meanrange), nprojfp): 
        sys.stdout.write('\b\b\b\b%04d' % pp)
        sys.stdout.flush() 
        # one new image to add - replaces an existing image in the stack
        # real index
        ii = pp*substep
        # old images to subtract from the sum
        oldreal = realstack[:,:,int(np.mod(pp, meanrange))]*1.
        oldperfect = perfectstack[:,:,int(np.mod(pp, meanrange))]*1.
        if utopec and (ii>columnstart and ii<columnend):
            # this is dark, so set the image to zero
            realim = np.zeros((ysize, xsize))
            perfectim = np.zeros((ysize, xsize))
        else:
            # flatfield 
            realim = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
            f = float(ii)/nproj
            ref = refB*f + refA*(1-f)
            realim = (realim-dark)/(ref-dark)
            # perfect - use pp in case wrapping - calc flatfield
            perfectim = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % pp).getData()*1.
            perfectim = np.exp(-perfectim)
        realstack[:,:,int(np.mod(pp, meanrange))] = realim # place in the stack
        perfectstack[:,:,int(np.mod(pp, meanrange))] = perfectim # place in the stack                        
        # deal with the sum images
        realsum = realsum - oldreal + realim
        perfectsum = perfectsum - oldperfect + perfectim
        ff = np.float32(np.log(perfectsum/realsum))
        # write the new ff image
        edf.setData(ff)
        ndx = pp - int(meanrange / 2)
        edf.write(scanname + os.sep + 'andy'  + os.sep + 'ff_%06d.edf' % ndx)
        ndxfinal = ndx # save this for the next bit



##### this is the bit I'm trying to replace with something faster! #####
#    for pp in range(meanrange, (nprojfp)):
#        sys.stdout.write('\b\b\b\b%04d' % pp)
#        sys.stdout.flush()
#        # real - flatfield
#        ii = pp*substep
#        realim = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
#        f = float(ii)/nproj
#        ref = refB*f + refA*(1-f)
#        realim = (realim-dark)/(ref-dark)
#        ## check for dark images
#        ## check for dark images
#        if utopec and (ii in columnlist):#
#            # this is dark, so skip it
#            validimages[np.mod(pp, meanrange)] = 0
#        else:   
#            realstack[:,:,np.mod(pp, meanrange)] = realim # place in the stack
#            # perfect image to add - calc flatfield
#            perfectim = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % pp).getData()*1.
#            perfectim = np.exp(-perfectim)
#            perfectstack[:,:,np.mod(pp, meanrange)] = perfectim # place in the stack
#            validimages[np.mod(pp, meanrange)] = 1
#        if utopec:
#            print("avoid columns...")
#            validndx = np.nonzero(validimages)[0]
#            ff = np.float32(np.log(np.sum(perfectstack[:,:,validndx], 2)/np.sum(realstack[:,:,validndx], 2)))
#        else:
#            ff = np.float32(np.log(perfectstack.mean(2)/realstack.mean(2)))
#        edf.setData(ff)
#        ndx = pp - int(meanrange / 2)
#        edf.write(scanname + os.sep + 'andy'  + os.sep + 'ff_%06d.edf' % ndx)
#        ndxfinal = ndx # save the last ndx        
#    # this leaves us with a stack of ff images to use later

    # make the mask
    if nsteps == 1:
        ffcen = np.zeros((ysize, xsize))
        cenmask = np.zeros((ysize, xsize))
    else:
        cenprofile = np.zeros(xsize)
        cenprofile[int(rotaxis - 0.25*cenr) : int(rotaxis + 0.25*cenr)] = 1
        xdata = np.arange(len(cenprofile[int(rotaxis + 0.25*cenr):int(rotaxis+cenr)]))
        xdata = xdata * np.pi / xdata.max() # 0 to pi
        cenprofile[int(rotaxis + 0.25*cenr):int(rotaxis+cenr)] = (np.cos(xdata) + 1)/2.
        xdata = np.arange(len(cenprofile[int(rotaxis - cenr):int(rotaxis - 0.25*cenr)]))
        xdata = xdata * np.pi / xdata.max() # 0 to pi
        xdata = xdata[::-1] # pi to 0
        cenprofile[int(rotaxis - cenr):int(rotaxis - 0.25*cenr)] = (np.cos(xdata) + 1)/2.
        cenmask = np.tile(cenprofile.reshape((1, xsize)), (ysize, 1))
        #cenmask = np.zeros((ysize, xsize))

    # finally, go through all the projections, applying this and saving out cor.edfs
    # if utopec, substitute the fake projection...?
    sys.stdout.write('apply the correction... ....')
    currentndx = None
    for ii in range(nproj):
        im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
        f = float(ii)/nproj
        ref = refB*f + refA*(1-f)
        im = (im-dark)/(ref-dark)
        # individual correction - find nearest correction image
        myndx = int(np.round(float(ii)/substep))
        if nsteps==1:
            ndx = ndx0 # always the same
        elif myndx <= ndx0: 
            ndx = ndx0
        elif myndx >= ndxfinal:
            ndx = ndxfinal
        else:
            ndx = myndx
        if ndx != currentndx:
            # if we don't alrady have it, read the ff
            myff = edf.read(scanname + os.sep + 'andy'  + os.sep + 'ff_%06d.edf' % ndx).getData()*1.
            currentndx = ndx
        # mix myff with ffcen to avoid blurring centre
        myff = (myff*(1 - cenmask)) + (ffcen*cenmask)
        # apply the correction
        im = im * np.exp(myff)
        # utopec special behaviour
        if utopec and ((ii>columnstart) and (ii<columnend)):
            # this is a column, substitute the forward projection
            print("image %d is a column, sub by the fp" % ii)
            im = edf.read(scanname + os.sep + 'andy'  + os.sep + 'fp_%06d.edf' % myndx).getData()
            im = np.exp(-im)
        # write the results
        edf.setData(np.float32(im))
        edf.write(scanname + os.sep + scanname + "_edfs"  + os.sep + ('cor%06d.edf' % ii))       
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()
    
    # update the normal par file - re-read
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    # remove the "normal" ff correction
    pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
    # image paths    
    pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'cor')    
    pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
    pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
    # save the updated pars!
    par_tools.par_write(parname, pars)

    # now, reconstruct!
    launch_reconstruction.launch_reconstruction(scanname, blocksize=0, useroi=False, interactive=False)


