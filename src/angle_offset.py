
import argparse
import par_tools
import os

def angle_offset(scanname, angle=0.0, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    current_angle = float(pars['ANGLE_OFFSET'][0])
 
    if interactive:
        selection = input("Angle offset ? [%0.1f] : " % current_angle)
        angle = current_angle if selection=="" else float(selection)

    # write this to the par file
    pars = par_tools.par_mod(pars, 'ANGLE_OFFSET', angle)
    par_tools.par_write(parname, pars)
    print("Updated par file with new angle offset")

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set the angle offset for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-a", "--angle", help="angle offset", default=0.0, type=float)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    angle_offset(args.scanname, angle=args.angle, interactive=args.interactive)

    # ----------------------------------------------------- #
