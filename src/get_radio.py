

from fabio.edfimage import edfimage
edf = edfimage()
import h5py
# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()
import os
import par_tools
import numpy as np

def get_radio(scanname, what='radio', ndx=0, unit='number', interpolate=False, interactive=True):
    """
    Helper function to get a radiograph
    Adapt to edf or nxs structure
    Can specify by index or angle
    what = radio / flat / ref / pre_ref / post_ref / dark
    ref is interpolated through the scan
    """
    if interactive:
        import pylab
        pylab.ion()

    if interactive:
        # get values from user
        what = input('Type of radiograph radio/[flat]/ref/pre_ref/post_ref/post_dark:')
        what = 'flat' if what=='' else what
        what = 'post_dark' if what == 'dark' else what # allow this
        # we might not need other info
        if what in ['radio', 'flat', 'ref']:
            ndx = input('Which radiograph (index or angle) [0] :')
            ndx = 0 if ndx=='' else float(ndx)
            unit = input('Units [number]/deg/deg_abs :')
            unit = 'number' if unit=='' else unit
            interpolate = input('Interpolate between images y/[n] :')
            interpolate = True if interpolate.lower() in ['y', 'yes', 'true'] else False        


    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()
    nproj = int(pars['NUM_LAST_IMAGE'][0])
    ndx = nproj if ndx == - 1 else ndx # allow -1 as an image ndx
    if unit in ['deg', 'deg_abs']:
        if unit == 'deg_abs':
            offset = float(pars['ANGLE_OFFSET'][0])
        else:
            offset = 0
        ndx = np.mod(ndx - offset, 360)
        # degrees to ndx
        ndx = ndx / float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) # mod on 360 degrees
        # is this after the end of the scan ?
        flip = False
        if ndx > nproj:
            # take the 180 offset image and flip it ?
            ndx = np.mod(ndx, 180/float(pars['ANGLE_BETWEEN_PROJECTIONS'][0])) # mode on projections per 180
            flip = True

    # do we interpolate between images
    if interpolate:
        print("Build Maxime\'s special 180 degree image")
        ndxA = np.floor(ndx)
        ndxB = ndxA + 1
        f = np.mod(ndx, 1)
    else:
        ndxA = np.round(ndx)
        ndxB = None
        f = 0

    # get the image - NXS case
    if ('PYHST_PROJECTION_FORMAT' in beamline.keys()) and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
        # open the nxs file
        fh = h5py.File(scanname + os.sep + scanname + '.nxs', 'r+') # to enable adding to the file
        daq = list(fh.keys())[0]
        daq_grp = fh[daq]
        scan_data = daq_grp["scan_data"]
        if 'orca' in scan_data.keys():
            orca_data = scan_data["orca"]
        elif 'orca_image' in scan_data.keys():
            orca_data = scan_data["orca_image"]
        elif 'after_' in scan_data.keys():
            orca_data = scan_data["after_"]
        else:
            print("can\'t find the data in the .nxs file!")
            return

        # do we need an image ?
        if what in ['radio', 'flat']:
            # read the image
            if interpolate:
                imA = orca_data[int(ndxA), :, :] * 1.
                imB = orca_data[int(ndxB), :, :] * 1.
                im = (imB*f) + (imA*(1-f))
            else:
                im = orca_data[ndxA, :, :] * 1.

        # do we need the references ?
        if what in ['flat', 'ref', 'pre_ref', 'post_ref', 'post_dark']:
            # if we are using NXS, do we already have references?
            if 'treated_data' in daq_grp.keys():
                print('reading references from the nxs file')
                refA = daq_grp['treated_data']['pre_ref'][:] * 1.
                refB = daq_grp['treated_data']['post_ref'][:] * 1.
                dark = daq_grp['treated_data']['post_dark'][:] * 1.
            # if treated references not present, create them
            else: 
                print('creating references in the nxs file')
                # create the new group in the nxs
                daq_grp.create_group('treated_data')
                fr = h5py.File(scanname + os.sep + 'pre_ref.nxs', 'r')
                refA = np.median(fr['ref_0'], 0)
                daq_grp['treated_data'].create_dataset('pre_ref', data=refA)
                fr.close()                
                fr = h5py.File(scanname + os.sep + 'post_ref.nxs', 'r')
                refB = np.median(fr['ref_0'], 0)
                daq_grp['treated_data'].create_dataset('post_ref', data=refB)
                fr.close()                
                fr = h5py.File(scanname + os.sep + 'post_dark.nxs', 'r')
                dark = np.median(fr['dark_0'], 0)
                daq_grp['treated_data'].create_dataset('post_dark', data=refB)
                fr.close()

        # close the file
        fh.close()

    # else, edf case
    else:
        edfpath = scanname + os.sep + scanname + "_edfs" + os.sep 
        # do we need an image ?
        if what in ['radio', 'flat']:
            if interpolate:
                imA = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxA).data
                imB = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxB).data
                im = (imB*f) + (imA*(1-f))
            else:
                im = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxA).data

        # do we need the references ?
        if what in ['flat', 'ref', 'pre_ref', 'post_ref', 'post_dark']:
            # references
            refndx = int(pars['FF_NUM_LAST_IMAGE'][0])
            dark = 1.0*edf.read(edfpath + "dark.edf").data
            refA = 1.0*edf.read(edfpath + "ref000000.edf").data
            refB = 1.0*edf.read(edfpath + "ref%06d.edf" % refndx).data
    
    if what in ['flat', 'ref']:
    # Do the interpolated ref
        f_ref = ndxA / nproj
        ref = refA * (1 - f_ref) + refB * f_ref # interpolate ref
    if what == 'flat':
        im = (im - dark) / (ref - dark) # flatfield

    # Are we using NXS ?
    if ('PYHST_PROJECTION_FORMAT' in beamline.keys()) and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
        fh.close()

    # flip if needed
    if flip:
        print('Flip image left-right')
        im = im[:, ::-1]

    # account for returning refs or darks
    if what == 'ref':
        im = ref
    elif what == 'pre_ref':
        im = refA
    elif what == 'post_ref':
        im = refB
    elif what == 'post_dark':
        im = dark
    else:
        pass # im is the im calculated     


    # Show the image ?
    if interactive:
        pylab.imshow(im)
        pylab.title('%s : radio %d (%s)' % (scanname, ndx, what))
    
    return im




