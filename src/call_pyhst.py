


# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()
import os
import time
import sys
import glob
import socket

def call_pyhst(parfilename, timeout=0):
    """
    Call PyHST on a parfile
    Uses the version defined for the beamline
    This is interactive type of function, not submitted to a queue
    """

    if beamline['BEAMLINE'][0] == 'PSICHE':
        print('----------at psiche, timeout = %d -------' % timeout)
        # local version
        if timeout==0:
            # launch, and wait for the job to finish
            os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null' % parfilename)
        else:
            # launch, and if not finished after timeout, kill the job
            # get the job list
            joblist = glob.glob('/proc/*/comm')
            existing = []
            for elem in joblist:
                try:
                    if open(elem).read().strip() == 'pyhst_2_2015b':
                        existing.append(elem)
                except:
                    continue
            # start the job
            os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null &' % parfilename)
            t0 = time.time()
            # wait for job to start
            time.sleep(10)
            wait = 30
            count = 0
            search = True
            while search:
                joblist = glob.glob('/proc/*/comm')
                pid = []
                for elem in joblist:
                    try:
                        if open(elem).read().strip() == 'pyhst_2_2015b':
                            if elem not in existing:
                                pid.append(elem)
                    except:
                        continue
                if len(pid)<2:
                    search = True # keep looking
                    count+=1
                    print("could not find the pyhst jobs - wait a bit longer (?)")
                    time.sleep(1)
                    if count>wait:
                        search = False
                        print("could not find the pyhst jobs - quitting (?)")
                        return
                else:
                    search = False # found our pyhst x 2
                    print("Found the pyhst jobs - now let them run for up to %d seconds..." % timeout)

            # wait for job to finish, or for timeout
            sys.stdout.write('PYHST2 running for 0000 seconds')
            cont = True
            while cont :
                if not os.path.exists(pid[0]):
                    sys.stdout.write('\nPYHST2 finished\n')
                    sys.stdout.flush()                    
                    cont = False
                time.sleep(1)
                t = time.time()
                sys.stdout.write('\b\b\b\b\b\b\b\b\b\b\b\b%04d seconds' % (t-t0))
                sys.stdout.flush()
                if (t-t0) > timeout:
                    sys.stdout.write('\ntimeout! killing job to unblock\n')
                    sys.stdout.flush()
                    os.system('kill %s' % pid[0].split(os.sep)[2])
                    time.sleep(5)
                    cont = False
            sys.stdout.write('\n\n')
            sys.stdout.flush()            

    elif beamline['BEAMLINE'][0] == 'PSICHE_NEW':
        # new slurm on psiche-gpu1 and 2
        hostname = socket.gethostname()
        if ('PYHST_PROJECTION_FORMAT' in beamline.keys()) and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
            template = '/shared/Slurm_templates/srun_template2021a.slurm'
        else:
            template = '/shared/Slurm_templates/srun_template.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end
        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
        fout.close()
        f.close()
        os.system('chmod +x %s' % scriptfilename)
        # interactive node! use sbatch
        if 'SLURM_NTASKS' in os.environ:
            # we are inside a slurm job already
            os.system(scriptfilename)
        else:
            # we are not inside a slurm job - launch with sbatch
            os.system('srun --exclusive --gres gpu:1 %s' % scriptfilename)
        #if hostname == 'psiche-gpu1':
        #    print("on psiche-gpu1 - launch with srun - but could already be inside a slurm job")
        #    # interactive node! use srun
        #    os.system('srun --exclusive --gres gpu:1 %s' % scriptfilename)
        #elif hostname == 'psiche-gpu2':
        #    print("on psiche-gpu2 - run directly")
        #    # run directly
        #    os.system(scriptfilename)
        #else:
        #    # on a calculation node? run direct
        #    os.system(scriptfilename)


    elif beamline['BEAMLINE'][0] == 'ANATOMIX':

        # new style
        hostname = socket.gethostname()
        template = '/home/experiences/anatomix/com-anatomix/Slurm_templates/srun_template_atxGPU.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end
        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
        fout.close()
        f.close()
        os.system('chmod +x %s' % scriptfilename)
        if hostname == 'atxgpu.cluster':
            print("launch with srun...")
            # interactive node! use srun
            os.system('srun -N1 %s' % scriptfilename)
        else:
            # on a calculation node! run direct
            os.system(scriptfilename)

    elif beamline['BEAMLINE'][0] == 'SUMO':
        # slurm version
        # for anatomix - write a mini script
        # is there a way to write the machine file in srun? 
        hostname = socket.gethostname()
        if hostname == 'sumo':
            # on the interactive node - machine file on scratch seems to work
            template = '/home/experiences/psiche/com-psiche/srun_template_sumoGPU.slurm'
        else:
            # on a calculation node - we can use GPU
            template = '/home/experiences/psiche/com-psiche/srun_template_sumoGPU.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end

        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
        fout.close()
        f.close()
        os.system('chmod +x %s' % scriptfilename)
        if hostname == 'sumo':
            print("launch with srun...")
            # interactive node! use srun
            os.system('srun -N1 %s' % scriptfilename)
        else:
            # on a calculation node! run direct
            os.system(scriptfilename)
    else:
        print("Beamline name not recognised")

def batch_pyhst(parfilename, timeout=0):
    """
    Call PyHST on a parfile
    Uses the version defined for the beamline
    This is submitted to a queue if available
    """

    if beamline['BEAMLINE'][0] == 'PSICHE':
        # local version
        if timeout==0:
            # launch, and wait for the job to finish
            os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null' % parfilename)
        else:
            # launch, and if not finished after timeout, kill the job
            # get the job list
            joblist = glob.glob('/proc/*/comm')
            existing = []
            for elem in joblist:
                try:
                    if open(elem).read().strip() == 'pyhst_2_2015b':
                        existing.append(elem)
                except:
                    continue
            # start the job
            os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null &' % parfilename)
            t0 = time.time()
            # wait for job to start
            time.sleep(10)
            wait = 30
            count = 0
            search = True
            while search:
                joblist = glob.glob('/proc/*/comm')
                pid = []
                for elem in joblist:
                    try:
                        if open(elem).read().strip() == 'pyhst_2_2015b':
                            if elem not in existing:
                                pid.append(elem)
                    except:
                        continue
                if len(pid)<2:
                    search = True # keep looking
                    count+=1
                    print("could not find the pyhst jobs - wait a bit longer (?)")
                    time.sleep(1)
                    if count>wait:
                        search = False
                        print("could not find the pyhst jobs - quitting (?)")
                        return
                else:
                    search = False # found our pyhst x 2
                    print("Found the pyhst jobs - now let them run for up to %d seconds..." % timeout)

            # wait for job to finish, or for timeout
            sys.stdout.write('PYHST2 running for 0000 seconds')
            cont = True
            while cont :
                if not os.path.exists(pid[0]):
                    sys.stdout.write('\nPYHST2 finished\n')
                    sys.stdout.flush()                    
                    cont = False
                time.sleep(1)
                t = time.time()
                sys.stdout.write('\b\b\b\b\b\b\b\b\b\b\b\b%04d seconds' % (t-t0))
                sys.stdout.flush()
                if (t-t0) > timeout:
                    sys.stdout.write('\ntimeout! killing job to unblock\n')
                    sys.stdout.flush()
                    os.system('kill %s' % pid[0].split(os.sep)[2])
                    time.sleep(5)
                    cont = False
            sys.stdout.write('\n\n')
            sys.stdout.flush()     

    elif beamline['BEAMLINE'][0] == 'PSICHE_NEW':
        # slurm version
        # new style
        if ('PYHST_PROJECTION_FORMAT' in beamline.keys()) and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
            template = '/shared/Slurm_templates/sbatch_template2021a.slurm'
        else:
            template = '/shared/Slurm_templates/sbatch_template.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end
        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
        f.close()
        fout.close()
        os.system('chmod +x %s' % scriptfilename)
        print("launch with sbatch...")
        # may have to deal with the case that we are already inside a slurm batch job/
        # interactive node! use sbatch
        if 'SLURM_NTASKS' in os.environ:
            # we are inside a slurm job already
            print('WE ARE INSIDE A SLURM JOB : SLURM_NTASKS = %s' % os.environ['SLURM_NTASKS'])
            print('launching %s' % scriptfilename)
            os.system(scriptfilename)
        else:
            # we are not inside a slurm job - launch with sbatch
            print('NOT INSIDE A SLURM JOB - I THINK ')
            print('launching %s with SBATCH' % scriptfilename)
            os.system('sbatch %s' % scriptfilename)

    elif beamline['BEAMLINE'][0] == 'ANATOMIX':
        # slurm version
        # new style
        template = '/home/experiences/anatomix/com-anatomix/Slurm_templates/batch_template.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end
        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
            if line.find('linesbefore')!=-1:
                for myline in linesbefore:
                    fout.write(myline+'\n')
            if line.find('linesafter')!=-1:
                for myline in linesafter:
                    fout.write(myline+'\n')
        f.close()
        fout.close()
        os.system('chmod +x %s' % scriptfilename)
        hostname = socket.gethostname()
        if hostname == 'atxgpu.cluster':
            print("launch with sbatch...")
            # interactive node! use sbatch
            os.system('sbatch %s' % scriptfilename)
        else:
            # on a calculation node!  run direct
            os.system(scriptfilename)

    elif beamline['BEAMLINE'][0] == 'SUMO':
        # slurm version
        # for SUMO - write a mini batch script
        template = '/home/experiences/psiche/com-psiche/batch_template.slurm'
        f = open(template, 'rt')
        # same name as the par file, but with slurm.sh at the end
        scriptfilename = parfilename[0:parfilename.rfind('.')] + '_slurm_script.sh'
        fout = open(scriptfilename, 'wt')
        for line in f.readlines():
            fout.write(line.replace('XXX', parfilename))
            if line.find('linesbefore')!=-1:
                for myline in linesbefore:
                    fout.write(myline+'\n')
            if line.find('linesafter')!=-1:
                for myline in linesafter:
                    fout.write(myline+'\n')
        f.close()
        fout.close()
        os.system('chmod +x %s' % scriptfilename)
        hostname = socket.gethostname()
        if hostname == 'sumo':
            print("launch with sbatch...")
            # interactive node! use sbatch
            os.system('sbatch %s' % scriptfilename)
        else:
            # on a calculation node!  run direct
            os.system(scriptfilename)

    else:
        print("Beamline name not recognised")

def batch_script(scriptfilename):
    if beamline['BEAMLINE'][0] == 'PSICHE':
        # local version - trivial
        os.system('chmod +x %s' % scriptfilename)
        os.system(scriptfilename)

    elif beamline['BEAMLINE'][0] == 'PSICHE_NEW':
        # slurm version
        if ('PYHST_PROJECTION_FORMAT' in beamline.keys()) and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
            template = '/shared/Slurm_templates/sbatch_script_template2021a.slurm'
        else:
            template = '/shared/Slurm_templates/sbatch_script_template.slurm'
        f = open(template, 'rt')
        headerlines = f.readlines()
        f.close()
        f = open(scriptfilename, 'rt')
        scriptlines = f.readlines()
        f.close()
        # slurm script has same name script, but with .slurm at the end
        slurmfilename = scriptfilename[0:scriptfilename.rfind('.')] + '.slurm'
        fout = open(slurmfilename, 'wt')
        for line in headerlines:
            fout.write(line.replace('XXX', slurmfilename))
        for line in scriptlines:
            fout.write(line)
        fout.close()
        os.system('chmod +x %s' % slurmfilename)
        print("launch with sbatch...")
        os.system('sbatch %s' % scriptfilename)

    elif beamline['BEAMLINE'][0] == 'ANATOMIX':
        # slurm version
        # for SUMO - add the slurm header
        template = '/home/experiences/anatomix/com-anatomix/Slurm_templates/slurm_header_template.slurm'
        f = open(template, 'rt')
        headerlines = f.readlines()
        f.close()
        f = open(scriptfilename, 'rt')
        scriptlines = f.readlines()
        f.close()
        # slurm script has same name script, but with .slurm at the end
        slurmfilename = scriptfilename[0:scriptfilename.rfind('.')] + '.slurm'
        fout = open(slurmfilename, 'wt')
        for line in headerlines:
            fout.write(line.replace('XXX', slurmfilename))
        for line in scriptlines:
            fout.write(line)
        fout.close()
        os.system('chmod +x %s' % slurmfilename)
        hostname = socket.gethostname()
        if hostname == 'atxgpu.cluster':
            print("launch with sbatch...")
            # interactive node! use sbatch
            os.system('sbatch %s' % scriptfilename)
        else:
            # on a calculation node!  run direct
            os.system(scriptfilename)
    
    elif beamline['BEAMLINE'][0] == 'SUMO':
        # slurm version
        # for SUMO - add the slurm header
        template = '/home/experiences/psiche/com-psiche/slurm_header_template.slurm'
        f = open(template, 'rt')
        headerlines = f.readlines()
        f.close()
        f = open(scriptfilename, 'rt')
        scriptlines = f.readlines()
        f.close()
        # slurm script has same name script, but with .slurm at the end
        slurmfilename = scriptfilename[0:scriptfilename.rfind('.')] + '.slurm'
        fout = open(slurmfilename, 'wt')
        for line in headerlines:
            fout.write(line.replace('XXX', slurmfilename))
        for line in scriptlines:
            fout.write(line)
        fout.close()
        os.system('chmod +x %s' % slurmfilename)
        hostname = socket.gethostname()
        if hostname == 'sumo':
            print("launch with sbatch...")
            # interactive node! use sbatch
            os.system('sbatch %s' % scriptfilename)
        else:
            # on a calculation node!  run direct
            os.system(scriptfilename)
    else:
        print("Beamline name - %s - not recognised" % beamline['BEAMLINE'][0])





