
# ring correction by some sort of more clever double flatfield

import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from skimage import morphology
from scipy import signal
import pickle

def double_ff_nc(scanname, activate=True, pixelmask=0.95, nsteps=72, filtx=11, filtz=5, interactive=True):

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    if interactive:
        selection = input("Disactivate the non constant ring correction? y/[n] : ")
        activate = False if selection.lower() in ['y', 'yes', 'true'] else True
        

    # update the parfile...
    if activate:
        # remove the "normal" ff correction
        pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
        # image paths    
        pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'cor')    
        pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
        pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
        par_tools.par_write(parname, pars)  
    else:
         # image paths    
        pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + scanname)    
        pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'YES')
        pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'YES')   
        print('note - run regular double flatfield correction again if required')
        par_tools.par_write(parname, pars)  
        return

    # read the references
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    refA = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'ref000000.edf').getData()*1.
    refB = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).getData()*1.
    dark = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'dark.edf').getData()*1.

    # get the original double ff correction
    if os.path.exists(scanname + os.sep + scanname + "_edfs"  + os.sep + 'ff.edf'):
        orig_ff = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + 'ff.edf').getData()
    else:
        print("need to run the regular double flatfield correction first!")
        return

    # make the bad pixel map using refB
    if pixelmask == 0:
        print("No pixel mask - correcting all pixels")
        mymap = np.ones(refB.shape)
    else:
        print("make the map of bad pixels (using threshold pixelmask = %0.3f)" % pixelmask)
        refB_filt = signal.medfilt2d(refB, [9,9])
        mymap = (refB_filt / refB)<pixelmask # is a map of bad pixels...
        mymap2 = morphology.binary_dilation(mymap) # dilate once
        mymap3 = morphology.binary_dilation(mymap2) # dilate twice
        mymap4 = morphology.binary_dilation(mymap3) # dilate thrice
        mymap = ((mymap*1.) + mymap2 + mymap3 + mymap4) / 4.
    
    
    # make the mean stack
    make_stack = False # read it if possible
    if os.path.exists(scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle'):
        print("read the existing mean_stack")
        im_mean = pickle.load(open(scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle', 'rb'))
        if im_mean.shape[2] != nsteps:
            print("nsteps has changed - recreate the mean_stack")
            make_stack = True
    else:
        make_stack = True

    if interactive and not make_stack:
        selection = input("Recreate the existing mean_stack ? y/[n] : ")
        make_stack = True if selection.lower() in ['y', 'yes', 'true'] else False
    
    imperstep = nproj / nsteps  
    if make_stack:
        # read every third image
        im_mean = np.zeros((refA.shape[0], refA.shape[1], nsteps))        
        substep = 3 # leave this hard coded
        print("create the filtered mean image")
        for kk in range(0, nsteps):
            imtmp = np.zeros(refA.shape)
            for jj in range(0, imperstep, substep):
                ii = (kk*imperstep) + jj
                im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
                f = float(ii)/nproj
                ref = refB*f + refA*(1-f)
                im = (im-dark)/(ref-dark)
                # do the original ff correction
                im = im * np.exp(orig_ff)
                imtmp = imtmp + im
            im_mean[:,:,kk] = imtmp / len(range(0, imperstep, substep))
        # save out the im_mean to save recreating it
        print("save the mean_stack")
        pickle.dump(im_mean, open(scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle', 'wb'))
        
    # deal with the filter parameters
    if interactive and (filtx==11 and filtz==5):
        selection = input("Enter the x (horizontal) filter length in pixels [11] : ")
        filtx = 11 if selection=="" else int(selection)
        selection = input("Enter the z (vertical) filter length in pixels [5] : ")
        filtz = 5 if selection=="" else int(selection)
    if np.mod(filtx, 2)==0:
        filtx = filtx+1
        print("Add one to filter length to make it odd")
    if np.mod(filtz, 2)==0:
        filtz = filtz+1
        print("Add one to filter length to make it odd")
    filt = [filtz, filtx]
    print("using filter [%d, %d]" % (filt[0], filt[1]))

    im_mean_filt = np.zeros(im_mean.shape)
    sys.stdout.write('filter mean stack\n    ')
    for ii in range(im_mean.shape[2]):
        im_mean_filt[:,:,ii] = signal.medfilt2d(im_mean[:,:,ii], filt) 
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()
    ff = np.float32(np.log(im_mean_filt/im_mean))
    ff[np.nonzero(np.isinf(ff))]=0

    # apply the bad pixel mask
    ff2 = ff * 1.
    for ii in range(im_mean.shape[2]):
        ff2[:,:,ii] = ff[:,:,ii] * mymap

    # treat all the images
    sys.stdout.write('treat images\n    ')
    for ii in range(nproj):
        im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
        f = float(ii)/nproj
        ref = refB*f + refA*(1-f)
        im = (im-dark)/(ref-dark)
        # do the original ff correction
        im = im * np.exp(orig_ff)
        # individual correction
        if ii < imperstep:
            myff = ff2[:,:,0]
        elif ii > ((nsteps-1) * imperstep):
            myff = ff2[:,:,-1]
        else:
            #ndx = ((float(ii)/nproj) * nsteps) - 0.5
            ndx = float(ii) / imperstep        
            ndx1 = np.floor(ndx)
            #ndx2 = np.ceil(ndx)
            #f = np.mod(ndx, 1)
            #myff = ff2[:,:,ndx1]*(1-f) + ff2[:,:,ndx2]*f
            myff = ff2[:,:,ndx1]
        # write the results
        edf.setData(np.float32(im * np.exp(myff)))
        edf.write(scanname + os.sep + scanname + "_edfs"  + os.sep + ('cor%06d.edf' % ii))       
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Make, activate or deactivate double flatfield ring correction for non constant rings")
    parser.add_argument("scanname", help="The name of the scan you want to treat", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-a", "--activate", help="Activate (or disactivate) an existing correction [True]/False", default=True, type=testbool)
    parser.add_argument("-n", "--nsteps", help="Number of steps in angle", default=36, type=int)
    parser.add_argument("-p", "--pixelmask", help="Threshold for bad pixels (0=all pixels)", default=0.95, type=float)
    parser.add_argument("-x", "--filtx", help="Filter size (x, hor) for correction", default=9, type=int)
    parser.add_argument("-z", "--filtz", help="Filter size (z, ver) for correction", default=9, type=int)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    double_ff_nc(args.scanname, activate=args.activate, pixelmask=args.pixelmask, nsteps=args.nsteps, filtx=args.filtx, filtz=args.filtz, spiralsize=args.spiralsize, interactive=args.interactive)

    # ----------------------------------------------------- #
    
    



