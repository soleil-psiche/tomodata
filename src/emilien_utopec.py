
import argparse
import par_tools
import os
from fabio.edfimage import edfimage
edf = edfimage()
import numpy as np
import sys
import reconstruct_and_view
import glob
import pickle

def emilien_utopec(scanname, interactive=True):
    '''
    Filter a reconstructed volume using Emilien's fft filter
    '''
    # did we already run the utopec function, to remove partially truncated projections and locate columns?
    if os.path.exists('%s/utopec_replaced.pickle' % scanname):
        rep = pickle.load(open('%s/utopec_replaced.pickle' % scanname, 'rb'))
    else:
        print("Run UToPEC function first, to remove partially truncated projections and locate columns")
        #return
    
    # has the reconstruction been done?
    # pag or not pag?
    if os.path.exists('%s/%s_0_pag.vol.info' % (scanname, scanname)):
        volname = '%s/%s_0_pag.vol' % (scanname, scanname)
    elif os.path.exists('%s/%s_0.vol.info' % (scanname, scanname)):
        volname = '%s/%s_0.vol' % (scanname, scanname)
    else:
        print("No reconstructed volume - run reconstruction first")
        #return

    # read the parfile
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)  
    xsize = int(pars['END_VOXEL_1'][0]) - int(pars['START_VOXEL_1'][0]) + 1
    ysize = int(pars['END_VOXEL_2'][0]) - int(pars['START_VOXEL_2'][0]) + 1
    zsize = int(pars['END_VOXEL_3'][0]) - int(pars['START_VOXEL_3'][0]) + 1
    if xsize!=ysize:
        print("WARNING - RECONSTRUCTION IS NOT SQUARE... MAY NOT WORK!")

    # now, calculate the filter
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    tmp = np.zeros(nproj)
    for ii in rep:
        tmp[ii] = 1
    # there will be special cases when this doesn't work - column cutting 0 degrees
    column_centre = (np.arange(len(tmp)) * tmp).sum() / tmp.sum()
    # parameters, using Emilien names:
    theta_A = np.deg2rad(column_centre*float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]))
    Phi = np.deg2rad((180-10)/2.) # standard Utopec column width?
    eps = np.deg2rad(25.) # standard smoothness parameter?
    gamma = 6. # centre hole in pixels - low frequency pass. # sigma, may need to convert this to FWHM

    # filter
    x = np.arange(xsize)
    xx, yy = np.meshgrid(x, x)
    radius = np.sqrt((yy - xsize/2.)**2 + (xx - xsize/2.)**2)

    # gaussian low frequency pass
    lowpass = np.exp(-((radius**2) / (2 * gamma**2)))

    # Frikel mask 
    # if the column_centre is zero, the columns are parallel to the beam at zero degrees, 
    # and vertical in the reconstruction.
    # theta is the angle, and we measure from the centre of the opening (i.e. 90 from the columns)
    # we want values running from 0 to pi/2 (90) in each of 4 quadrants
    thetas = np.arctan2(yy - xsize/2., xx - xsize/2.) # zero horizontal, -pi-+pi
    thetap = thetas + theta_A + np.pi/2 # rotate to match 
    thetap = np.mod(thetap, np.pi*2) # put on 0-2pi (0-360)
    thetap = thetap - np.pi # put on -pi-+pi
    # for the function, -pi == 0 == pi, so we can take the abs
    thetap = np.abs(thetap)
    thetap = np.pi/2 - np.abs(thetap - np.pi/2)
    # this gives what we want!
    
    # calculate x. 
    x = thetap - (Phi - eps)
    # calculate the Frikel function
    Frikel = np.exp(x**2 / (x**2 - eps**2))
    Frikel[np.nonzero(thetap<(Phi-eps))] = 1
    Frikel[np.nonzero(thetap>Phi)] = 0

    # add the gamma function
    Frikel = 1 - ((1-Frikel)*(1-lowpass))

    # open the reconstructed volume as a memmap
    fread = np.memmap(volname, dtype=np.float32, mode='r', shape=(xsize, ysize, zsize), order='F')
    fwrite = np.memmap(volname+'_filtered', dtype=np.float32, mode='w+', shape=(xsize, ysize, zsize), order='F')

    sys.stdout.write('filtering...\n Done: 0000')
    for ii in range(zsize):
        ii
        sys.stdout.write('\b\b\b\b%04d' % ii)
        im = fread[:,:,ii].T
        imfft = np.fft.fftshift(np.fft.fft2(im))
        imfilt = np.abs(np.fft.ifft2(imfft*Frikel))
        fwrite[:,:,ii] = np.float32(imfilt).T
        fwrite.flush()
        sys.stdout.flush()
    del fwrite
    del fread

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Run Emilien\'s filter on a volume")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    emilien_utopec(args.scanname, interactive=args.interactive)

    # ----------------------------------------------------- #


