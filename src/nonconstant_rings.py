
# ring correction by some sort of more clever double flatfield
import multiprocessing as mp
import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from skimage import morphology, transform
from scipy import signal
import pickle

# update to new ff filename convention

# possibly need something to stop it from killing contrast in the middle.

def nonconstant_rings(scanname, activate=True, pixelmask=0.95, nsteps=8, filtx=25, filtz=15, ncores=8, interactive=True):

    # read the par file
    expdir = os.getcwd()
    parname = expdir + os.sep + scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    

    if interactive:
        selection = input("Disactivate the non constant ring correction? y/[n] : ")
        activate = False if selection.lower() in ['y', 'yes', 'true'] else True
        print('note - run regular double flatfield correction again if required')
	

    # update the parfile...
    if activate:
        # remove the "normal" ff correction
        pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
        # image paths    
        pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'cor')    
        pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
        pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
        # save the updated pars!
        par_tools.par_write(parname, pars)
    else:
         # image paths    
        pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + scanname)    
        pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'YES')
        pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'YES')       
        # save the updated pars!
        par_tools.par_write(parname, pars)
        return # finished here!

    if interactive:
        selection = input("Value for pixelmask (<1, lower is more selective. 0==no mask) ? [0] : ")
        pixelmask = 0 if selection.lower() in ['', '0'] else float(selection)
        selection = input("Number of correction steps through scan (0=normal ring correction) ? [8] : ")
        nsteps = 8 if selection.lower() in ['', '8'] else int(selection)
        # deal with the filter parameters
        selection = input("Enter the x (horizontal) filter length in pixels [9] : ")
        filtx = 9 if selection=="" else int(selection)
        selection = input("Enter the z (vertical) filter length in pixels [9] : ")
        filtz = 9 if selection=="" else int(selection)
        if np.mod(filtx, 2)==0:
            filtx = filtx+1
            print("Add one to filter length to make it odd")
        if np.mod(filtz, 2)==0:
            filtz = filtz+1
            print("Add one to filter length to make it odd")
    filt = [filtz, filtx]
    print("using filter [%d, %d]" % (filt[0], filt[1]))

    # read the references
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    refA = edf.read(expdir + os.sep + scanname + os.sep + scanname + "_edfs" + os.sep +'ref000000.edf').getData()*1.
    refB = edf.read(expdir + os.sep + scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).getData()*1.
    dark = edf.read(expdir + os.sep + scanname + os.sep + scanname + "_edfs" + os.sep +'dark.edf').getData()*1.
    sizex = int(pars['NUM_IMAGE_1'][0])
    sizey = int(pars['NUM_IMAGE_2'][0])
    # get the original double ff correction

    # make the bad pixel map using refB
    if pixelmask==0:
        print("No pixel mask : correcting all pixels")
        mymap = np.ones(refB.shape)
    else:
        print("Make the map of bad pixels (using threshold pixelmask = %0.3f)" % pixelmask)
        refB_filt = signal.medfilt2d(refB, [9,9])
        mymap = (refB_filt / refB)<pixelmask # is a map of bad pixels...
        mymap2 = morphology.binary_dilation(mymap) # dilate once
        mymap3 = morphology.binary_dilation(mymap2) # dilate twice
        mymap4 = morphology.binary_dilation(mymap3) # dilate thrice
        mymap = ((mymap*1.) + mymap2 + mymap3 + mymap4) / 4.
        
    # make the mean stack
    make_stack = False # read it if possible
    if os.path.exists(expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle'):
        print("read the existing mean_stack")
        im_mean = pickle.load(open(expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle', 'rb'))
        if im_mean.shape[2] != nsteps:
            print("nsteps has changed - recreate the mean_stack")
            make_stack = True
    else:
        make_stack = True

    if interactive and not make_stack:
        selection = input("Recreate the existing mean_stack ? y/[n] : ")
        make_stack = True if selection.lower() in ['y', 'yes', 'true'] else False


    imperstep = nproj / nsteps    
    if make_stack:
        
        # multiprocessing to make mean
        p = mp.Pool(ncores)
        # job details
        joblength = int(nproj / nsteps)
        starts = np.arange(0, nproj-joblength+1, joblength)
        ends = starts + joblength
        ends[-1] = nproj
        n = len(starts)
        step = 3
        myinfo = zip([scanname]*n, starts, ends, [step]*n, [sizex]*n, [sizey]*n, [refA]*n, [refB]*n, [dark]*n, [nproj]*n, [expdir]*n)
        # read the images
        print("Reading using multiprocess with %d processes to go faster" % ncores)
        blocks = p.map(make_mean, myinfo)
        print("close the pool")
        # close the pool?
        p.close()
        # read the results
        im_mean = np.zeros((refA.shape[0], refA.shape[1], nsteps))
        for ii in range(nsteps):
            im_mean[:,:,ii] = blocks[ii]

        # save out the im_mean to save recreating it
        print("save the mean_stack")
        pickle.dump(im_mean, open(expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + 'mean_stack.pickle', 'wb'))
        

    # multiprocessing to filter stack
    p = mp.Pool(ncores)
    # job details
    n = nsteps
    myinfo = zip([im_mean[:,:,ii] for ii in range(nsteps)], [filt]*n, [sizex]*n, [sizey]*n)
    # read the images
    print("Filter mean stack using multiprocess with %d processes to go faster" % ncores)
    blocks = p.map(filter_mean, myinfo)
    print("close the pool")
    # close the pool?
    p.close()
    # read the results
    im_mean_filt = np.zeros(im_mean.shape)
    for ii in range(nsteps):
        im_mean_filt[:,:,ii] = blocks[ii]



        #im_mean_filt[:,:,ii] = signal.medfilt2d(im_mean[:,:,ii], filt) 
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()
    ff = np.float32(np.log(im_mean_filt/im_mean))
    ff[np.nonzero(np.isinf(ff))]=0

    # apply the bad pixel mask
    ff2 = ff * 1.
    if pixelmask!=0:
        for ii in range(im_mean.shape[2]):
            ff2[:,:,ii] = ff[:,:,ii] * mymap2


    # treat all the image with a pool
    p = mp.Pool(ncores)
    # job details
    joblength = int(nproj / ncores)
    starts = np.arange(0, nproj-joblength+1, joblength)
    ends = starts + joblength
    ends[-1] = nproj # make sure we get the last image
    n = len(starts)
    myinfo = zip([scanname]*n, starts, ends, [ff2]*n, [refA]*n, [refB]*n, [dark]*n, [nproj]*n, [imperstep]*n, [nsteps]*n, [expdir]*n)
    # read the images
    print("Correct images using multiprocess with %d processes to go faster" % ncores)
    t0 = time.time()
    blocks = p.map(filter_images, myinfo)
    print("close the pool")
    # close the pool?
    p.close()
    t1 = time.time()
    print('time was %0.1f seconds' % (t1-t0))


# to be used by multiprocess
def filter_mean(info):
    # median filter for stack
    im_mean = info[0]
    filt = info[1]
    sizex = info[2]
    sizey = info[3]

    if (filt[0]==-1) & (filt[1]==-1):
        # -1 to indicate homogeneous correction
        # we take simply the mean value as a "homogeneous" smooth mean image
        im_mean_filt = np.ones(im_mean.shape) * im_mean.mean()
    elif (filt[0]<30) & (filt[1]<30):
        im_mean_filt = signal.medfilt2d(im_mean, filt) 
    else:
        # large filts, so downsize first
        sizey_ds = int(np.floor((sizey / 4)))
        sizex_ds = int(np.floor((sizex / 4)))
        filtx_ds = int(np.floor((filt[0] / 4)))
        filtz_ds = int(np.floor((filt[1] / 4)))
        if np.mod(filtx_ds, 2)==0:
            filtx_ds+=1
        if np.mod(filtz_ds, 2)==0:
            filtz_ds+=1
        filt_ds = [filtz_ds, filtx_ds]
        # skimage wants float values between -1 and +1
        #im_mean_min = im_mean.min()
        #im_mean_max = im_mean.max()
        # put on 0 to +1
        #im_mean = (im_mean - im_mean_min) / (im_mean_max - im_mean_min)
        im_mean[np.nonzero(im_mean>1.)] = 1.
        im_mean_ds = transform.resize(im_mean, [sizey_ds, sizex_ds], mode='reflect')                
        im_mean_filt_ds = signal.medfilt2d(im_mean_ds, filt_ds)
        # fix the corners
        im_mean_filt_ds[np.nonzero(im_mean_filt_ds==0)] = im_mean_ds[np.nonzero(im_mean_filt_ds==0)]
        im_mean_filt = transform.resize(im_mean_filt_ds, [sizey, sizex], mode='reflect')

    return im_mean_filt




# to be used by multiprocess
def filter_images(info):
    # info : scanname, first, last
    scanname = info[0]
    first = info[1]
    last = info[2]    
    ffstack = info[3]
    refA = info[4]
    refB = info[5]
    dark = info[6]
    nproj = info[7]
    imperstep = info[8]
    nsteps = info[9]
    expdir = info[10]

    for ii in range(first, last):
        # flatfield
        im = edf.read(expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
        f = float(ii)/nproj
        ref = refB*f + refA*(1-f)
        im = (im-dark)/(ref-dark)
        # individual correction
        if ii < (imperstep/2):
            myff = ffstack[:,:,0]
        elif ii > ((nsteps-0.5) * imperstep):
            myff = ffstack[:,:,-1]
        else:
            ndx = (float(ii) / imperstep) - 0.5        
            ndx1 = int(np.floor(ndx))
            ndx2 = int(np.ceil(ndx))
            f = np.mod(ndx, 1)
            myff = ffstack[:,:,ndx1]*(1-f) + ffstack[:,:,ndx2]*f
            #myff = ffstack[:,:,ndx1]
        # write the results
        edf.setData(np.float32(im * np.exp(myff)))
        edf.write(scanname + os.sep + scanname + "_edfs"  + os.sep + ('cor%06d.edf' % ii))       
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()


# to be used by multiprocess
def make_mean(info):
    # info : scanname, first, last, step, refA, refB, dark, nproj
    scanname = info[0]
    first = info[1]
    last = info[2]
    step = info[3]
    xsize = info[4]
    ysize = info[5]
    refA = info[6]
    refB = info[7]
    dark = info[8]
    nproj = info[9]
    expdir = info[10]

    im_mean = np.zeros((ysize, xsize), np.float32)
    count = 0.0
    for ii in range(first, last, step):
        # this is just simple adding all projections
        im = np.float32(edf.read(expdir + os.sep + scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData())
        im_mean = im_mean + im
        count+=1
    im_mean = im_mean / count
    # make average ref
    meanii = (first + last) / 2. # average scan position
    f = float(meanii)/nproj
    ref = refB*f + refA*(1-f)
    im_mean = (im_mean - dark) / (ref - dark)
    return im_mean



def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Make, activate or deactivate double flatfield ring correction for non constant rings")
    parser.add_argument("scanname", help="The name of the scan you want to treat", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-a", "--activate", help="Activate (or disactivate) an existing correction [True]/False", default=True, type=testbool)
    parser.add_argument("-n", "--nsteps", help="Number of steps in angle", default=36, type=int)
    parser.add_argument("-c", "--ncores", help="Number of cores for multiprocess", default=8, type=int)
    parser.add_argument("-p", "--pixelmask", help="Threshold for bad pixels (0=all pixels corrected)", default=0.95, type=float)
    parser.add_argument("-x", "--filtx", help="Filter size (x, hor) for correction", default=9, type=int)
    parser.add_argument("-z", "--filtz", help="Filter size (z, ver) for correction", default=9, type=int)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    nonconstant_rings(args.scanname, activate=args.activate, pixelmask=args.pixelmask, nsteps=args.nsteps, filtx=args.filtx, filtz=args.filtz, ncores=args.ncores, interactive=args.interactive)

    # ----------------------------------------------------- #
    
    



