
import argparse
import par_tools
import os

def total_variation(scanname, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    # write this to the par file
    pars = par_tools.par_mod(pars, 'BETA_TV', 0.2)
    pars = par_tools.par_mod(pars, 'FISTA', 1)
    pars = par_tools.par_mod(pars, 'ITERATIVE_CORRECTIONS', 20)
    pars = par_tools.par_mod(pars, 'ITERATIVE_CORRECTIONS_NOPREC', 1)
    pars = par_tools.par_mod(pars, 'DENOISING_TYPE', 1)
    pars = par_tools.par_mod(pars,'OPTIONS', '{ \'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'Y\'}')

    par_tools.par_write(parname, pars)
    print("Wrote hard coded TV parameters to par file...")



def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set total variation parameters for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    total_variation(args.scanname, interactive=args.interactive)

    # ----------------------------------------------------- #
