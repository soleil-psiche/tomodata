# it seems to be that the nxs file arrives before the synchro_log gets updated.
# So could start processing the latest nxs, and wait for the log, or - easier - do 
# the latest complete pair

# updated version 2, hopefully improved, rename when validated.

# can we add spirals ?

import preprocess_fast
import preprocess_all
import preprocess_spiral
import paganin
import rotation_axis
import double_ff
import reconstruct_and_view
from tkinter import filedialog
import glob
import os
import view_only
import time
import pylab
import numpy as np
pylab.ion()

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

# have tried to adapt to the recordingmanager version of tomo_spiral

def live_preview(mydirectory=None, fast=False, spiral=False, instructions=None, interactive=True):
    """
    Live automatic reconstruction of everything in a fast dataset? or an experiment directory?
    Should probably use the same syntax as reconstruct_all (i.e. a protocol.txt)
    """

    # intro, get spool directory
    curdir = os.getcwd()
    pieces = curdir.split(os.sep)
    expname = pieces[-1]
    # test - is the start of the curdir out reconstruction directory?
    if curdir[0:len(beamline['RECONSTRUCTION_PATH'][0])] != beamline['RECONSTRUCTION_PATH'][0]:
        print("\n\n\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
        print("  You should launch live_preview from your experiment directory")
        print("  Usually %s/.../Name_MMYY" % beamline['RECONSTRUCTION_PATH'][0])
        print("  Currently in : %s" % curdir)
        print("  quitting..."    )
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n")
        sys.exit()
    else:
        print("\n\n\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
        print("  Live_preview for experiment %s on %s" % (expname,  beamline['BEAMLINE'][0]))
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n")
    myspooldir = beamline['DEFAULT_SPOOL_DIRECTORY'][0] + curdir[len(beamline['RECONSTRUCTION_PATH'][0]):]


    if interactive:
        # fast series or not?
        selection = input("Do you want to follow a fast sequence (no = normal scans) y/[n] : ")
        fast = False if selection.lower() in ['', 'n', 'no', 'false', '0'] else True
        selection = input("Do you want to follow spiral scans (no = normal scans) y/[n] : ")
        spiral = False if selection.lower() in ['', 'n', 'no', 'false', '0'] else True
        selection = input("Which slice to reconstruct? [default=0, centre slice] : ")
        slicendx = 0 if selection == '' else int(selection)

        # get the directory to watch
        mydirectory = filedialog.askdirectory(initialdir=myspooldir, title='select the data directory (fast scan directory or spool directory)')
        if len(mydirectory) == 0: # we cancelled
            return
        else:
            # this depends if it is a fast sequence or not
            if fast:
                fastnameroot = mydirectory.split(os.sep)[-1]
                spooldir = mydirectory[0:mydirectory.rfind(os.sep)]
            else:
                spooldir = mydirectory


        filename = filedialog.askopenfilename(title='select an instructions file')
        fh = open(filename, 'rt')
        instructionsA = fh.readlines()
        fh.close()
        # remove end of line characters
        instructions = []
        for name in instructionsA:
            instructions.append(name.strip())
    else:
        if (mydirectory==None) or (instructions==None):
            print("need to supply directory and instructions")
            return

    # loop over the directory, reconstructing the most recent scan
    loop = True
    myimages = [] # keep a list of the last 6 images
    for ii in range(6):
        myimages.append(np.zeros((2048, 2048)))
    myfignum = 0
    pylab.figure(figsize=(25, 15)) # big
    pylab.subplot(2,3,1)
    pylab.gray()
    
    while loop:
        found_new_tomo = False
        try:
            ##########################################################################################
            # Case for a fast sequence...
            if fast:
      
                # get the list of scans in the fast directory
                fullscannames = glob.glob(spooldir + os.sep + fastnameroot + os.sep + fastnameroot.lower() + "_*.nxs")
                fullscannames = sorted(fullscannames)
                # read the angles file
                fh = open(spooldir + os.sep + fastnameroot + os.sep + fastnameroot + '.synchro_log', 'rt')
                lines = fh.readlines()
                fh.close()
                if (len(lines) != len(fullscannames)):
                    print(".synchro_log file was inconsistent with the data - Do the last complete scan/angle pair")
    
                # this might have "refs" in it for intermediate references
                angledata = np.zeros(len(lines))
                for ii in range(len(lines)):
                    word = lines[ii].strip().split(', ')[1]
                    if word == 'refs':
                        angledata[ii] = np.nan
                    else:
                        angledata[ii] = float(word)

                # get the name and number of the latest .nxs
                if len(fullscannames)>0:
                    lastnxs = fullscannames[-1].split(os.sep)[-1]
                    lastnxsstep = int(lastnxs[-10:-4]) # read the integer part
                    lastname = lastnxs[0:-11] # strip off the integer part
                else:
                    lastnxsstep = 0
                # last synchro_log step
                lastlogstep = len(lines) # file numbering starts at 1, len() is right!
                # last complete record
                laststep = np.min((lastnxsstep, lastlogstep))
                print('###########')

                # find the last non-reference
                search = True
                while search:
                    if laststep>=1:
                        if np.isnan(angledata[laststep-1]):
                            print("final step %d was a reference, look to one before" % laststep)
                            laststep-=1  
                            search = True # keep looking
                        else:
                            search = False # found something 
                            doprepro = True             
                    else:
                        search = False # didn't find anything
                        doprepro = False             

                if doprepro:
                    # has this latest scan been reconstructed ?
                    if not os.path.exists('%s_fast_%d_' % (fastnameroot, laststep)):
                        found_new_tomo = True
                        # preprocess the latest - fastnameroot has any capital letters
                        myscanname = preprocess_fast.preprocess_fast(scannameroot=fastnameroot, step=laststep, spooldir=spooldir, interactive=False)
                        lastname = myscanname
            # End of case for a fast sequence...
            ##########################################################################################
            # Case for spiral scans...
            elif spiral:
                # get the list of spiral scans in time order - after acquisition are called *_spiral
                # adapt this to the recordingmanager version with (un)finished_spiral flag
                fullscannames = glob.glob(spooldir + os.sep + "*spiral/finished_spiral")
                fullscannames.sort(key=os.path.getctime)
                lastname = fullscannames[-1].split(os.sep)[-2] # use -2 while testing
                # has this latest scan been reconstructed ?
                if not os.path.exists(lastname):
                    found_new_tomo = True
                    preprocess_spiral.preprocess_spiral(scannameroot=lastname, spooldir=spooldir, interactive=False)
            # End of case for a spiral scan
            ##########################################################################################
            # Case for a normal scan...
            else:
                # get the list of normal scans in time order - after acquisition these have a par file
                fullscannames = glob.glob(spooldir + os.sep + "*/*.par")
                fullscannames.sort(key=os.path.getctime)
                lastname = fullscannames[-1].split(os.sep)[-1]
                lastname = lastname.split('.')[0] # get rid of the .par
    
                # what kind of data do we have?
                nxslist = glob.glob(spooldir + os.sep + lastname +os.sep+'*.nxs')
                if os.path.exists(spooldir + os.sep + lastname +os.sep+'images'):
                    myformat = 'edf'
                else:
                    myformat = 'hdf5'

                if myformat == 'hdf5':
                    # has the dark been done?
                    if os.path.exists(spooldir + os.sep + lastname +os.sep+'post_dark.nxs'):
                        darks_ready = True
                    else:
                        darks_ready = False    

                # has this latest scan been reconstructed ?
                if not os.path.exists(lastname) and darks_ready:
                    found_new_tomo = True
                    preprocess_all.preprocess(scanname=lastname, accfac=-1, format=myformat, bin=[1,1,1], spooldir=spooldir, interactive=False)
            # End of case for a normal scan
            ##########################################################################################
            # Now, reconstruct a slice from the last thing we preprocessed
            if found_new_tomo:
                print('Apply instructions to %s' % lastname)
                for instruction in instructions:
                    # function name
                    myinstruction = instruction.replace('XXX', lastname)
                    os.system(myinstruction)
                
                # reconstruct and view 
                reconstruct_and_view.reconstruct_and_view(lastname, slicendx=slicendx, timeout=60, interactive=False)
                myslice = view_only.view_only(lastname, interactive=False)
                mymin = myslice.mean()-(3*myslice.std())    
                mymax = myslice.mean()+(3*myslice.std())
                ndx = np.mod(myfignum, 6)
                myfignum+=1 # for the next time
                myimages[ndx] = myslice
                pylab.subplot(2,3,ndx+1)
                pylab.imshow(myslice)
                pylab.clim(mymin, mymax)
                pylab.title(lastname)
                pylab.pause(0.1)


            else:
                print("Waiting for new data...  Ctrl - c to stop loop")
                pylab.draw()
                time.sleep(5)

        except KeyboardInterrupt:
            loop = False

    # after the loop
    print("Finished live preview")

            
def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="live preview - watch a directory, and reconstruct slices of the most recent acquisition using a given protocol")
    parser.add_argument("directory", help="The directory to watch", default=None, type=str)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-f", "--fast", help="fast acquistion", default=False, type=testbool)
    parser.add_argument("-p", "--protocol", help="protocol file", default=None, type=str)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    live_preview(mydirectory=args.directory, fast=args.fast, instructions=args.protocol, interactive=args.interactive)

    # ----------------------------------------------------- #
