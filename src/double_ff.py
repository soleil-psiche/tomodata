# python function to check create or manage double flatfield ring corrrection
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# should save the mean image for fast processings
# should save the ff.edf correction image somewhere with a unique name so it can be backed up

# this should downsize before filtering for large filters

# add multiprocess to speed up making the mean image

import multiprocessing as mp
import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from scipy import signal
from skimage import morphology, transform


def double_ff(scanname, activate=True, filtx=9, filtz=9, spiralsize=0, step=3, dotubes=False, pixelmask=0, homogeneous=False, interactive=True):

    # for multiprocessing
    if interactive:
        ncores = 16
        njobs = 16        
    else:
        ncores = 4 # it seems like launching too many processes causes hanging in slurm
        njobs = 16

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    expdir = os.getcwd()

    # Are we disactivating?
    if not activate:
        pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
        par_tools.par_write(parname, pars)
        print("Disactivated double flatfield correction")
        return

    # is there an existing ff.edf ?
    flatfieldname = expdir + os.sep + scanname + os.sep + scanname + "_edfs" + os.sep + scanname + "_ff.edf"
    if os.path.exists(flatfieldname) and interactive:
        selection = input("Use existing double flatfield correction ? [y]/n : ")
        selection = "y" if selection=="" else selection
        if selection == "y":
            pars = par_tools.par_mod(pars, 'DOUBLEFFCORRECTION', '\"%s\"' % flatfieldname)
            par_tools.par_write(parname, pars)
            print("Activated double flatfield correction")
            return
        else:
            selection = input("Disactivate the existing correction ? [y]/n : ")
            selection = "y" if selection=="" else selection
            if selection == "y":
                pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
                par_tools.par_write(parname, pars)
                print("Disactivated double flatfield correction")
                return            
    # correct the tubes only?
    if interactive:
        selection = input("Special correction for the sCMOS lines (tubes)? y/[n] : ")
        dotubes=True if selection.lower() in ['y', 'yes', 'true'] else False
    # make and use a pixelmask?
    if interactive:
        selection = input("Use a pixel mask? (i.e. correct only badly saturated spots)  y/[n] : ")
        tmp=True if selection.lower() in ['y', 'yes', 'true'] else False
        if tmp:
            selection = input("Threshold for mask [65353] : ")
            pixelmask = 65353 if selection == '' else int(selection)
        else:
            pixelmask=0 # no pixel mask
    # Special case for homogeneous samples?
    if interactive:
        selection = input("Homogeneous sample (i.e. low contrast, local tomography)  y/[n] : ")
        homogeneous=True if selection.lower() in ['y', 'yes', 'true'] else False

    # otherwise, make the correction - no filter required for tubes only and homogeneous cases
    if not homogeneous: 
        if interactive and (filtx==9 and filtz==9):
            selection = input("Enter the x (horizontal) filter length in pixels [9] : ")
            filtx = 9 if selection=="" else int(selection)
            selection = input("Enter the z (vertical) filter length in pixels [9] : ")
            filtz = 9 if selection=="" else int(selection)
        if np.mod(filtx, 2)==0:
            filtx = filtx+1
            print("Add one to filter length to make it odd")
        if np.mod(filtz, 2)==0:
            filtz = filtz+1
            print("Add one to filter length to make it odd")
        filt = [filtz, filtx]
        print("using filter [%d, %d]" % (filt[0], filt[1]))
    
    # is this a spiral acquisition wih different pattern of lines?
    if interactive and spiralsize==0:
        selection = input("Is this a spiral acquisition? y/[n] : ")
        selection = "n" if selection=="" else selection
        if selection == 'y':
            selection = input("Enter the original vertical image dimension [600] : ")
            spiralsize = 600 if selection=="" else int(selection)
            print("Correcting spiral acquisition; original vertical image size %d" % spiralsize)

    sizex = int(pars['NUM_IMAGE_1'][0])
    sizey = int(pars['NUM_IMAGE_2'][0])

    # does the mean already exist?
    test = os.path.isfile(scanname + os.sep + scanname + "_edfs" + os.sep +'mean.edf')
    if test and interactive:
        selection = input("A mean image already exists! Recreate it? y/[n] : ")
        selection = "y" if selection.lower() in ['y', 'yes', 'true'] else 'n'
    elif test:
        print("will use the existing mean image")
        selection = "n" # mean doesn't exist, so create it - default for non-interactive too

    if test and selection=='n':
        # read the existing mean
        im_mean = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'mean.edf').data *1.
    else:
        # read the references
        nproj = int(pars['NUM_LAST_IMAGE'][0])
        lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
        refA = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'ref000000.edf').data *1.
        refB = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).data *1.
        dark = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'dark.edf').data *1.
        if interactive and (step==3):
            selection = input("Read every Nth image to make mean [3] : ")
            step = 3 if selection=="" else int(selection)
        # multiprocessing to make mean
        p = mp.Pool(ncores)
        # job details
        joblength = int(nproj / njobs)
        starts = np.arange(0, nproj-joblength+1, joblength)
        ends = starts + joblength
        ends[-1] = nproj
        n = len(starts)
        myinfo = zip([scanname]*n, starts, ends, [step]*n, [sizex]*n, [sizey]*n)
        # read the images
        print("Reading using multiprocess with %d processes to go faster" % ncores)
        blocks = p.map(make_sum, myinfo)
        print("close the pool")
        # close the pool?
        p.close()
        ## deal with the results - this works
        #im_mean = np.zeros(refA.shape, np.float32)
        #im_mean2 = np.zeros(refA.shape, np.float32)
        #count=0
        #count2=0      
        #for ii in range(n):
        #    im_mean = im_mean + blocks[ii][0]
        #    count = count + blocks[ii][1]
        #    im_mean2 = im_mean2 + blocks[ii][2]
        #    count2 = count2 + blocks[ii][3]
        # note - if the anvil gaps are closed, we can fail to find any "good" images
        #if count > (nproj/20):
        #    im_mean = im_mean / count
        #else:
        #    im_mean = im_mean2 / count2
        # flatfield everything at once - note that we assume we have a good average over the scan
        #ref = (refA + refB) / 2 # scan average reference
        #im_mean = (im_mean - dark) / (ref - dark)
        #im_mean = np.float32(im_mean)

        stack = np.zeros((refA.shape[0], refA.shape[1], n))
        for ii in range(n):
            im = blocks[ii][2] / blocks[ii][3]
            ndx = (starts[ii] + ends[ii]) / 2. 
            f = ndx/nproj
            ref = refB*f + refA*(1-f)
            im = (im-dark)/(ref-dark)
            stack[:,:,ii] = im
        # try the median of this stack
        stack.sort(2)
        im_mean = np.float32(stack[:,:,2:-2].mean(2))
        print("Try using a mean without outliers... may need to look at this again...")
        ## make the mean
        #im_mean = np.zeros(refA.shape)
        #im_mean2 = np.zeros(refA.shape)
        ## read every tenth image
        #print("create the filtered mean image - avoid completely dark images")
        #count=0
        #count2=0
        #sys.stdout.write('\nReading...     ')    
        #for ii in range(0, nproj, step):
        #    sys.stdout.write('\b\b\b\b%04d' % ii)
        #    sys.stdout.flush()
        #    im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).data *1.
        #    f = float(ii)/nproj
        #    ref = refB*f + refA*(1-f)
        #    im = (im-dark)/(ref-dark)
        #    im_mean2 = im_mean + im
        #    count2+=1          
        #    if len(np.nonzero(im<0.01)[0]) < (0.1*refA.shape[0]*refA.shape[1]): # for Eglantine 0717
        #        im_mean = im_mean + im
        #        count+=1
        ## note - if the anvil gaps are closed, we can fail to find any "good" images
        #if count > (nproj/20):
        #    im_mean = im_mean / count
        #else:
        #    im_mean = im_mean2 / count2

        sys.stdout.write('\ndone!\n')
        sys.stdout.flush()
        # save out the im_mean to save recreating it
        edf.data = im_mean
        edf.write(scanname + os.sep + scanname + "_edfs" + os.sep +'mean.edf')

    im_mean_orig = im_mean * 1.
    # add a correction for the sCMOS electronics - "tube correction"
    # note change mean to median to avoid introducing lines from bright spots
    if dotubes:
        tubes = np.zeros((sizey, sizex))
        if spiralsize == 0: # single scan, not spiral
            tmpA = np.median(im_mean[0:(sizey/2), :], 0)
            tmpB = np.median(im_mean[(sizey/2):, :], 0)
            # simple smoothing
            tmpAs = np.zeros(tmpA.shape)
            tmpBs = np.zeros(tmpB.shape)
            for ii in range(-20,20):
                tmpAs = tmpAs + np.roll(tmpA, ii)
                tmpBs = tmpBs + np.roll(tmpB, ii)
            tmpAs = tmpAs / 40.
            tmpBs = tmpBs / 40.
            corA = tmpA - tmpAs
            corB = tmpB - tmpBs
            corA[0:20]=0
            corB[0:20]=0
            corA[-20:]=0
            corB[-20:]=0
            # clip this at 3 * sigma?
            #sigA = corA.std()*3
            #sigB = corB.std()*3
            #corA[np.nonzero(np.abs(corA) > sigA)] = 0
            #corB[np.nonzero(np.abs(corB) > sigB)] = 0
            # build the correction
            for ii in range(0, (sizey/2)):
                tubes[ii, :] = corA
                tubes[(ii+(sizey/2)), :] = corB
        else: # spiral mode - derive the averaged correction from the middle portion of the data
            tmpA = np.median(im_mean[0:spiralsize, :], 0)
            tmpB = np.median(im_mean[spiralsize:-spiralsize, :], 0)
            tmpC = np.median(im_mean[-spiralsize:, :], 0)
            tmpAs = np.zeros(tmpB.shape)
            tmpBs = np.zeros(tmpB.shape)
            tmpCs = np.zeros(tmpB.shape)
            for ii in range(-10,10):
                tmpAs = tmpAs + np.roll(tmpA, ii)
                tmpBs = tmpBs + np.roll(tmpB, ii)
                tmpCs = tmpCs + np.roll(tmpC, ii)
            tmpAs = tmpAs / 20.
            tmpBs = tmpBs / 20.
            tmpCs = tmpCs / 20.
            corA = tmpA - tmpAs
            corB = tmpB - tmpBs
            corC = tmpC - tmpCs
            corA[0:10]=0
            corA[-10:]=0
            corB[0:10]=0
            corB[-10:]=0
            corC[0:10]=0
            corC[-10:]=0
            for ii in range(0, sizey):
                if ii<spiralsize:
                    tubes[ii, :] = corA
                elif ii>=(sizey-spiralsize):
                    tubes[ii, :] = corC
                else:
                    tubes[ii, :] = corB

        tubescor = im_mean - tubes
        fftubes = np.float32(np.log(tubescor/im_mean))   
        fftubes[np.nonzero(np.isinf(fftubes))]=0
        im_mean = im_mean * np.exp(fftubes)
    else:
        fftubes = np.zeros(im_mean.shape)

    # now, can use this corrected thing in the subsequent correction
    if homogeneous:
        # we take simply the mean value as a "homogeneous" smooth mean image
        ff = np.float32(np.log(im_mean.mean()/im_mean))
    else:
        if (filtx<30) & (filtz<30):
            im_mean_filt = signal.medfilt2d(im_mean, filt) 
        else:
            # large filts, so downsize first
            sizey_ds = int(np.floor((sizey / 4)))
            sizex_ds = int(np.floor((sizex / 4)))
            filtx_ds = int(np.floor((filtx / 4)))
            filtz_ds = int(np.floor((filtz / 4)))
            if np.mod(filtx_ds, 2)==0:
                filtx_ds+=1
            if np.mod(filtz_ds, 2)==0:
                filtz_ds+=1
            filt_ds = [filtz_ds, filtx_ds]
            # skimage wants float values between -1 and +1
            #im_mean_min = im_mean.min()
            #im_mean_max = im_mean.max()
            # put on 0 to +1
            #im_mean = (im_mean - im_mean_min) / (im_mean_max - im_mean_min)
            im_mean[np.nonzero(im_mean>1.)] = 1.
            im_mean_ds = transform.resize(im_mean, [sizey_ds, sizex_ds], mode='reflect')                
            im_mean_filt_ds = signal.medfilt2d(im_mean_ds, filt_ds)
            # fix the corners
            im_mean_filt_ds[np.nonzero(im_mean_filt_ds==0)] = im_mean_ds[np.nonzero(im_mean_filt_ds==0)]
            im_mean_filt = transform.resize(im_mean_filt_ds, [sizey, sizex], mode='reflect')

        ff = np.float32(np.log(im_mean_filt/im_mean))
        ff[np.nonzero(np.isinf(ff))]=0

        # remove the edges which may be bad?
        mask = np.ones(ff.shape)
        halffiltx = int((filtx+1)/2)
        ramp = np.linspace(0, 1.0, halffiltx)
        ramp = ramp.reshape((1, halffiltx))
        mask[:, 0:halffiltx] = np.tile(ramp, (sizey, 1))
        mask[:, -halffiltx:] = np.tile(ramp[0, ::-1], (sizey, 1))
        ff = ff * mask        

    # Are we applying a pixel mask? - this does not apply to the tubes
    if pixelmask!=0:
        # make the bad pixel map using refB
        # this is good, but perhaps problematic with a movement correction
        print("make the map of bad (saturated) pixels" )
        lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
        refB = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).data *1.  
        badpixels = refB>=pixelmask # just the saturated points
        mymap = np.zeros(badpixels.shape)
        myfilt = 15
        for ii in range(myfilt):
            mymap += (badpixels*1.)
            badpixels = morphology.binary_dilation(badpixels)
        mymap = mymap * 1. / myfilt
        mymap = np.sin((mymap - 0.5)*np.pi)
        mymap = (mymap + 1.) / 2. # smooth transitions, 0 - 1
        ff = ff * mymap

    # combine the two corrections
    ff = ff + fftubes

    # avoid nans ! these cause problems
    if np.any(np.isnan(ff)):
        print('fixing nans')
        mynans = np.nonzero(np.isnan(ff))
        for ii in range(len(mynans[0])):
            # get the neighbourhood
            tmp = ff[(mynans[0][ii]-1):(mynans[0][ii]+2), (mynans[1][ii]-1):(mynans[1][ii]+2)]
            val = tmp[np.nonzero(np.bitwise_not(np.isnan(tmp)))].mean()
            ff[mynans[0][ii], mynans[1][ii]] = val

    # make sure this is float32, although 64 seems to work with pyhst...
    ff = np.float32(ff)
    edf.data = ff
    edf.write(flatfieldname)
    pars = par_tools.par_mod(pars, 'DOUBLEFFCORRECTION', '\"%s\"' % flatfieldname)
    par_tools.par_write(parname, pars)
    print("Activated double flatfield correction")


# to be used by multiprocess
def make_sum(info):
    # info : scanname, first, last, step, refA, refB, dark, nproj
    scanname = info[0]
    first = info[1]
    last = info[2]
    step = info[3]
    xsize = info[4]
    ysize = info[5]
    im_mean = np.zeros((ysize, xsize), np.float32)
    im_mean2 = np.zeros((ysize, xsize), np.float32)
    count, count2 = 0,0
    for ii in range(first, last, step):
        # this is just simple adding all projections
        im = np.float32(edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).data)
        im_mean2 = im_mean2 + im
        count2+=1
        # this is not flatfielded, so the values are wrong...
        # this should deal with columns          
        if len(np.nonzero(im<0.01)[0]) < (0.1*im.shape[0]*im.shape[1]): # for Eglantine 0717
            im_mean = im_mean + im
            count+=1
    return im_mean, count, im_mean2, count2



def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Make, activate or deactivate double flatfield ring correction")
    parser.add_argument("scanname", help="The name of the scan you want to treat", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-a", "--activate", help="Activate (or disactivate) an existing correction [True]/False", default=True, type=testbool)
    parser.add_argument("-t", "--dotubes", help="Special correction for the sCMOS lines (tubes) True/[False]", default=False, type=testbool)
    parser.add_argument("-x", "--filtx", help="Filter size (x, hor) for correction", default=9, type=int)
    parser.add_argument("-z", "--filtz", help="Filter size (z, ver) for correction", default=9, type=int)
    parser.add_argument("-s", "--spiralsize", help="Spiral acquisition original image size y", default=0, type=int)
    parser.add_argument("-n", "--step", help="Step size when making mean", default=3, type=int)
    parser.add_argument("-p", "--pixelmask", help="Threshold for pixelmask to correct only bad spots (0=no mask)", default=0, type=int)
    parser.add_argument("--homogeneous", help="Homogeneous sample True/[False]", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    double_ff(args.scanname, activate=args.activate, filtx=args.filtx, filtz=args.filtz, spiralsize=args.spiralsize, step=args.step, dotubes=args.dotubes, pixelmask=args.pixelmask, homogeneous=args.homogeneous, interactive=args.interactive)

    # ----------------------------------------------------- #
