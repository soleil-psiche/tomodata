
import argparse
import par_tools
import os
from fabio.edfimage import edfimage
edf = edfimage()
import numpy as np
import sys
import reconstruct_and_view
import glob
import pickle

def utopec(scanname, interactive=True):
    '''
    Remove partial projections from utopec tomo
    '''

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if interactive:
        tmp = input("Remove partially obscured projections? [y]/n : ")
        tmp = True if tmp.lower() in ['y', 'yes', ''] else False
        if not tmp:
            print("Not removing projections.  To restore projections, rerun preprocessing")
            return

    # make a sinogram
    pars = par_tools.par_mod(pars, 'OUTPUT_SINOGRAMS', 'YES')
    print("setting the zero clip value")
    pars = par_tools.par_mod(pars, 'ZEROCLIPVALUE', 0.05)
    par_tools.par_write(parname, pars)
    print("build the sinogram")
    reconstruct_and_view.reconstruct_and_view(scanname, interactive=False)
    # put the par file back to normal !
    pars = par_tools.par_mod(pars, 'OUTPUT_SINOGRAMS', 'NO')
    par_tools.par_write(parname, pars)

    # read the sinogram
    sinolist = glob.glob(scanname+os.sep+'*sinogram*edf')
    sino = edf.read(sinolist[0]).getData()
    psino = sino.sum(1) # profile

    # gradient to find images cut by columns
    psinograd = np.abs(np.gradient(psino))
    #columns = np.nonzero(psino == psino.max())[0]
    columns = np.nonzero(psino > 0.95*psino.max())[0] # treat special case with noise (?)
    edges = np.nonzero(psinograd > (psinograd.mean() + psinograd.std()))[0]

    # replace projections near the cut, count all bad projections
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * nproj
    halfacq = True if scanrange>350 and scanrange<370 else False
    if halfacq:
        print("Special treatment for a 360 degree scan!")
    bad = []
    replace = []
    for ii in range(nproj):
        if ((ii+1) in edges) or ((ii-1) in edges):
            replace.append(ii)
            bad.append(ii)
            if halfacq:
                if ii<(nproj/2):
                    bad.append(ii + int(nproj/2))
                else:
                    bad.append(ii - int(nproj/2))
        elif ii in columns:
            bad.append(ii)
            if halfacq:
                if ii<(nproj/2):
                    bad.append(ii + int(nproj/2))
                else:
                    bad.append(ii - int(nproj/2))
    bad = np.unique(bad)

    # save the list of replaced projections for later
    pickle.dump(bad, open('%s/utopec_replaced.pickle' % scanname, 'wb'))  

    # info - how many good projections
    goodproj = nproj - len(bad)
    perreal = 100. * goodproj / nproj 
    print("getting %0.1f percent of real projections" % perreal)

    print("Replace the partially obscured images by darks")
    # replace all the bad ones - slower but more safer
    for ii in bad:
        os.system('cp %s/%s_edfs/dark.edf %s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, scanname, scanname, ii))

    print("Done! - reconstruct to see the effect")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Replace partially obscured projections by darks")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    utopec(args.scanname, interactive=args.interactive)

    # ----------------------------------------------------- #
