# python function to check paganin parameters
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# could add that if no ROI is selected, the roi from the par file is used

import argparse
import par_tools
import os
import pylab
pylab.ion()
from edf_read_dirty import vol_read_dirty
import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
import sys
import time
from edf_read_dirty import vol_read_dirty
import call_pyhst

def paganin(scanname, paglength=25, pus=2.0, puc=0.5, off=False, interactive=True):

    # read the par file
    expdir = os.getcwd()
    parname = expdir + os.sep + scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    pixelsize = float(pars['IMAGE_PIXEL_SIZE_1'][0])
    # save the original values
    origpars = pars.copy()

    if interactive:
        # if on, ask to switch off
        if ('DO_PAGANIN' in pars.keys()) and pars['DO_PAGANIN'][0]=='1':
            off = input("Switch off Paganin filter y/[n]  :")
            off = "n" if off.lower() in ["", "n", "no"] else 'y'
        else:
            off = "n"
        if off=="n":
            print("Enter the Paganin length in pixels [%d], or select by scanning (s):" % paglength )
            selection = input("Paganin length [%d] / s : " % paglength)
            selection = str(paglength) if selection=="" else selection
            if selection.replace(".", "", 1).isdigit():
                paglength = int(selection)
                selection = "m"
            # ask for the other parameters too
            tmp = input("Paganin unsharp filter length in pixels [%d]: " % pus )
            pus = pus if tmp=="" else float(tmp)
            tmp = input("Paganin unsharp filter coefficent [%0.1f]: " % puc )
            puc = puc if tmp=="" else float(tmp)

            if selection == "s":
                print("Will scan Paganin length (units pixels)")
                lowerlimit = float(input("from lower limit: "))
                upperlimit = float(input("to upper limit: "))
                stepsize = float(input("in step size: "))
                roisize = input("size reconstruction ROI [default taken from par file]: ")
        else:
            selection = "m"
    else:
        selection = "m"

    if off=="y" or off==True:
        pars = par_tools.par_mod(pars, "DO_PAGANIN", 0)
        par_tools.par_write(parname, pars)
        print("Switch off Paganin filter in %s" % scanname        )
        return

    if selection == "m":
        print("Paganin parameters: length=%d (%d pixels), marge=%d, PUS=%0.1f, PUC=%0.1f" % (int(paglength*pixelsize), int(paglength), int(paglength), pus, puc))
        pars = par_tools.par_mod(pars,'DO_PAGANIN', 1)
        pars = par_tools.par_mod(pars,'PAGANIN_Lmicron', paglength*pixelsize)
        pars = par_tools.par_mod(pars,'PAGANIN_MARGE', int((paglength)))
        pars = par_tools.par_mod(pars,'PUS', pus)
        pars = par_tools.par_mod(pars,'PUC', puc)
        par_tools.par_write(parname, pars)
        print("Paganin parameters updated in %s" % scanname        )
        return

    if selection == "s":
        # create a subdir
        if not os.path.exists(expdir+os.sep+scanname+os.sep+"tmp"):
            os.mkdir(scanname+os.sep+"tmp")
        # tmp parfile
        tmpparname = expdir + os.sep + scanname + os.sep + "tmp" + os.sep + "tmp.par"
        # apply ROI
        xsize = float(pars['NUM_IMAGE_1'][0])
        zsize = float(pars['NUM_IMAGE_2'][0])
        # apply the roi in z
        tmppars = par_tools.par_mod(pars, 'START_VOXEL_3', int(np.round(zsize/2)))
        tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_3', int(np.round(zsize/2))) 
        # handle the roi in x/y
        if roisize != "":
            roisize = int(roisize)
            xstart = int(np.floor((xsize/2)-(roisize/2)))
            xend = int(xstart + roisize -1)
            tmppars = par_tools.par_mod(tmppars, 'START_VOXEL_1', xstart)
            tmppars = par_tools.par_mod(tmppars, 'START_VOXEL_2', xstart)
            tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_1', xend)
            tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_2', xend)
            roi1 = roisize
            roi2 = roisize
        else:
            # x/y ROI is inherited from the main par file
            roi1 = int(pars['END_VOXEL_1'][0]) - int(pars['START_VOXEL_1'][0]) + 1
            roi2 = int(pars['END_VOXEL_2'][0]) - int(pars['START_VOXEL_2'][0]) + 1
        # axis positions - do upperlimit position
        upperlimit = upperlimit+0.1
        paglength = np.arange(lowerlimit, upperlimit, stepsize)
        slices = np.zeros((roi1, roi2, len(paglength)))
        # do the reconstructions
        for ii in range(len(paglength)):  
            slicename = expdir + os.sep + scanname + os.sep + "tmp" + os.sep + "tmp_%d_.vol" % ii
            # set the paganin params:
            tmppars = par_tools.par_mod(tmppars,'DO_PAGANIN', 1)
            tmppars = par_tools.par_mod(tmppars,'PAGANIN_Lmicron', int(paglength[ii]*pixelsize))
            tmppars = par_tools.par_mod(tmppars,'PAGANIN_MARGE', int(paglength[ii]))
            tmppars = par_tools.par_mod(tmppars,'PUS', pus)
            tmppars = par_tools.par_mod(tmppars,'PUC', puc)
            tmppars = par_tools.par_mod(tmppars, 'OUTPUT_FILE', slicename)
            par_tools.par_write(tmpparname, tmppars)
            sys.stdout.write('start slice %0.1f... \n' % paglength[ii])
            sys.stdout.flush()
            # local version
            #os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null' % tmpparname)
            #os.system('PyHST2_2015b %s > /dev/null' % tmpparname)

            # use a helper function to treat multiple beamlines
            call_pyhst.call_pyhst(tmpparname)

        # read results
        for ii in range(len(paglength)):  
            # read the slice
            slicename = scanname + os.sep + "tmp" + os.sep + "tmp_%d_.vol" % ii
            success = False
            timeout = 0
            while not success and (timeout<5):
                try:
                    myslice = vol_read_dirty(slicename, volsize=[roi1, roi2 ,1], roi=[0,0,0,roi1,roi2,1], datatype=np.float32)
                    slices[:,:,ii] = myslice[:,:,0]
                    success = True
                except:
                    print("vol_read failed for %s...  try again" % slicename  )
                    time.sleep(1)
                    timeout+=1		    
        # show results
        pylab.figure()
        x = int(np.ceil(float(len(paglength))**0.5))
        if (x*(x-1))>float(len(paglength)):
            y = x-1
        else:
            y = x
        mymin = slices.mean()-(3*slices.std())
        mymax = slices.mean()+(3*slices.std())
        for ii in range(len(paglength)):
            pylab.subplot(y,x,int(ii+1))
            pylab.imshow(slices[:,:,ii].T)
            pylab.gray()
            pylab.clim(mymin, mymax)
            pylab.title("%0.1f" % paglength[ii])
        # ask for the best guess
        selection = input("enter the best Paganin length [default no change] : ")
        if selection.replace(".", "", 1).isdigit():
            paglength = int(selection)
            print("Paganin parameters: length=%d microns (%d pixels), marge=%d, PUS=%0.2f, PUC=%0.2f" % (int(paglength*pixelsize), int(paglength), int(paglength), pus, puc))
            # apply this ot the original pars so as not to lose the ROI
            pars = par_tools.par_mod(origpars,'DO_PAGANIN', 1)
            pars = par_tools.par_mod(pars,'PAGANIN_Lmicron', paglength*pixelsize)
            pars = par_tools.par_mod(pars,'PAGANIN_MARGE', int(paglength))
            pars = par_tools.par_mod(pars,'PUS', pus)
            pars = par_tools.par_mod(pars,'PUC', puc)
            
            par_tools.par_write(parname, pars)
        else:
            print("Not updating par file")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Find/change rotation axis in tomography data")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-m", "--manual", help="Specify Paganin length in pixels [25]", default=25, type=float)
    parser.add_argument("--pus", help="Specify Paganin unsharp length in pixels [2.0]", default=2.0, type=float)
    parser.add_argument("--puc", help="Specify Paganin unsharp coefficent [0.5]", default=0.5, type=float)
    parser.add_argument("-o", "--off", help="Force off Pagainin filter [False]/True", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    paganin(args.scanname, paglength=args.manual, pus=args.pus, puc=args.puc, off=args.off, interactive=args.interactive)

    # ----------------------------------------------------- #
