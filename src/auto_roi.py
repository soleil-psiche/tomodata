
import argparse
import par_tools
import os
from fabio.edfimage import edfimage
edf = edfimage()
import numpy as np

def auto_roi(scanname, threshold=0.25, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    # read first and last image, take "worst" case 
    refA = edf.read('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, int(pars['FF_NUM_FIRST_IMAGE'][0]))).getData()
    refB = edf.read('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, int(pars['FF_NUM_LAST_IMAGE'][0]))).getData()
    dark = edf.read('%s/%s_edfs/dark.edf' % (scanname, scanname)).getData()
    imA = edf.read('%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, 1)).getData()*1.
    imA = (imA - dark) / (refA - dark)
    imB = edf.read('%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, int(pars['NUM_LAST_IMAGE'][0]))).getData()*1.
    imB = (imB - dark) / (refB - dark)
    im = np.min(np.dstack((imA, imB)), 2)
    profile = im.sum(1)
    profile = profile / profile.max()

    # set roi
    zstart = np.nonzero(profile>threshold)[0][0] + 1
    zend = np.nonzero(profile>threshold)[0][-1] + 1    
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', zstart)
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', zend)

    # write this to the par file
    par_tools.par_write(parname, pars)




def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set automatic roi in z for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-t", "--threshold", help="threshold for setting roi", default=0.25, type=float)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    auto_roi(args.scanname, threshold=args.threshold, interactive=args.interactive)

    # ----------------------------------------------------- #
