from tkinter import filedialog
import os

def select_name_only():


    scandir = filedialog.askdirectory(initialdir=os.getcwd(), title='double click into the scan directory')
    if len(scandir) == 0: # we cancelled
        return scandir
    else:
        scanname = scandir.split(os.sep)[-1]
        return scanname
