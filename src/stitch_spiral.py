

# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# handle 16 bit hdf5 

from tkinter import filedialog
from fabio.edfimage import edfimage
edf = edfimage()
import sys
import numpy as np
import os
import h5py
import argparse
import par_tools
import glob
import pylab
pylab.ion()
from edf_read_dirty import vol_read_dirty
import correlateImages

# globals for figure function
global cid, fig, coords
coords = []

def onclick(event):
    ix, iy = event.xdata, event.ydata
    if len(coords)!=0:
        sys.stdout.write("\b"*3)
        sys.stdout.flush()
    sys.stdout.write("(%d)" % (len(coords)+1))
    sys.stdout.flush()    coords.append((ix, iy))


def stitch_spiral(scannameroot=None, dzpix=None, filt=None, roi=None, interactive=True):
    # after reconstructing all the subvolumes of a spiral, stitch together with alignment...
    # launch from the exp dir

    if interactive:
        # get the scanname interactively - all data saved in the flyscan-data spool on srv3
        expdir = os.getcwd()
        if scannameroot == None:
            scandir = filedialog.askdirectory(initialdir=expdir, title='double click into one of the spiral scans')
            if len(scandir) != 0: # we cancelled
                name_example = scandir.split(os.sep)[-1]
            scannameroot = name_example[0:-2]
        

    # get the list of scans
    fullscannames = glob.glob(expdir + os.sep + scannameroot + "*")
    fullscannames = sorted(fullscannames)

    # read the first and second parfiles
    scanname0 = fullscannames[0].split(os.sep)[-1]
    parname = scanname0 + os.sep + scanname0 + ".par"
    pars = par_tools.par_read(parname)
    scanname1 = fullscannames[1].split(os.sep)[-1]
    parname = scanname1 + os.sep + scanname1 + ".par"
    pars1 = par_tools.par_read(parname)

    # guess the z step...
    g1 = (float(pars1['#tomotz'][0]) - float(pars['#tomotz'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
    if pars.has_key('#ptz'):
        g2 = (float(pars1['#ptz'][0]) - float(pars['#ptz'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
    else:
        g2 = 0
    if pars.has_key('#table2z'):
        g3 = (float(pars1['#table2z'][0]) - float(pars['#table2z'][0])) / float(pars['IMAGE_PIXEL_SIZE_2'][0])*1000
    else:
        g3 = 0
    guessdz = int(np.round(np.max([g1, g2, g3])))

    # if interactive, confirm the zstep if we are using motor positions
    if dzpix==None:
        if interactive:
            dzpix = input('Enter the z step in pixels [%d] : ' % guessdz)
            dzpix = guessdz if dzpix == '' else int(dzpix)
        else:
            dzpix = guessdz

    # if interactive, confirm the high pass filter length
    if interactive and filt==None:
        filt = input("high-pass filter length (slow! 0=no filter) [7] :")
        filt = 7 if filt=="" else int(filt)    

    # how many scans?
    nspi = len(fullscannames) # how many files!
    nsteps = int(fullscannames[-1][-2:]) - int(fullscannames[0][-2:]) + 1 # how many steps!

    # get the dimensions 
    xsize = int(pars['END_VOXEL_1'][0]) - int(pars['START_VOXEL_1'][0]) + 1 
    ysize = int(pars['END_VOXEL_2'][0]) - int(pars['START_VOXEL_2'][0]) + 1 
    zsize = int(pars['NUM_IMAGE_2'][0])
    zsizeout = zsize + ((nsteps-1) * dzpix)
    
    # origin of each block - nominal to start with
    origins = np.zeros((nsteps, 3)) # x y z?
    for ii in range(nsteps):
        origins[ii, 2] = zsizeout - zsize - (ii*dzpix)

    # overlap in z
    zoverlap = zsize - dzpix

    # look for Paganin volumes?
    pag = False
    if pars.has_key('DO_PAGANIN') and (pars['DO_PAGANIN'][0]=='1'):
        pag = True

    # get a list of correlation positions
    global fig, cid, coords
    positions = np.zeros((nsteps, 2)) # x y
    fig = pylab.figure(1)   
    for block in range(1, nsteps):
        nameA = fullscannames[block-1]
        if pag:
            nameA = nameA + os.sep + nameA.split(os.sep)[-1]+'_0_pag.vol'
        else:
            nameA = nameA + os.sep + nameA.split(os.sep)[-1]+'_0.vol'
        sliceA = vol_read_dirty(nameA, volsize=[xsize, ysize, zsize], roi=[0,0,int(zoverlap/2),xsize,ysize,1], datatype=np.float32)
        pylab.imshow(sliceA[:,:,0])
        pylab.title('Block {}: click where you want to correlate (or outside)'.format(block))
        # get clicks from the image
        cid = fig.canvas.mpl_connect('button_press_event', onclick) 
        pylab.show()
        input('Click figure to select ROI, then enter to continue...')
        fig.canvas.mpl_disconnect(cid)
        positions[block, :] = coords[-1]

    # crop a bit? - ask before starting correlation
    tmp = input('crop the output in xy? y/[n] : ')
    crop = True if tmp.lower() in ['y', 'yes', 'true', '1'] else False
    if crop:
        tmp = input('crop how many pixels from each side? [0 = no crop]')
        delta = 0 if tmp == '' else int(tmp)
        newxsize = xsize - (2*delta)
        newysize = ysize - (2*delta)
    else:
        delta = 0
        newxsize = xsize
        newysize = ysize

    # now, work through the list, adjusting origins
    shifts = np.zeros((nsteps, 3)) # x y z?
    for block in range(1, nsteps):
        if np.any(np.isnan(positions[block, :])):
            print("no correlation for this block")
            continue
        else:
            myx = positions[block, 0]
            myy = positions[block, 1]
        # otherwise, correlate a subvolume
        nameA = fullscannames[block-1]
        nameB = fullscannames[block]
        if pag:
            nameA = nameA + os.sep + nameA.split(os.sep)[-1]+'_0_pag.vol'
            nameB = nameB + os.sep + nameB.split(os.sep)[-1]+'_0_pag.vol'
        else:
            nameA = nameA + os.sep + nameA.split(os.sep)[-1]+'_0.vol'
            nameB = nameB + os.sep + nameB.split(os.sep)[-1]+'_0.vol'

        blockA = vol_read_dirty(nameA, volsize=[xsize, ysize, zsize], roi=[int(myy-100),int(myx-100),int(zoverlap/2)-100,200,200,200], datatype=np.float32)
        blockB = vol_read_dirty(nameB, volsize=[xsize, ysize, zsize], roi=[int(myy-100),int(myx-100),zsize-int(zoverlap/2)-100,200,200,200], datatype=np.float32)

        cx,cy,cz = correlateImages.correlateVolumes(blockA, blockB, filt)
        shifts[block, :] = [cx, cy, cz]
        print("done block {}".format(block))

    # integral shifts
    intshifts = np.zeros((nsteps, 3)) # x y z?
    for block in range(0, nsteps):
        intshifts[block, :] = shifts[0:(block+1), :].sum(0)

    print(intshifts)

    # apply these to the origins
    for block in range(nsteps):    
        origins[block, :] = origins[block, :] - intshifts[block, :] # signs?

    # soft edges?
    soft = True # weight the edges of blocks from 0.01-1 over 100 voxels
    weight = np.ones(zsize)
    if soft:
        print('using soft edges over 100 voxels')
        weight[0:100] = np.arange(100)/100.+ 0.01
        weight[-100:] = np.arange(99, -1, -1)/100. + 0.01

    # now, can we write this to an output volume
    outputname = 'stitched_' + scannameroot + '.vol'
    f = np.memmap(outputname, dtype=np.float32, mode='w+', shape=(newxsize, newysize, zsizeout), order='F')
    count = np.zeros(zsizeout) # keep track of overlaps

    # start writing
    for block in range(nsteps):
        name = fullscannames[block]
        if pag:
            name = name + os.sep + name.split(os.sep)[-1]+'_0_pag.vol'
        else:
            name = name + os.sep + name.split(os.sep)[-1]+'_0.vol'
        if crop: # crop here to reduce memory use
            # should probably read with a memory map too
            blockA = vol_read_dirty(name, volsize=[xsize, ysize, zsize], roi=[delta,delta,0,newxsize,newysize,zsize], datatype=np.float32)
        else:
            blockA = vol_read_dirty(name, volsize=[xsize, ysize, zsize], roi=[0,0,0,xsize,ysize,zsize], datatype=np.float32)
        #blockA = np.roll(np.roll(blockA, int(origins[block, 0]), 1), int(origins[block, 1]), 0)
        # crop here
        #if crop:
        #    blockA = blockA[delta:-delta, delta:-delta, :]
        # write this in
        for zz in range(zsize):
            myslice = blockA[:,:,zz] # read one slice
            myslice = np.roll(np.roll(myslice, int(origins[block, 0]), 1), int(origins[block, 1]), 0) # roll one slice
            #myslice = myslice.T # transpose one slice # no transpose in fact?
            #myslice = blockA[:,:,zz].T # transpose so it ends up the right way around
            zout = zz + origins[block, 2]
            f[:,:,zout]+=myslice*weight[zz]
            count[zout]+=weight[zz]
            if np.mod(zz, 100)==0:
                f.flush() # needed?
                print('flushing after {} slices of block {}'.format(zz, block))
        f.flush()
        print("done block {}".format(block))

    # fix the normalisation
    print("renormalising...")
    for ii in range(zsizeout):
        if not((count[ii]==0) | (count[ii]==1)):
            f[:,:,ii] = f[:,:,ii] / count[ii] 
        if np.mod(ii, 100)==0:
            f.flush() # needed?
            print('flushing after {} slices...'.format(ii))
    f.flush()
    del f

    # write an info file too
    print('writing .info file')
    f = open(outputname + '.info', 'w')
    f.write('! PyHST_SLAVE VOLUME INFO FILE\n')
    f.write('NUM_X = ' + str(newxsize) + '\n')
    f.write('NUM_Y = ' + str(newysize) + '\n')
    f.write('NUM_Z = ' + str(zsizeout) + '\n')
    f.close()
    print("combined volume is ", newxsize, 'x', newysize, 'x', zsizeout)

