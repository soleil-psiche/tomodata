# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# convert 32 to 16 bit for pyHST, median references, mean darks
import fabio
import glob
import sys
import numpy as np
import os

# for the flyscan, nxs case
import h5py

# scanname as argument
args = sys.argv
scanname = args[1]
# remove the trailing / if required
if scanname[-1]=="/":
    scanname = scanname[0:-1]	

# edf writer
writer = fabio.edfimage.edfimage()

# directory for edf files
if not os.path.exists(scanname + os.sep + scanname + '_edfs'):
    os.mkdir(scanname + os.sep + scanname + '_edfs')

# read the data
filename = scanname + os.sep + scanname + '.nxs'
f = h5py.File(filename)
daq = f.keys()[0]
daq_grp = f[daq]
scan_data = daq_grp["scan_data"]
if 'orca' in scan_data.keys():
    orca_data = scan_data["orca"]
elif 'after_' in scan_data.keys():
    orca_data = scan_data["after_"]
else:
    print("can\'t find the data in the .nxs file!")
nproj = orca_data.shape[0]
for ii in range(orca_data.shape[0]):
    
    sys.stdout.write("%04d/" % ii)
    sys.stdout.flush()
    
    im = orca_data[ii]
    writer.setData(im)
    name = scanname + os.sep + scanname + '_edfs' + os.sep + scanname + ('%06d'%ii) + '.edf'
    writer.write(name)    
    
    sys.stdout.write("\b"*5)
    sys.stdout.flush()

f.close()
sys.stdout.write("finished treating images\n")
sys.stdout.flush()

print("horrible bodge - 41 refs and darks assumed")

# read the darks
sys.stdout.write("treating darks\n")
sys.stdout.flush()

mylist = os.listdir(scanname + os.sep + 'darks')
f = h5py.File(scanname + os.sep + "darks" + os.sep + mylist[0])
a=f['entry']
b=a['scan_data']
data = b['orca']
data_dims = data.shape
stack = np.zeros((data_dims[1], data_dims[2], 41))
for ndx in range(10):
    stack[:,:,ndx]=data[ndx, :, :]

f.close()
for ii in range(1, len(mylist)):
    f = h5py.File(scanname + os.sep + "darks" + os.sep + mylist[ii])
    a=f['entry']
    b=a['scan_data']
    data = b['orca']
    for ndx2 in range(data.shape[0]):
        ndx+=1
        stack[:,:,ndx]=data[ndx2, :, :]
    
    f.close()
med = np.mean(stack, 2)
med = np.uint16(med)
writer.setData(med)
name = scanname + os.sep + scanname + '_edfs'+os.sep+'dark.edf'
writer.write(name)

sys.stdout.write("finished treating darks\n")
sys.stdout.flush()

# read the refs
sys.stdout.write("treating  refs\n")
sys.stdout.flush()
mylist = os.listdir(scanname + os.sep + 'refs')
f = h5py.File(scanname + os.sep + "refs" + os.sep + mylist[0])
a=f['entry']
b=a['scan_data']
data = b['orca']
data_dims = data.shape
stack = np.zeros((data_dims[1], data_dims[2], 41))
for ndx in range(10):
    stack[:,:,ndx]=data[ndx, :, :]
f.close()
for ii in range(1, len(mylist)):
    f = h5py.File(scanname + os.sep + "refs" + os.sep + mylist[ii])
    a=f['entry']
    b=a['scan_data']
    data = b['orca']
    for ndx2 in range(data.shape[0]):
        ndx+=1
        stack[:,:,ndx]=data[ndx2, :, :]
    f.close()
med = np.median(stack, 2)
med = np.uint16(med)
writer.setData(med)
name = scanname + os.sep + scanname + '_edfs'+os.sep+'ref000000.edf'
writer.write(name)
name = scanname + os.sep + scanname + '_edfs'+os.sep+ ('ref%06d.edf' % nproj)
writer.write(name)

sys.stdout.write("finished treating prescan refs\n")
sys.stdout.flush()

