
import par_tools
import os
import sys

def load_beamline():
    """
    Load the beamline specific options for tomography
    These are defined in a PyHST style par file
    i.e. text file containing:  OPTION = value
    """

    # get the home directory
    home = os.path.expanduser('~')
    parfile = os.path.join(home, 'BEAMLINE_PARAMETERS.par')
    if os.path.exists(parfile):
        beamline = par_tools.par_read(parfile)
        return beamline
    else:
        print('beamline parfile %s not found ! Quitting...')
        sys.exit()
