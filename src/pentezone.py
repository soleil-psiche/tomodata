
import argparse
import par_tools
import os

def pentezone(scanname, value=200, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if interactive:
        value = raw_input("Value for PENTEZONE [200] : ")
        value = 200 if value == "" else int(value)

    # write this to the par file
    print("update PENTEZONE value in parfile - new value = %f" % value)
    pars = par_tools.par_mod(pars, 'PENTEZONE', value)
    par_tools.par_write(parname, pars)


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False


if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set PENTEZONE value for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-v", "--value", help="Specify PENTEZONE value", default=200, type=int)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    pentezone(args.scanname, value=args.value, interactive=args.interactive)

    # ----------------------------------------------------- #

