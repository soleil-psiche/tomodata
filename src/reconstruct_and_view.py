# python function to reconstruct one slice with current parameters
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

# add the slice dimensions in instrument coordinates...

import argparse
import par_tools
import os
import pylab
pylab.ion()
from edf_read_dirty import vol_read_dirty
import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
import sys
import time
import call_pyhst
import view_only

def reconstruct_and_view(scanname, slicendx=0, timeout=0, interactive=True):
    # no reason for not being interactive... keep default to false outside ipython to be consistent

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if slicendx == 0:
        # default to centre slice if not specified
        slicendx = int(int(pars["NUM_IMAGE_2"][0])/2)

    if interactive:
        selection = input("Slice to reconstruct? [%d] : " % slicendx)
        if selection.isdigit():
            slicendx = int(selection)

    # now, do the reconstruction
    expdir = os.getcwd()
    sliceparname = expdir + os.sep + scanname + os.sep + scanname + "_slice.par"
    slicename = expdir + os.sep + scanname + os.sep + scanname + "_slice.vol"
    # x/y ROI is inherited from the main par file
    roi1 = int(pars['END_VOXEL_1'][0]) - int(pars['START_VOXEL_1'][0]) + 1
    roi2 = int(pars['END_VOXEL_2'][0]) - int(pars['START_VOXEL_2'][0]) + 1
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', slicendx)
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', slicendx)
    pars = par_tools.par_mod(pars, 'OUTPUT_FILE', slicename)
    par_tools.par_write(sliceparname, pars)

    # local version
    #os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null' % sliceparname)
    #os.system('PyHST2_2015b %s > /dev/null' % sliceparname)

    sys.stdout.write('start slice %d... \n' % slicendx)
    sys.stdout.flush()
    # use a helper function to treat multiple beamlines
    call_pyhst.call_pyhst(sliceparname, timeout)

    if interactive:
        # use view_only to open and display the slice
        view_only.view_only(scanname)
    else:
        print("Run in interactive mode to show image")


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Reconstruct one slice and display the result")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-s", "--slice", help="Slice to reconstruct [centre]", default=0, type=int)
    parser.add_argument("-t", "--timeout", help="kill stuck pyhst job after N seconds", default=0, type=int)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    reconstruct_and_view(args.scanname, slicendx=args.slice, timeout=args.timeout, interactive=args.interactive)

    # ----------------------------------------------------- #
