import glob
import os

# remove empty directories with walk

mydir = '/mnt2/usbdisk2/PSICHE_0319'
for root, dirs, files in os.walk(mydir):

    if root == mydir:
        print("this is root dir, continue")
        continue
    else:   
        if (len(dirs)==0) & (len(files)==0):
            print("delete dir %s" % root)
            os.system('rm -rf %s' % root)


# does a scan have all its edf files?

In [112]: for name in mylist:
   .....:     mylist2 = glob.glob(name + os.sep + name + '_edfs/*')
   .....:     print('%s : %d' % (name, len(mylist2)))

# do paganin? do non-paganin?
mylist = glob.glob('rene*')
dopag = []
doabs = []
for name in mylist:
    if os.path.isdir(name):
        if os.path.exists(name + os.sep + name + '.par'):
            if not os.path.exists(name + os.sep + name + '_0_pag.vol.info'):
                dopag.append(name)
            if not os.path.exists(name + os.sep + name + '_0.vol.info'):
                doabs.append(name)
f = open('S_list_with_pag.txt', 'wt')
for name in dopag:
    f.write('%s\n' % name)
f.close()
f = open('S_list_with_abs.txt', 'wt')
for name in doabs:
    f.write('%s\n' % name)
f.close()



