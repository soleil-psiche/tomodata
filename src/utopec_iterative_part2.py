
import argparse
import par_tools
import os
import numpy as np
import pickle
from fabio import edfimage
edf = edfimage.edfimage()
import sys
from skimage import transform

def utopec_iterative_part2(scanname):
    
    # could multi process the edf writing to go faster

    # upscale forward simulated projections and inject into full size dataset
    # assumes that we have a binned dataset, and assumes 4x4 binning
    scanname_bin = scanname + '_BINNING'

    # get the colum positions from the downsized dataset
    cols = pickle.load(open('%s/utopec_replaced.pickle' % scanname_bin,'rb'))
    mycols = []
    start = cols[0]
    offset = 0
    for ii in range(1, len(cols)):
        if cols[ii]==(start+ii-offset):
            continue
        else:
            end = cols[ii-1]+1 # for range
            mycols.append([start, end])
            start = cols[ii]
            offset = ii*1
    end = cols[ii]+1
    mycols.append([start, end])
    print('columns are: '+str(mycols))

    # dimensions
    expdir = os.getcwd()
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1


    # make a folder for the corrected projections and the forward projections
    print('make the initial projections...')
    if not os.path.exists('%s/corrected' % scanname):
        os.mkdir('%s/corrected' % scanname)
    dark = edf.read('%s/%s_edfs/dark.edf' % (scanname, scanname)).getData() * 1.
    refA = edf.read('%s/%s_edfs/ref000000.edf' % (scanname, scanname)).getData() * 1.
    refB = edf.read('%s/%s_edfs/ref%06d.edf' % (scanname, scanname, lastref)).getData() * 1.
    if os.path.exists('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)):
        ff = edf.read('%s/%s_edfs/%s_ff.edf' % (scanname, scanname, scanname)).getData()
    else:
        ff = np.zeros(refA.shape)
    # write projections in corrected folder
    for ii in range(nproj):
        if ii in cols:
            im = edf.read('%s/corrected/corrected%06d.edf' % (scanname_bin, ii)).getData()
            mymin = im.min()
            mymax = im.max()
            im = (im-mymin)/(mymax-mymin)
            im2 = transform.rescale(im, 4)
            im2 = (im2*(mymax-mymin)) + mymin
            edf.setData(np.float32(im2))
        else:
            im = edf.read('%s/%s_edfs/%s%06d.edf' % (scanname, scanname, scanname, ii)).getData()
            ref = refB*(ii/float(lastref)) + refA*(1 - (ii/float(lastref)))
            flat = (im - dark) / (ref - dark)
            flat = flat * np.exp(ff)
            flat = -np.log(flat)
            # crop if we're cropping
            #flat = flat[zstart_out:zend_out, :]
            edf.setData(np.float32(flat))
        # write the projection
        edf.write('%s/corrected/corrected%06d.edf' % (scanname, ii))
        sys.stdout.write('\b\b\b\b%04d' % ii)
        sys.stdout.flush()


    # get everything ready in the par file
    pars = par_tools.par_mod(pars, 'FILE_PREFIX', '%s/%s/corrected/corrected' % (expdir, scanname))
    pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
    pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
    pars = par_tools.par_mod(pars, 'TAKE_LOGARITHM', 'NO')
    #pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', zend_out-zstart_out)
    #pars = par_tools.par_mod(pars, 'START_VOXEL_3', 1)
    #pars = par_tools.par_mod(pars, 'END_VOXEL_3', zend_out-zstart_out)
    #pars = par_tools.par_mod(pars, 'END_VOXEL_3', ysize)
    pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION') # remove this if we have it
    par_tools.par_write(parname, pars)

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False


if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="iterative reconstruction for utopec data. Part 2")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    utopec_iterative_part2(args.scanname)

    # ----------------------------------------------------- #
