
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par


from tkinter import filedialog
import os
import argparse
import par_tools

def copy_parameters(scanname, source="", interactive=True):

    if source=="":
        if not interactive:
            print("Need interactive mode or to supply a source scan name")
            return
        else:
            # get the source scanname interactively
            expdir = os.getcwd()
            source = filedialog.askopenfilename(initialdir=expdir, filetypes=[('par files', '.par')], title='Select the .par file from which to copy parameters')
    sourcename = source.split(os.sep)[-1]
    print("Copy parameters from %s to %s.par" % (sourcename, scanname))

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    # read the par file
    sourcepars = par_tools.par_read(source)

    # update pars
    if interactive:

        selection = input("Copy rotation axis position? [y]/n : ")
        selection = True if selection.lower() in ['', 'y', 'yes', 'true'] else False
        if selection:
            print('copy rotation axis')
            pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", sourcepars["ROTATION_AXIS_POSITION"][0])
        else:
            print('not copying rotation axis')

        selection = input("Copy ROI? [y]/n : ")
        selection = True if selection.lower() in ['', 'y', 'yes', 'true'] else False
        if selection:
            print('copy ROI')
            pars = par_tools.par_mod(pars, "START_VOXEL_1", sourcepars["START_VOXEL_1"][0])
            pars = par_tools.par_mod(pars, "START_VOXEL_2", sourcepars["START_VOXEL_2"][0])
            pars = par_tools.par_mod(pars, "START_VOXEL_3", sourcepars["START_VOXEL_3"][0])
            pars = par_tools.par_mod(pars, "END_VOXEL_1", sourcepars["END_VOXEL_1"][0])
            pars = par_tools.par_mod(pars, "END_VOXEL_2", sourcepars["END_VOXEL_2"][0])
            pars = par_tools.par_mod(pars, "END_VOXEL_3", sourcepars["END_VOXEL_3"][0])
        else:
            print('not copying ROI')

        selection = input("Copy angle offset? [y]/n : ")
        selection = True if selection.lower() in ['', 'y', 'yes', 'true'] else False
        if selection:
            print('copy angle offset')
            pars = par_tools.par_mod(pars, "ANGLE_OFFSET", sourcepars["ANGLE_OFFSET"][0])
        else:
            print('not copying angle offset')

        selection = input("Copy options (axis to centre, half tomo? [y]/n : ")
        selection = True if selection.lower() in ['', 'y', 'yes', 'true'] else False
        if selection:
            print('copy options')
            pars = par_tools.par_mod(pars, "OPTIONS", sourcepars["OPTIONS"][0])
            # need to also copy the options that change when setting up half acquisition
            pars = par_tools.par_mod(pars, "ANGLE_BETWEEN_PROJECTIONS", sourcepars["ANGLE_BETWEEN_PROJECTIONS"][0], sourcepars["ANGLE_BETWEEN_PROJECTIONS"][1])
            pars = par_tools.par_mod(pars, "NUM_LAST_IMAGE", sourcepars["NUM_LAST_IMAGE"][0], sourcepars["NUM_LAST_IMAGE"][1])
        else:
            print('not copying options')        

        selection = input("Copy Paganin parameters? [y]/n : ")
        selection = True if selection.lower() in ['', 'y', 'yes', 'true'] else False
        if selection:
            print('copying parameters')
            if sourcepars.keys().count('DO_PAGANIN') and (sourcepars['DO_PAGANIN'][0] == "1"):
                pars = par_tools.par_mod(pars, "DO_PAGANIN", sourcepars["DO_PAGANIN"][0])
                pars = par_tools.par_mod(pars, "PAGANIN_Lmicron", sourcepars["PAGANIN_Lmicron"][0])
                pars = par_tools.par_mod(pars, "PAGANIN_MARGE", sourcepars["PAGANIN_MARGE"][0])
                pars = par_tools.par_mod(pars, "PUS", sourcepars["PUS"][0])
                pars = par_tools.par_mod(pars, "PUC", sourcepars["PUC"][0])
            else:
                # no paganin in source, so make sure it is off
                print('Paganin disactivated in source dataset')
                pars = par_tools.par_mod(pars, "DO_PAGANIN", "0")
        else:
            print('not copying Paganin')

    else: # update everything
        print('update everything')
        pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", sourcepars["ROTATION_AXIS_POSITION"][0])
        pars = par_tools.par_mod(pars, "START_VOXEL_1", sourcepars["START_VOXEL_1"][0])
        pars = par_tools.par_mod(pars, "START_VOXEL_2", sourcepars["START_VOXEL_2"][0])
        pars = par_tools.par_mod(pars, "START_VOXEL_3", sourcepars["START_VOXEL_3"][0])
        pars = par_tools.par_mod(pars, "END_VOXEL_1", sourcepars["END_VOXEL_1"][0])
        pars = par_tools.par_mod(pars, "END_VOXEL_2", sourcepars["END_VOXEL_2"][0])
        pars = par_tools.par_mod(pars, "END_VOXEL_3", sourcepars["END_VOXEL_3"][0])
        # need to also copy the options that change when setting up half acquisition
        pars = par_tools.par_mod(pars, "ANGLE_BETWEEN_PROJECTIONS", sourcepars["ANGLE_BETWEEN_PROJECTIONS"][0], sourcepars["ANGLE_BETWEEN_PROJECTIONS"][1])
        pars = par_tools.par_mod(pars, "NUM_LAST_IMAGE", sourcepars["NUM_LAST_IMAGE"][0], sourcepars["NUM_LAST_IMAGE"][1])
        pars = par_tools.par_mod(pars, "ANGLE_OFFSET", sourcepars["ANGLE_OFFSET"][0])
        pars = par_tools.par_mod(pars, "OPTIONS", sourcepars["OPTIONS"][0])
        
        if sourcepars.keys().count('DO_PAGANIN') and (sourcepars['DO_PAGANIN'][0] == "1"):
            pars = par_tools.par_mod(pars, "DO_PAGANIN", sourcepars["DO_PAGANIN"][0])
            pars = par_tools.par_mod(pars, "PAGANIN_Lmicron", sourcepars["PAGANIN_Lmicron"][0])
            pars = par_tools.par_mod(pars, "PAGANIN_MARGE", sourcepars["PAGANIN_MARGE"][0])
            pars = par_tools.par_mod(pars, "PUS", sourcepars["PUS"][0])
            pars = par_tools.par_mod(pars, "PUC", sourcepars["PUC"][0])
        else:
            # no paganin in source, so make sure it is off
            pars = par_tools.par_mod(pars, "DO_PAGANIN", "0")

    # write the updated par file
    par_tools.par_write(parname, pars)
    print("Par file %s updated" % scanname)


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Copy reconstruction parameters from one scan to another (ROI,Pag,axis pos)")
    parser.add_argument("scanname", help="The name of the scan to work on", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-s", "--source", help="name of scan from which to copy parameters", default="", type=str)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    copy_parameters(args.scanname, source=args.source, interactive=args.interactive)

    # ----------------------------------------------------- #
