
import argparse
import par_tools
import os

def zero_clip(scanname, value=0.001, interactive=True):
    
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    if interactive:
        value = input("Value for zero clip [0.1] : ")
        value = 0.1 if value == "" else float(value)

    # write this to the par file
    print("update zero clip value in parfile - new value = %f" % value)
    pars = par_tools.par_mod(pars, 'ZEROCLIPVALUE', value)
    par_tools.par_write(parname, pars)




def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Set zero clip value for a reconstruction")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-v", "--value", help="Specify zero clip value", default=0.001, type=float)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    
    args = parser.parse_args()

    # now, call movement_correction with these arguments
    zero_clip(args.scanname, value=args.value, interactive=args.interactive)

    # ----------------------------------------------------- #
