import sys
sys.path.append('/shared/Python/')
import preprocess_all
import numpy as np
import os
import time
from fabio.edfimage import edfimage
edf = edfimage()
import glob

#this python scritp is suppose to compute all proccessing steps you want 
#including binning preproccessing and reconstruction processing in protocol.txt


curdir = os.getcwd()
Lcurdir = curdir.split(os.sep)

#parameters
myscanname =str(sys.argv[1])    #it's the last name of the spool directory
ProtocolName = 'Protocol_Utopec.txt'
#### 2RM if working line 30 #### myformat = 'edf'    #may a test should be add ... ? for it look lines 56 to 63 in preprocess_all.py
Choosenbin=[2,2,1]

scandir = '/nfs/spool1/FILETRANSFER/com-psiche/'+Lcurdir[-3]+os.sep+Lcurdir[-2]+os.sep+Lcurdir[-1]+os.sep+myscanname
#scandir = '/nfs/spool1/FILETRANSFER/com-psiche/proposal/2021/Mishurova_0421/'+myscanname
#scandir = '/nfs/spool1/FILETRANSFER/com-psiche/inhouse/2021/Henry_0421/'+myscanname

spooldir = scandir[0:scandir.rfind(os.sep)]


fullscannames = glob.glob(spooldir + os.sep + "*/*.par")
fullscannames.sort(key=os.path.getctime)
lastname = fullscannames[-1].split(os.sep)[-1]
lastname = lastname.split('.')[0] # get rid of the .par

nxslist = glob.glob(spooldir + os.sep + myscanname +os.sep+'*.nxs')
if os.path.exists(spooldir + os.sep + myscanname +os.sep+'images'):
    myformat = 'edf'
else:
    myformat = 'hdf5'

#preprocess including binning#
##############################
scanname_out = preprocess_all.preprocess(scanname=myscanname, accfac=-1, format=myformat, bin=Choosenbin, spooldir=spooldir, interactive=False)

#select protocol file#
#########################
#filename = filedialog.askopenfilename(title='select an instructions file')
fh = open(ProtocolName, 'rt')
instructionsA = fh.readlines()
fh.close()
# remove end of line characters
instructions = []
for name in instructionsA:
    instructions.append(name.strip())

print('Apply instructions to %s' % scanname_out)
for instruction in instructions:
    # function name
    myinstruction = instruction.replace('XXX', scanname_out)
    os.system(myinstruction)
                


