import glob
import pylab
pylab.ion()
import os
from edf_read_dirty import read_volsize, vol_read_dirty
import numpy as np

def check_reconstructions(mydir, mystring=None, skipslices=True, only='both', interactive=False):
    '''noread, allzeros, somezeros, nozeros, redo = 
    check_reconstructions(mydir, skipslices=True, interactive=False)
    '''
    # for outputs
    allzeros = []
    somezeros = []
    nozeros = []
    noread = []
    redo = [] # for interactive check

    # check that all scans in subfolders in mydir are ok - 
    origdir = os.getcwd()
    os.chdir(mydir)

    if mystring==None:
        scanlist = glob.glob('*')
    else:
        scanlist = glob.glob(mystring)

        
    for name in scanlist:
        # look for a raw file
        print("__try %s  ___" % name)
        if only in ['both', 'raw']:
            rawlist = glob.glob('%s/%s*raw' % (name, name))
            for rawname in rawlist:
                try:
                    volsize, datatype = read_volsize(rawname)
                    if (volsize[2]==1) & skipslices:
                        print("this is a 2D image")
                    else:
                        # read a colummn of data
                        nbytes = np.dtype(datatype).itemsize
                        collength = len(range(0, volsize[2], 50))
                        col = np.zeros(collength)
                        slicesize = volsize[0] * volsize[1]                    
                        f = open(rawname, 'rb')
                        # guess what is zero value - corner voxel
                        if datatype in [np.uint8, np.uint16]:
                            zeroval = np.fromstring(f.read(nbytes), datatype)
                        else:
                            zeroval = 0.0
                        print("zero val is %d" % zeroval)
                        offset = (np.floor((volsize[0]*(volsize[1]/2))+(volsize[0]/2)))
                        for jj, ii in enumerate(range(0, volsize[2], 50)):
                            f.seek((offset + (ii*slicesize)) * nbytes)
                            col[jj] = np.fromstring(f.read(nbytes), datatype)
                        f.close()
                        if np.all((col-col[0])==0): # recad - 0 in the vol can become something else here
                            print('%s looks bad - all zeros' % rawname)
                            allzeros.append(rawname)
                        elif np.sometrue(col==0):
                            print('%s contains some zeros' % rawname)
                            if (len(np.nonzero(col==0))==1) and (collength>1):
                                print('only one zero value in a recad volume - probably ok')
                                nozeros.append(rawname)
                            else:
                                print('more than one zero - check!')
                                somezeros.append(rawname)
                        else:
                            print('%s appears good' % rawname)
                            nozeros.append(rawname)
                            if interactive:
                                im = vol_read_dirty(rawname, roi='centre')
                                pylab.clf()
                                pylab.imshow(im[:,:,0])
                                pylab.show()
                                tmp = input('is this ok [y]/n :')
                                tmp = 'y' if tmp.lower() in ['', 'y', 'yes'] else 'n'
                                if tmp == 'n':
                                    redo.append(name)
                except:
                    print("problem reading %s" % rawname)
                    noread.append(rawname)
        
        # look for a vol file
        if only in ['both', 'vol']:
            vollist = glob.glob('%s/%s*vol' % (name, name))
            for volname in vollist:
                try:
                    volsize = read_volsize(volname)[0]
                    if (volsize[2]==1) & skipslices:
                        print("this is a 2D image")
                    else:
                        # read a colummn of data
                        collength = len(range(0, volsize[2], 50))
                        col = np.zeros(collength)
                        slicesize = volsize[0] * volsize[1]                    
                        f = open(volname, 'rb')
                        offset = (np.floor((volsize[0]*(volsize[1]/2))+(volsize[0]/2)))
                        for jj, ii in enumerate(range(0, volsize[2], 50)):
                            f.seek(4*(offset + (ii*slicesize)))
                            col[jj] = np.fromstring(f.read(4), np.float32)
                        f.close()
                        if np.all(col==0):
                            print('%s looks bad - all zeros' % volname)
                            allzeros.append(volname)
                        elif np.sometrue(col==0):
                            print('%s contains some zeros' % volname)
                            somezeros.append(volname)
                        else:
                            print('%s appears good' % volname)
                            nozeros.append(volname)
                            if interactive:
                                im = vol_read_dirty(volname, roi='centre')
                                pylab.clf()
                                pylab.imshow(im[:,:,0])
                                pylab.show()
                                tmp = input('is this ok [y]/n :')
                                tmp = 'y' if tmp.lower() in ['', 'y', 'yes'] else 'n'
                                if tmp == 'n':
                                    redo.append(name)
                except:
                    print("problem reading %s" % volname)
                    noread.append(volname)

    # are there scans in the top level? Can happen with copied data
    print('Check the top level in case')
    if only in ['both', 'raw']:
        rawlist = glob.glob('*raw')
        for rawname in rawlist:
            name = rawname.split('.')[0]
            try:
                volsize, datatype = read_volsize(rawname)
                if (volsize[2]==1) & skipslices:
                    print("this is a 2D image")
                else:
                    # read a colummn of data
                    nbytes = np.dtype(datatype).itemsize
                    collength = len(range(0, volsize[2], 50))
                    col = np.zeros(collength)
                    slicesize = volsize[0] * volsize[1]                    
                    f = open(rawname, 'rb')
                    # guess what is zero value - corner voxel
                    if datatype in [np.uint8, np.uint16]:
                        zeroval = np.fromstring(f.read(nbytes), datatype)
                    else:
                        zeroval = 0.0
                    print("zero val is %d" % zeroval)
                    offset = int(np.floor((volsize[0]*(volsize[1]/2))+(volsize[0]/2)))
                    for jj, ii in enumerate(range(0, volsize[2], 50)):
                        f.seek((offset + (ii*slicesize)) * nbytes)
                        col[jj] = np.fromstring(f.read(nbytes), datatype)
                    f.close()
                    if np.all(col==0):
                        print('%s looks bad - all zeros' % rawname)
                        allzeros.append(rawname)
                    elif np.sometrue(col==0):
                        print('%s contains some zeros' % rawname)
                        if (len(np.nonzero(col==0))==1) and (collength>1):
                            print('only one zero value in a recad volume - probably ok')
                            nozeros.append(rawname)
                        else:
                            print('more than one zero - check!')
                            somezeros.append(rawname)
                    else:
                        print('%s appears good' % rawname)
                        nozeros.append(rawname)
                        if interactive:
                            im = vol_read_dirty(rawname, roi='centre')
                            pylab.clf()
                            pylab.imshow(im[:,:,0])
                            pylab.show()
                            tmp = input('is this ok [y]/n :')
                            tmp = 'y' if tmp.lower() in ['', 'y', 'yes'] else 'n'
                            if tmp == 'n':
                                redo.append(name)
            except:
                print("problem reading %s" % rawname)
                noread.append(rawname)

    # look for a vol file
    if only in ['both', 'vol']:
        vollist = glob.glob('*vol')
        for volname in vollist:
            name = volname.split('.')[0]
            try:
                volsize = read_volsize(volname)[0]
                if (volsize[2]==1) & skipslices:
                    print("this is a 2D image")
                else:
                    # read a colummn of data
                    collength = len(range(0, volsize[2], 50))
                    col = np.zeros(collength)
                    slicesize = volsize[0] * volsize[1]                    
                    f = open(volname, 'rb')
                    offset = (np.floor((volsize[0]*(volsize[1]/2))+(volsize[0]/2)))
                    for jj, ii in enumerate(range(0, volsize[2], 50)):
                        f.seek(4*(offset + (ii*slicesize)))
                        col[jj] = np.fromstring(f.read(4), np.float32)
                    f.close()
                    if np.all(col==0):
                        print('%s looks bad - all zeros' % volname)
                        allzeros.append(volname)
                    elif np.sometrue(col==0):
                        print('%s contains some zeros' % volname)
                        somezeros.append(volname)
                    else:
                        print('%s appears good' % volname)
                        nozeros.append(volname)
                        if interactive:
                            im = vol_read_dirty(volname, roi='centre')
                            pylab.clf()
                            pylab.imshow(im[:,:,0])
                            pylab.show()
                            tmp = input('is this ok [y]/n :')
                            tmp = 'y' if tmp.lower() in ['', 'y', 'yes'] else 'n'
                            if tmp == 'n':
                                redo.append(name)
            except:
                print("problem reading %s" % volname)
                noread.append(volname)

    # return to the original directory - 
    os.chdir(origdir)

    return noread, allzeros, somezeros, nozeros, redo
