
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par
# if no scanname given, user select folder

# handle either 32 bit edfs or 16 bit hdf5 - current options
# use either in ipython or from outside for batch processing

# convert 32 to 16 bit for pyHST, median references, mean darks

# this version is for the acquisition with no Init allowed on the camera, so all image names are the same ('orca')

# should get rid of the fast option
# add an on the fly mean image
# is reading with multiprocess faster?

from tkinter import filedialog
from fabio.edfimage import edfimage
edf = edfimage()
import glob
import sys
import numpy as np
import os
import h5py
import argparse
from scipy.ndimage.filters import convolve1d
import par_tools
from scipy import signal
from view_only import view_only

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def preprocess(scanname=None, accfac=-1, format='hdf5', bin=[1,1,1], spooldir="", interactive=True):
    # preprocess... in interactive mode with no scanname, format tells where to start looking for data
    # otherwise, it's a choice

    # where is raw data collected?
    if format not in ['hdf5', 'edf']:
        print("format %s not understood - quitting" % format)
        return

    # get the beamline default spool directory
    if spooldir == "":
        spooldir = beamline['DEFAULT_SPOOL_DIRECTORY'][0]
    # get the scanname interactively
    if scanname == None:
        scandir = filedialog.askdirectory(initialdir=spooldir, title='double click into the scan directory')
        if len(scandir) == 0: # we cancelled
            return scandir
        else:
            scanname = scandir.split(os.sep)[-1]
            spooldir = scandir[0:scandir.rfind(os.sep)]
            # what kind of data do we have?
            nxslist = glob.glob(scandir+os.sep+'*.nxs')
            if len(nxslist)==0:
                format = 'edf'
            else:
                format = 'hdf5'
            # this isn't very robust...

    # does preprocessed data already exist?
    if interactive & os.path.exists(scanname + os.sep + scanname + "_edfs" + os.sep + "dark.edf"):
        print("Preprocessed data already exists")
        a = input("Repeat prepocessing? [y]/n: ")
        a = 'y' if a == "" else a
        if a == 'n':
            print("not repeating preprocessing")
            view_only(scanname)
            return scanname

    if format == 'edf':
        # automatic calculation of the accumulation factor
        # read first ref from the spool
        name = "%s/%s/refA/orca000001.edf" % (spooldir, scanname)
        im = edf.read(name).getData()
        guess = int(np.ceil(im.max() / 65535.))
        if interactive:
            print("Confirm the accumulation factor for 32 bit edf data")
            a = input("Accumulation factor? [%d]: " % guess)
            accfac = guess if a == "" else int(a)        
        else:
            if accfac == -1:
                accfac = guess
                print("Guessing accumulation factor %d" % accfac)
            else:
                print("Using supplied accumulation factor %d" % accfac)

    # handle interactive choice of filterlength
    #if interactive & (mean==-1):
    #    mean = input("filter projections with 1 x N mean filter? [0 = no filter] or N: ")
    #    mean = 0 if mean in ["", "n", "N"] else int(mean)
    renormalise = True # by default
    if interactive & all(np.array(bin)==1):
    #    mean = input("filter projections with 1 x N mean filter? [0 = no filter] or N: ")
    #    mean = 0 if mean in ["", "n", "N"] else int(mean)
        val = input("bin projections data y/[n] : ")
        val = True if val.lower() in ['y', 'yes', 'true'] else False
        if val:
            val = input("bin factor in X (horizontal) [1] : ")
            bin[0] = 1 if val=='' else int(val)
            val = input("bin factor in Y (vertical) [1] : ")
            bin[1] = 1 if val=='' else int(val)
            val = input("bin factor in scan direction (angle) [1] : ")
            bin[2] = 1 if val=='' else int(val)
            renormalise = input("renormalise after binning [y]/n : ")
            renormalise = False if val.lower() in ['n', 'no', 'false'] else True
        else:
            bin = [1,1,1]
            renormalise = True


    # if we are binning, change the scanname to scanname_out
    if any(np.array(bin)!=1):
        # we are binning in some way - change the name
        scanname_out = scanname + '_BINNING'
        print('scanname will become : %s' % scanname_out)
    else:
        scanname_out = scanname

    # now, we know where the data is and how much - do the necessary!
    if format=="hdf5":
        do_hdf5(scanname, scanname_out, spooldir, bin, renormalise, interactive)
    else:
        do_edf(scanname, scanname_out, spooldir, bin, accfac)

    # update the par file to reflect the new location
    expdir = os.getcwd() + os.sep + scanname_out
    parname = scanname_out + os.sep + scanname_out + ".par"
    pars = par_tools.par_read(parname)
    # slice.vol for output
    pars = par_tools.par_mod(pars, "OUTPUT_FILE", expdir + os.sep + scanname_out + "_slice.vol")
    pars = par_tools.par_mod(pars, "FILE_PREFIX", expdir + os.sep + scanname_out + "_edfs" + os.sep + scanname_out)
    pars = par_tools.par_mod(pars, "BACKGROUND_FILE", expdir + os.sep + scanname_out + "_edfs" + os.sep + "dark.edf")
    pars = par_tools.par_mod(pars, "FF_PREFIX", expdir + os.sep + scanname_out + "_edfs" + os.sep + "ref")
    if np.any(np.array(bin)!=1):
        # account for binning
        print("account for binning in par file")
        pars = par_tools.par_mod(pars, 'IMAGE_PIXEL_SIZE_1', float(pars['IMAGE_PIXEL_SIZE_1'][0])*bin[0], '(binned)')
        pars = par_tools.par_mod(pars, 'IMAGE_PIXEL_SIZE_2', float(pars['IMAGE_PIXEL_SIZE_2'][0])*bin[1], '(binned)')
        pars = par_tools.par_mod(pars, 'NUM_IMAGE_1', int(np.floor(float(pars['NUM_IMAGE_1'][0])/bin[0])), '(binned)')
        pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', int(np.floor(float(pars['NUM_IMAGE_2'][0])/bin[1])), '(binned)')
        pars = par_tools.par_mod(pars, 'NUM_LAST_IMAGE', int(np.floor(float(pars['NUM_LAST_IMAGE'][0])/bin[2])), '(binned)')
        pars = par_tools.par_mod(pars, 'FF_NUM_LAST_IMAGE', int(np.floor(float(pars['FF_NUM_LAST_IMAGE'][0])/bin[2])), '(binned)')
        pars = par_tools.par_mod(pars, 'FF_FILE_INTERVAL', pars['FF_NUM_LAST_IMAGE'][0], '(binned)')
        pars = par_tools.par_mod(pars, 'ROTATION_AXIS_POSITION', float(pars['ROTATION_AXIS_POSITION'][0])/bin[1], '(binned)')
        pars = par_tools.par_mod(pars, 'ANGLE_BETWEEN_PROJECTIONS', float(pars['ANGLE_BETWEEN_PROJECTIONS'][0])*bin[2], '(binned)')
        pars = par_tools.par_mod(pars, 'END_VOXEL_1', int(pars['NUM_IMAGE_1'][0]), '(binned)')
        pars = par_tools.par_mod(pars, 'END_VOXEL_2', int(pars['NUM_IMAGE_1'][0]), '(binned)')
        pars = par_tools.par_mod(pars, 'END_VOXEL_3', int(pars['NUM_IMAGE_2'][0]), '(binned)')

    par_tools.par_write(parname, pars)
    
    # finished
    sys.stdout.write('\nFinished preprocessing %s\n' % scanname_out)
    return scanname_out

    
############### PREPROCESS EDF DATA ###############
def do_edf(scanname, scanname_out, spooldir, bin, accfac):

    # subfunction for clarity
    # make the directories 
    if not os.path.exists(scanname_out):
        os.mkdir(scanname_out)
    if not os.path.exists(scanname_out+os.sep+scanname_out+"_edfs"):
        os.mkdir(scanname_out+os.sep+scanname_out+"_edfs")

    # get the par file
    os.system('cp %s/%s/*.par %s/%s.par' % (spooldir,scanname,scanname_out,scanname_out)) 

    # how many projections, refs, darks, calibs? 
    filelist = glob.glob(spooldir + os.sep + scanname + os.sep + 'refA' +os.sep + '*.edf')
    nref = len(filelist)
    filelist = glob.glob(spooldir + os.sep + scanname + os.sep + 'dark' +os.sep + '*.edf')
    ndark = len(filelist)
    filelist = glob.glob(spooldir + os.sep + scanname + os.sep + 'images' +os.sep + '*.edf')
    nproj = len(filelist)
    filelist = glob.glob(spooldir + os.sep + scanname + os.sep + 'calib' +os.sep + '*.edf')
    ncalib = len(filelist)
    #binning
    # read one image for dimensions
    im = edf.read("%s/%s/images/orca%06d.edf" % (spooldir, scanname, 1)).getData()
    bin = np.array(bin)
    nproj = int(np.floor(nproj / bin[2]))
    xsize, ysize = im.shape[1], im.shape[0]
    ysizeout = int(np.floor(ysize / bin[1]))
    xsizeout = int(np.floor(xsize / bin[0]))
    ysizein = ysizeout*bin[1]
    xsizein = xsizeout*bin[0]

    sys.stdout.write('treat images:       ')
    for ii in range(nproj):
        if np.all(bin==1): # no binning
            # read image from the spool
            name = "%s/%s/images/orca%06d.edf" % (spooldir, scanname, ii+1) # numbered from 1
            im = edf.read(name).getData()
        else: # binning
            iiA = ii*bin[2]
            name = "%s/%s/images/orca%06d.edf" % (spooldir, scanname, iiA+1) # numbered from 1
            im = np.uint32(edf.read(name).getData())
            # bin in angle
            for jj in range(iiA+1, iiA+bin[2]):
                name = "%s/%s/images/orca%06d.edf" % (spooldir, scanname, jj)
                im += np.uint32(edf.read(name).getData())
            # bin in x/y        
            if bin[0]!=1 or bin[1]!=1:
                im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).sum(3).sum(1)
            im = im / np.prod(bin) 
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength)  
        # convert to 16 bit
        imout = np.uint16(im/accfac)
        edf.setData(imout)
        # go to numbering from zero
        nameout = "%s/%s_edfs/%s%06d.edf" % (scanname_out, scanname_out, scanname_out, ii) # number from 0
        edf.write(nameout)    
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()

    # get calib images, if present, and drop in the same directory
    sys.stdout.write('\ntreat calib images:       ')
    for ii in range(1, ncalib+1):
        # read image from the spool
        name = "%s/%s/calib/orca%06d.edf" % (spooldir, scanname, ii)
        im = edf.read(name).getData()
        if bin[0]!=1 or bin[1]!=1:
            im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength)  
        # convert to 16 bit
        imout = np.uint16(im/accfac)
        edf.setData(imout)
        # go to numbering from zero
        nameout = "%s/%s_edfs/%s%06d.edf" % (scanname_out, scanname_out, 'calib', ii-1)
        edf.write(nameout)    
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()        

    sys.stdout.write('\ntreat initial references:       ')
    stack = np.zeros((ysize, xsize, nref))
    for ii in range(1, nref+1):
        # read image from the spool
        name = "%s/%s/refA/orca%06d.edf" % (spooldir, scanname, ii)
        im = edf.read(name).getData()
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength) 
        # convert to 16 bit
        im = np.uint16(im/accfac)
        # add to stack
        stack[:,:,ii-1] = im
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()
    # do the median
    med = np.median(stack, 2)
    if bin[0]!=1 or bin[1]!=1:
        print('bin the refA')
        med = med[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
    med = np.uint16(med)
    edf.setData(med)
    edf.write(scanname_out + os.sep + scanname_out+ "_edfs/ref000000.edf")

    sys.stdout.write('\ntreat final references:       ')
    for ii in range(1, nref+1):
        # read image from the spool
        name = "%s/%s/refB/orca%06d.edf" % (spooldir, scanname, ii)
        im = edf.read(name).getData()
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength) 
        # convert to 16 bit
        im = np.uint16(im/accfac)
        # add to stack
        stack[:,:,ii-1] = im
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()
    # do the median
    med = np.median(stack, 2)
    if bin[0]!=1 or bin[1]!=1:
        print('bin the refB')
        med = med[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
    med = np.uint16(med)
    edf.setData(med)
    edf.write(scanname_out + os.sep + scanname_out+ "_edfs/ref%06d.edf" % nproj)

    sys.stdout.write('\ntreat darks:       ')
    stack = np.zeros((ysize, xsize, ndark))
    # work around the numbering problem - try to fix
    # for ii in range(nref+1, ndark+nref+1):
    for ii in range(1, ndark+1):

        # read image from the spool
        name = "%s/%s/dark/orca%06d.edf" % (spooldir, scanname, ii)
        name_x = "%s/%s/dark/orca%06d.edf" % (spooldir, scanname, (ii+nref))
        if not os.path.exists(name) and os.path.exists(name_x):
            name = name_x
        im = edf.read(name).getData()
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength)
        # convert to 16 bit
        im = np.uint16(im/accfac) 
        # add to stack
        stack[:,:,ii-nref-1] = im
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()
    # do the mean
    med = np.mean(stack, 2)
    # median filter for hot pixel
    med = signal.medfilt2d(med, [3, 3]) 
    if bin[0]!=1 or bin[1]!=1:
        print('bin the dark')
        med = med[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
    med = np.uint16(med)
    edf.setData(med)
    edf.write(scanname_out + os.sep + scanname_out+ "_edfs/dark.edf")
############### PREPROCESS EDF DATA ###############

############## PREPROCESS HDF5 DATA ###############
def do_hdf5(scanname, scanname_out, spooldir, bin, renormalise, interactive=False):
    # subfunction for clarity

    # make the directories
    if not os.path.exists(scanname_out):
        os.mkdir(scanname_out)
    if not os.path.exists(scanname_out+os.sep+scanname_out+"_edfs"):
        os.mkdir(scanname_out+os.sep+scanname_out+"_edfs")

    # get the par file
    os.system('cp %s/%s/*.par %s/%s.par' % (spooldir,scanname,scanname_out,scanname_out)) 

    # read the data from the nxs files
    filename = spooldir + os.sep + scanname + os.sep + scanname + '.nxs'
    f = h5py.File(filename, 'r')
    daq = list(f.keys())[0]
    daq_grp = f[daq]
    scan_data = daq_grp["scan_data"]
    if 'orca' in scan_data.keys():
        orca_data = scan_data["orca"]
    elif 'orca_image' in scan_data.keys():
        orca_data = scan_data["orca_image"]
    elif 'after_' in scan_data.keys():
        orca_data = scan_data["after_"]
    else:
        print("can\'t find the data in the .nxs file!")
        return
    
    # do the images
    sys.stdout.write('treat images:       ')
    bin = np.array(bin)
    nproj = int(np.floor(orca_data.shape[0] / bin[2]))
    ysizeout = int(np.floor(orca_data.shape[1] / bin[1]))
    xsizeout = int(np.floor(orca_data.shape[2] / bin[0]))
    ysizein = ysizeout*bin[1]
    xsizein = xsizeout*bin[0]

    for ii in range(nproj):
        if np.all(bin==1): # no binning
            im = orca_data[ii]
        else: # binning
            iiA = ii*bin[2]
            iiB = iiA + bin[2]
            # bin in angle
            im = np.uint32(orca_data[iiA:iiB]).sum(0)
            # bin in x/y        
            if bin[0]!=1 or bin[1]!=1:
                im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).sum(3).sum(1)
            if renormalise:
                im = im / np.prod(bin) 
        im = np.uint16(im)
        sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
        sys.stdout.flush()
        # write as edf
        edf.data = im
        name = scanname_out + os.sep + scanname_out + '_edfs' + os.sep + scanname_out + ('%06d'%ii) + '.edf'
        edf.write(name)      

    f.close()

    # do the calib images
    if os.path.isfile(spooldir + os.sep + scanname + os.sep + 'calib.nxs'):
        #f = h5py.File(scanname + os.sep + "calib.nxs")
        f = h5py.File(spooldir + os.sep + scanname + os.sep + "calib.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        ncalib = data.shape[0]
        for ii in range(ncalib):
            im = data[ii]
            if bin[0]!=1 or bin[1]!=1:
                im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
            im = np.uint16(im)
            if not renormalise:
                im = im * np.prod(bin) 
            sys.stdout.write(6*"\b"+("%d" % ii).rjust(6))
            sys.stdout.flush()
            # write as edf
            edf.data = im
            name = scanname_out + os.sep + scanname_out + '_edfs' + os.sep + 'calib' + ('%06d' % ii) + '.edf'
            edf.write(name)      
        f.close()

    print("looking for " + spooldir + os.sep + scanname + os.sep + "pre_ref.nxs")
    if os.path.exists(spooldir + os.sep + scanname + os.sep + "pre_ref.nxs"):
        # do the first refs
        sys.stdout.write('\ntreat initial reference group: ')
        #f = h5py.File(scanname + os.sep + "pre_ref.nxs")
        f = h5py.File(spooldir + os.sep + scanname + os.sep + "pre_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        im = np.uint16(np.median(data, 0))
        if bin[0]!=1 or bin[1]!=1:
            im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
        if not renormalise:
            im = im * np.prod(bin) 
        im = np.uint16(im)
        # write as edf
        edf.data = im
        name = scanname_out + os.sep + scanname_out + '_edfs' + os.sep + 'ref000000.edf'
        edf.write(name)      
        f.close()

        # do the last refs
        sys.stdout.write('\ntreat final reference group: ')
        #f = h5py.File(scanname + os.sep + "post_ref.nxs")
        f = h5py.File(spooldir + os.sep + scanname + os.sep + "post_ref.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the median
        im = np.uint16(np.median(data, 0))
        if bin[0]!=1 or bin[1]!=1:
            im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
        if not renormalise:
            im = im * np.prod(bin) 
        im = np.uint16(im)
        # filter if required
        #if filterlength>1:
        #    im = convolve1d(im, np.ones(filterlength)/filterlength)  
        # write as edf
        edf.data = im
        name = scanname_out + os.sep + scanname_out + '_edfs'+os.sep+('ref%06d'%nproj)+'.edf'
        edf.write(name)      
        f.close()

        # do the darks
        sys.stdout.write("\ntreat darks: ")
        #f = h5py.File(scanname + os.sep + "post_dark.nxs")
        f = h5py.File(spooldir + os.sep + scanname + os.sep + "post_dark.nxs", 'r')
        daq = list(f.keys())[0]
        data = f[daq]
        # take the mean
        im = np.mean(data, 0)
        # median filter for hot pixel
        im = signal.medfilt2d(im, [3, 3])
        im = np.uint16(im)
        if bin[0]!=1 or bin[1]!=1:
            im = im[0:ysizein, 0:xsizein].reshape(ysizeout, bin[1], xsizeout, bin[0]).mean(3).mean(1)
        if not renormalise:
            im = im * np.prod(bin) 
        im = np.uint16(im)
        # write as edf
        edf.data = im
        name = scanname_out + os.sep + scanname_out + '_edfs' + os.sep + 'dark.edf'
        edf.write(name)         
        f.close()

    elif interactive:
        print("No references found - copy references from an existing scan...")
        copydir = filedialog.askdirectory(initialdir=os.getcwd(), title='double click into the scan to copy refs')
        if len(copydir) == 0: # we cancelled
            return
        else:
            copyname = copydir.split(os.sep)[-1]
            print("copying references from %s to %s" % (copyname, scanname))
            os.system('cp %s/%s_edfs/ref*edf %s/%s_edfs/dark*edf %s/%s_edfs/' % (copydir, copyname, copydir, copyname, scanname_out, scanname_out))
    else:
        print("No references and not interactive - cant deal with this yet...")

############## PREPROCESS HDF5 DATA ###############


if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Tomography data preprocessing tool for PyHST")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=bool)
    parser.add_argument("-a", "--acc", help="Accumulation factor for 32 to 16 bit conversion (auto = -1) [-1]", default=-1, type=int)
    parser.add_argument("-f", "--format", help="Data format [hdf5]/edf", default='hdf5', choices=['hdf5', 'edf'])
    parser.add_argument("-b", "--bin", help="Bin projections [x y z]", default=[1,1,1], nargs=3, type=int)
    parser.add_argument("-d", "--spooldir", help="Where to look for the raw data", default="", type=str)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    preprocess(args.scanname, accfac=args.acc, format=args.format, bin=args.bin, spooldir=args.spooldir, interactive=args.interactive)

    # ----------------------------------------------------- #
