
import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys



def auto_crop(scanname, xsize=0, ysize=0, zsize=0, interactive=True):
    ''' 
    crop a volume to a given size, with a centred ROI... to improve in future
    size = 0 means full reconstruction size
    '''            


    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)

    # read the current roi
    curroi = [None]*6
    curroi[0] = int(float(pars['START_VOXEL_1'][0]))
    curroi[1] = int(float(pars['START_VOXEL_2'][0]))
    curroi[2] = int(float(pars['START_VOXEL_3'][0]))
    curroi[3] = int(float(pars['END_VOXEL_1'][0]))
    curroi[4] = int(float(pars['END_VOXEL_2'][0]))
    curroi[5] = int(float(pars['END_VOXEL_3'][0]))
    # current size
    curzsize = int(pars['NUM_IMAGE_2'][0]) # max zsize from the image height
    # set the z curroi assuming this
    curroi[2] = 1
    curroi[5] = curzsize
    curxsize = (curroi[3]-curroi[0]+1) # current xsize from the image height
    curysize = (curroi[4]-curroi[1]+1) # current ysize from the image height
    # need to rerun half_acq to reset these values if they are wrong

    if interactive:
        # get reconstruction size from the user        
        print("remember, X is horizontal on the screen, Y is vertical on the screen")
        tmp = input('X size [%d]: ' % curxsize)
        xsize = curxsize if tmp == '' else int(tmp)    
        tmp = input('Y size [%d]: ' % curysize)
        ysize = curysize if tmp == '' else int(tmp)    
        tmp = input('Z size [%d]: ' % curzsize)
        zsize = curzsize if tmp == '' else int(tmp)
    else:
        # keep what is currently defined if we have passed in a zero
        xsize = curxsize if xsize == 0 else xsize
        ysize = curysize if ysize == 0 else ysize
        zsize = curzsize if zsize == 0 else zsize


    # if this a half acquisition?
    halfacq = True
    nproj = int(pars['NUM_LAST_IMAGE'][0])-int(pars['NUM_FIRST_IMAGE'][0])+1
    scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * nproj
    if not np.allclose(scanrange, 360, atol=10):
        halfacq = False
    else:
        # check the offset: 
        rotaxis = float(pars["ROTATION_AXIS_POSITION"][0])
        xsizeimage = float(pars['NUM_IMAGE_1'][0])
        delta = np.abs((xsizeimage/2)-rotaxis)
        if delta < 50:
            halfacq = False
    # fix the options
    if halfacq:
        pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'N\'}')
    else:
        pars = par_tools.par_mod(pars, 'OPTIONS', '{\'padding\':\'E\' , \'axis_to_the_center\':\'Y\' , \'avoidhalftomo\':\'N\'}')

    deltax = int((curxsize - xsize) / 2)
    deltay = int((curysize - ysize) / 2)
    deltaz = int((curzsize - zsize) / 2)
    pars = par_tools.par_mod(pars,'START_VOXEL_1', int(curroi[0]+deltax))
    pars = par_tools.par_mod(pars,'START_VOXEL_2', int(curroi[1]+deltay))
    pars = par_tools.par_mod(pars,'START_VOXEL_3', int(curroi[2]+deltaz))
    pars = par_tools.par_mod(pars,'END_VOXEL_1', int(curroi[3]-deltax))
    pars = par_tools.par_mod(pars,'END_VOXEL_2', int(curroi[4]-deltay))
    pars = par_tools.par_mod(pars,'END_VOXEL_3', int(curroi[5]-deltaz))


    print('updated par file with options...')
    par_tools.par_write(parname, pars)




def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Crop the reconstruction ROI for a tomo scan")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-x", "--xsize", help="xsize of reconstruction", default=0, type=int)
    parser.add_argument("-y", "--ysize", help="ysize of reconstruction", default=0, type=int)
    parser.add_argument("-z", "--zsize", help="zsize of reconstruction", default=0, type=int)
    
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    auto_crop(args.scanname, xsize=args.xsize, ysize=args.ysize, zsize=args.zsize, interactive=args.interactive)

    # ----------------------------------------------------- #
