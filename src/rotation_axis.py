


# python function to check centre of rotation
# by direct calculation / trial and error / user input
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

import argparse
import par_tools
import os
import pylab
pylab.ion()
from edf_read_dirty import vol_read_dirty
from fabio.edfimage import edfimage
edf = edfimage()
import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
from scipy import signal
from scipy import interpolate
import sys
import time
from edf_read_dirty import vol_read_dirty
from correlateImages import correlateImages
from skimage import morphology
from skimage import feature
import call_pyhst
import half_acq
# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def rotation_axis(scanname, manual_axis=None, guess_axis=False, filt=0, interactive=True):

    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    # save the original values
    origpars = pars.copy()

    if interactive and not (manual_axis == 'autoscan_readonly'):
        curpos = float(pars['ROTATION_AXIS_POSITION'][0])
        xsize = int(pars['NUM_IMAGE_1'][0])
        zsize = int(pars['NUM_IMAGE_2'][0])
        print("Current rotation axis position is %0.3f" % curpos)
        print("Can enter the rotation axis position, guess from the motor position, try to find it automatically (a) or by scanning (s):")
        selection = input("position / g / [a] / s : ")
        selection = "a" if selection=="" else selection
        if selection.replace(".", "", 1).isdigit():
            manual_axis = float(selection)
            selection = "m"
        if selection == "s":
            print("Will scan rotation axis position")
            lowerlimit = input("from lower limit [%0.1f] : " % (curpos-4))
            lowerlimit = curpos-4 if lowerlimit == '' else float(lowerlimit)
            upperlimit = input("to upper limit [%0.1f] : " % (curpos+4))
            upperlimit = curpos+4 if upperlimit == '' else float(upperlimit)
            stepsize = input("in step size [2] : ")
            stepsize = 2. if stepsize == '' else float(stepsize)
            roisize = input("size reconstruction ROI [%d] : " % (xsize/2))
            roisize = int(xsize/2) if roisize == '' else int(roisize)
            zslice = input("z slice to use [%d] : " % (zsize/2))
            zslice = int(zsize/2) if zslice == '' else int(zslice)
    else:
        if manual_axis != None:
            selection = "m"
            if (manual_axis == 'autoscan') or (manual_axis == 'autoscan_readonly'):
                selection = 's'        
                curpos = float(pars['ROTATION_AXIS_POSITION'][0])
                xsize = int(pars['NUM_IMAGE_1'][0])
                zsize = int(pars['NUM_IMAGE_2'][0])
                lowerlimit = curpos-5
                upperlimit = curpos+5   
                stepsize = 2.5 
                roisize = int(xsize)
                zslice = int(zsize/2)
        elif guess_axis:
            selection = 'g'
        else:
            selection = "a"

    ############### MANUAL ROTATION AXIS CHOICE ###############
    if selection == "m":
        # update par file
        # if interactive, reconstruct and show slice
        pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", manual_axis)
    ############### MANUAL ROTATION AXIS CHOICE ###############


    ############### ROTATION AXIS FROM MOTOR POS ###############
    elif selection == 'g':
        # get the possible reference_motor_aliases from BEAMLINE_PARAMETERS.par
        reference_aliases = []
        for key in beamline.keys():
            if (key[0:4] == 'tomo') and (key[-22:]=='_reference_motor_alias'):
                reference_aliases.append(beamline[key][0])
                # this is a list of current possibles. Could find a way to add others (e.g. old tomorefx) if needed

        for reference_alias in reference_aliases:   
            parfile_key = '#%s' % reference_alias
            if parfile_key in pars.keys():
                delta = 1000 * float(pars[parfile_key][0]) / float(pars['IMAGE_PIXEL_SIZE_1'][0])
                axispos = (float(pars['NUM_IMAGE_1'][0])/2) + delta 
                print("Motor positions suggest axis (%s) is %0.3f" % (reference_alias, axispos))
                if interactive:
                    tmp = input("Use this result? [y]/n : ")
                    tmp = "y" if tmp == "" else tmp
                    if tmp == "y":
                        pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", axispos)
                    else:
                        print("Not updating par file")
                        return
                else:
                    print("updating value in parameters")
                    pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", axispos)
            #else:
            #    print("No motor position found in par file - can\'t guess")
            #    return
    ############### ROTATION AXIS FROM MOTOR POS ###############


    ############### ROTATION AXIS BY CORRELATION ###############
    elif selection == "a":
        # determine the scan range        
        nproj = int(pars['NUM_LAST_IMAGE'][0])
        refndx = int(pars['FF_NUM_LAST_IMAGE'][0])
        
        scanrange = float(pars['ANGLE_BETWEEN_PROJECTIONS'][0]) * (nproj+1)
        if np.allclose(scanrange, 180, atol=10):
            scanrange = 180
            print("scanrange 180...")
        elif np.allclose(scanrange, 360, atol=10):
            scanrange = 360
            print("scanrange 360...")
        else:
            print("scan range %f is unusual - parfile problem? - carrying on..." % scanrange)

        # calculate - use calib images if available, otherwise images from scan
        print("Calculate the rotation axis position...")
        edfpath = scanname + os.sep + scanname + "_edfs" + os.sep     
        # references
        dark = 1.0*edf.read(edfpath + "dark.edf").data
        refA = 1.0*edf.read(edfpath + "ref000000.edf").data
        refB = 1.0*edf.read(edfpath + "ref%06d.edf" % refndx).data
        # images
        imA = 1.0*edf.read(edfpath + scanname + "000000.edf").data
        ndxold = nproj if scanrange == 180 else int(nproj/2) # old system - try to keep this as back up
        ndx = 180. / float(pars['ANGLE_BETWEEN_PROJECTIONS'][0])
        # make an exactly offset image
        ndxA = np.floor(ndx)
        ndxB = ndxA + 1
        f = np.mod(ndx, 1)
        if os.path.isfile(edfpath + scanname + "%06d.edf" % ndxA) and os.path.isfile(edfpath + scanname + "%06d.edf" % ndxB):
            print("Build Maxime\'s special 180 degree image")
            imBA = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxA).data
            imBB = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxB).data
            imB = (imBB*f) + (imBA*(1-f))
            movcorndx = ndxA
        elif os.path.isfile(edfpath + scanname + "%06d.edf" % ndxold):
            imB = 1.0*edf.read(edfpath + scanname + "%06d.edf" % ndxold).data
            movcorndx = ndxold
        elif os.path.isfile(edfpath + scanname + "%06d.edf" % (ndxold-1)): # in case this first missing
            imB = 1.0*edf.read(edfpath + scanname + "%06d.edf" % (ndxold-1)).data
            movcorndx = ndxold - 1
        else:
            print("Last image (%d) missing... quitting")
            return
        # flatfield
        imA = (imA - dark) / (refA - dark)
        if scanrange == 180:
            imB = (imB - dark) / (refB - dark)
        else:
            imB = (imB - dark) / (((refA+refB)/2) - dark)  
        
        # if we have a half acquisition scan, crop to get just the overlapping bit
        cropoffset = 0.
        if scanrange == 360:        
            xcen = (imA.shape[1]+1) /2.
            xsizeorig = imA.shape[1]
            print("crop data for half acquisition case... if we already have a guess for the axis")
            curpos = float(pars['ROTATION_AXIS_POSITION'][0])
            if curpos < xcen:
                xedge = int(curpos*2)
                imA = imA[:, 0:xedge]
                imB = imB[:, 0:xedge]
            else:
                xedge = int((curpos*2) - xsizeorig)
                imA = imA[:, xedge:]
                imB = imB[:, xedge:]
                cropoffset = xedge*1.

        # filter if needed
        #if interactive and filt==0:
        #    filt = input("High pass filter length [0] : ")
        #    filt = 0 if filt == '' else int(filt)
        #if filt != 0:
        #    imAs = signal.convolve2d(imA, np.ones((filt,filt))/(float(filt)**2), 'same', 'symm')
        #    imBs = signal.convolve2d(imB, np.ones((filt,filt))/(float(filt)**2), 'same', 'symm')
        #    imA = imA - imAs
        #    imB = imB - imBs

        # before correlation, flip 180 image
        imBflip = imB[:, ::-1]                        

        # is there an active movement correction to account for?
        if pars['DO_AXIS_CORRECTION'][0] == 'YES':
            print("account for the movement correction")
            movcorfile = pars['AXIS_CORRECTION_FILE'][0]
            movcor = np.loadtxt(movcorfile)
            delta = movcor[int(movcorndx)] - movcor[0]
            # apply the vertical part to imBflip
            imBflip = np.roll(imBflip, int(-delta[1]), 0)
        else:
            delta = np.zeros((2))

        # correlation
        sizex, sizey = imA.shape[1], imA.shape[0]
        c2 = np.zeros(sizex)
        # try using the median as well...
        c3 = np.zeros((sizey, sizex))
        for ii in range(sizey):
            # only include the row if the intensity is reasonable - not dark
            if imA[ii,:].mean() > 0.2:
                # correlate row by row, summing the results
                corResult = ifft(fft(imA[ii,:])*np.conj(fft(imBflip[ii,:])))
                corResult = np.abs(corResult)
                # normalise row 
                corResult = corResult - corResult.min()
                corResult = corResult / corResult.max()
                c2 = c2+corResult
                c3[ii, :] = corResult
            else:
                print("skip a row")
        c3 = np.median(c3, 0)
        corResultTotal = np.nonzero(c3==c3.max())[0][0]
        # pick out the five pixels around the peak
        # must stop this from going outside the matrix
        xdata = corResultTotal+[-2, -1, 0, 1, 2]
        xdata_ndx = np.mod(xdata, sizex).tolist()
        ydata = c3[xdata_ndx]
        # fit this with a quadratic
        # ydata = a(xdata)^2 + b(xdata) + c
        xdata = np.array(xdata)
        xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
        a,b,c = np.linalg.lstsq(xdata, ydata, rcond=None)[0]
        # find the maximum of this, derivate
        # dy/dx = 2a(xdata) + b = 0
        # x = -b / 2a        
        corResultFine = -b / (2*a)
        if corResultFine > (sizex/2.):
            axispos = corResultFine/2.
        else:
            axispos = (sizex-1+corResultFine)/2.
        #### repeat with the median ###
        #corResultTotal = np.nonzero(c3==c3.max())[0][0]
        ## pick out the five pixels around the peak
        ## must stop this from going outside the matrix
        #xdata = corResultTotal+[-2, -1, 0, 1, 2]
        #xdata_ndx = np.mod(xdata, sizex).tolist()
        #ydata = c3[xdata_ndx]
        ## fit this with a quadratic
        ## ydata = a(xdata)^2 + b(xdata) + c
        #xdata = np.array(xdata)
        #xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
        #a,b,c = np.linalg.lstsq(xdata, ydata)[0]
        ## find the maximum of this, derivate
        ## dy/dx = 2a(xdata) + b = 0
        ## x = -b / 2a        
        #corResultFine = -b / (2*a)
        #if corResultFine > (sizex/2.):
        #    axispos2 = corResultFine/2.
        #else:
        #    axispos2 = (sizex-1+corResultFine)/2.

        # add teh offset in case we cropped images
        axispos = axispos + cropoffset
        # add the movement correction delta - we change the corResult by delta... so we change the axis by delta/2
        axispos = axispos + (delta[0]/2.)

        print("Rotation axis seems to be %0.3f" % axispos)
        #print("( using median gives %0.3f )" % axispos2)

        # so - things to add - despeckle to remove bad spots
        # can use mean.edf, could also correct using double ff image
        # use skimage feature.register_translation
        # need to check if mean exists
        #print("build despeckled images for correlation with skimage")
        #meanname = edfpath + os.sep + "mean.edf"
        #meanim = edf.read(meanname).getData()
        #mask = np.zeros(meanim.shape)
        #mask[np.nonzero(meanim>0.98)] = 1 # (overly) bright spots
        #mask = morphology.binary_dilation(mask, np.ones((15,15))) # mask the edges too
        #mask2 = morphology.binary_dilation(mask, np.ones((3,3))) 
        #mask2 = mask2 - mask # just the edges of the spots
        #tmpA = imA * mask2
        #tmpA = morphology.dilation(tmpA, np.ones((51,51))) # dilate enough to close spots
        #imA[np.nonzero(mask)] = tmpA[np.nonzero(mask)]
        #tmpB = imB * mask2
        #tmpB = morphology.dilation(tmpB, np.ones((51,51))) # dilate enough to close spots
        #imB[np.nonzero(mask)] = tmpB[np.nonzero(mask)]
        #imBflip = imB[:, ::-1]
        #cx,cy = correlateImages(imA, imBflip, filt=11)
        #axispos3 = (sizex+1-cx)/2.
        #print("(( Using image correlation gives %0.3f ))" % axispos3)

        selection = ""
        if interactive:
            selection = input("Use this result? [y]/n : ")
        selection = "y" if selection == "" else selection
        if selection == "y":
            pars = par_tools.par_mod(pars, "ROTATION_AXIS_POSITION", axispos)
        else:
            print("Not updating par file")
            return
        #print("ANDY TESTING SOMETHNG !!!  NOT MODIFYING PAR FILE"  )
        ## should probably return the calculation using the nominal 180 image too      
        #curpos = float(pars['ROTATION_AXIS_POSITION'][0])
        #print("was %f, would be %f" % (curpos, axispos3))
        #return curpos, axispos3[0]
     ############### ROTATION AXIS BY CORRELATION ###############

     ############# ROTATION AXIS BY RECONSTRUCTION ##############
    else:
        # do find axis 2 process
        # create a subdir
        if not os.path.exists(scanname+os.sep+"tmp"):
            os.mkdir(scanname+os.sep+"tmp")
        # tmp parfile
        expdir = os.getcwd()
        tmpparname = expdir + os.sep + scanname + os.sep + "tmp" + os.sep + "tmp.par"
        # apply ROI
        xsize = float(pars['NUM_IMAGE_1'][0])
        zsize = float(pars['NUM_IMAGE_2'][0])
        xstart = int(np.floor((xsize/2)-(roisize/2)))
        xend = int(xstart + roisize -1)
        tmppars = par_tools.par_mod(pars, 'START_VOXEL_1', xstart)
        tmppars = par_tools.par_mod(tmppars, 'START_VOXEL_2', xstart)
        tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_1', xend)
        tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_2', xend)
        tmppars = par_tools.par_mod(tmppars, 'START_VOXEL_3', zslice)
        tmppars = par_tools.par_mod(tmppars, 'END_VOXEL_3', zslice) 
        # axis positions - do upperlimit position
        upperlimit = upperlimit+0.5
        pos = np.arange(lowerlimit, upperlimit, stepsize)
        slices = np.zeros((roisize, roisize, len(pos)))
        # do the reconstructions
        if not manual_axis == 'autoscan_readonly':
            for ii in range(len(pos)):  
                slicename = scanname + os.sep + "tmp" + os.sep + "tmp_%d_.vol" % ii
                tmppars = par_tools.par_mod(tmppars, "ROTATION_AXIS_POSITION", pos[ii])
                tmppars = par_tools.par_mod(tmppars, 'OUTPUT_FILE', slicename)
                par_tools.par_write(tmpparname, tmppars)
                sys.stdout.write('start slice for axis %0.1f...\n' % pos[ii])
                sys.stdout.flush()
                # local version
                #os.system('PyHST2_2015b %s PSICHEGPU,0,1 > /dev/null' % tmpparname)
                #os.system('PyHST2_2015b %s > /dev/null' % tmpparname)
                # use a helper function to treat multiple beamlines
                call_pyhst.call_pyhst(tmpparname)
        else:
            print("Just reading existing images! Not doing reconstructions!")
        # read results
        for ii in range(len(pos)):  
            # read the slice
            slicename = scanname + os.sep + "tmp" + os.sep + "tmp_%d_.vol" % ii
            success = False
            timeout = 0
            while not success and (timeout<5):
                try:
                    myslice = vol_read_dirty(slicename, volsize=[roisize, roisize ,1], roi=[0,0,0,roisize,roisize,1], datatype=np.float32)
                    slices[:,:,ii] = myslice[:,:,0]
                    success = True
                except:
                    print("vol_read failed for %s...  try again" % slicename  )
                    time.sleep(1)
                    timeout+=1
        if interactive:		    
            # show results
            pylab.figure()
            x = np.ceil(float(len(pos))**0.5)
            if (x*(x-1))>float(len(pos)):
                y = x-1
            else:
                y = x
            mymin = slices.mean()-(3*slices.std())
            mymax = slices.mean()+(3*slices.std())
            for ii in range(len(pos)):
                pylab.subplot(y,x,ii+1)
                pylab.imshow(slices[:,:,ii].T)
                pylab.gray()
                pylab.clim(mymin, mymax)
                pylab.title("%0.1f" % pos[ii])
            # ask for the best guess
            selection = input("enter the best rotation axis position [default no change] : ")
            if selection.replace(".", "", 1).isdigit():
                # apply this to the original pars so as not to lose the ROI
                pars = par_tools.par_mod(origpars, "ROTATION_AXIS_POSITION", float(selection))
            else:
                pars = origpars
                print("Not updating par file")
        else:
            print("scanning but not interactive, so dont change par file")
            pars = origpars
    ############# ROTATION AXIS BY RECONSTRUCTION ##############

    # Finally, save the par file
    par_tools.par_write(parname, pars)

    # in the case of guessing, now call half acq functions
    if selection=='g' and interactive:
        print("Automatically call half-acquisition padding option for extended slice")
        half_acq.half_acq(scanname)    

    return 


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Find/change rotation axis in tomography data")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    parser.add_argument("-m", "--manual", help="Specify rotation axis position", default=None)
    parser.add_argument("-f", "--filter", help="High pass filter length", default=0, type=int)
    parser.add_argument("-g", "--guess", help="Guess rotation axis position from motor positions", default=False, type=testbool)

    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    rotation_axis(args.scanname, manual_axis=args.manual, guess_axis=args.guess, filt=args.filter, interactive=args.interactive)

    # ----------------------------------------------------- #
