

# python function to check centre of rotation
# by direct calculation / trial and error / user input
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

import argparse
import par_tools
import os
import glob
import launch_reconstruction
import preprocess_all
import preprocess_fast
import pickle
from tkinter import filedialog
import sys
import call_pyhst

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def reconstruct_all(redo=False, useroi=False, scanlist=None, instructions=None, timeout=None, spooldir=None, interactive=True):
    '''
    Reconstruct all / all unreconstructed volumes in an experiment
    Can search for un-rceonstructed scans
    Can pass in a list of scans for reconstruction
    Can pass in a list of instructions (python commands to be called outside ipython, scanname replaced by XXX)
    e.g. python ~/Python/double_ff.py XXX --filtx=55 --filtz=55 --pixelmask=True
    If we supply a spooldir, use this to find the initial list.
    '''
    
    # get the expdir
    expdir = os.getcwd()
    
    # where is the python code?
    pythondir = beamline['pythondir'][0]

    # if the user passed in a list file
    if scanlist != None:
        fh = open(scanlist, 'rt')
        mylistA = fh.readlines()
        fh.close()
        # remove end of line characters
        mylist = []
        for name in mylistA:
            mylist.append(name.strip())
    else:

        # list all scans in the directory - the spool if supplied, otherwise the expdir
        if interactive:
            tmp = input('Do you need to preprocess data? y/[n] : ')
            if tmp.lower() in ['y', 'yes', 'true']:
                spooldir = filedialog.askdirectory(title='Select spool directory', initialdir='/nfs/spool1')
                tmp = input('Look for spiral scans to treat individually? y/[n] : ')
                spirals = True if tmp.lower() in ['y', 'yes', 'true'] else False
            tmp = input('Search string for glob? e.g. toto* (None) : ')
            globstring = None if tmp == '' else tmp
        if spooldir == None:
            if globstring==None:
                mylistB = sorted(glob.glob(expdir + os.sep + '*'))
            else:
                mylistB = sorted(glob.glob(expdir + os.sep + globstring))
        else:
            if globstring==None:
                mylistB = sorted(glob.glob(spooldir + os.sep + '*'))
                if spirals:
                    mylistC = sorted(glob.glob(spooldir + os.sep + '*spiral/*'))
                    mylistB = mylistB + mylistC
            else:
                mylistB = sorted(glob.glob(spooldir + os.sep + globstring))
                if spirals:
                    mylistC = sorted(glob.glob(spooldir + os.sep + globstring + '/*'))
                    mylistB = mylistB + mylistC

        mylistA = []
        preprolist = []

        # first cleaning of the list 
        # mylistB is the list of either the local directory or the spooldirectory.
        # if it is the spooldir, can need to preprocess
        print('______________________________________________________')
        for fullname in mylistB:
            name = fullname.split(os.sep)[-1]
            if name[-9:-2] == 'spiral_': # spiral scan, make spiral name
                spiraldir = name[:-3] 
            else:
                spiraldir = ''
            if os.path.isfile(name):
                print("%s is a file, not a scan" % name)
            elif (not os.path.exists(name + os.sep + name + '.par')) or (not os.path.exists(name + os.sep + name + '_edfs/dark.edf')):
                # either there is no par file, or this is on the spool and has not been preprocessed yet
                if (spooldir != None):
                    if os.path.exists(spooldir + os.sep + name + os.sep + name + '.synchro_log'):
                        # this looks like a fast series - get all scans via the synchro_log!
                        fh = open(spooldir + os.sep + name + os.sep + name + '.synchro_log', 'rt')
                        lines = fh.readlines()
                        fh.close()
                        for ii,line in enumerate(lines):
                            ang = line.split(', ')[1].strip() # this is either the angle, or "ref"
                            if ang != "refs":
                                step = ii + 1
                                fastname = name + '_fast_%d_' % step
                                mylistA.append(fastname) # want this in the reconstruction list!
                                if not os.path.exists(fastname + os.sep + fastname + '.par'):
                                    preprolist.append(fastname) # this is name that the scan will eventually have

                    elif os.path.exists(spooldir + os.sep + name + os.sep + name + '.par'):
                        # this looks like a normal scan
                        print("%s has a par file on the spool - preprocess first !" % name)
                        preprolist.append(name)
                        mylistA.append(name) # then reconstruct !
                    elif os.path.exists(spooldir + os.sep + spiraldir + os.sep + name + os.sep + name + '.par'):
                        # spiral case
                        print("%s has a par file on the spool (spiral scan) - preprocess first !" % name)
                        preprolist.append(name)
                        mylistA.append(name) # then reconstruct !
                    else:
                        print("%s has no par file on the spool - won\'t reconstruct" % name)
                else:
                    # not looking at the spool
                    print("%s has no par (or edfs) file - won\'t reconstruct" % name)
            else:
                mylistA.append(name)
        print('______________________________________________________')
        if spooldir != None:
            print("Found %d scans to preprocess in %s" % (len(preprolist), spooldir))
            print("Found %d scans in %s and %s" % (len(mylistA), spooldir, expdir))
        else:
            print("Found %d scans in %s" % (len(mylistA), expdir))

        if interactive:
            print("Can reconstruct all scans. Do you want to reconstruct if a volume already exists?")
            tmp = input('Repeat reconstruction where reconstructed volume exists ? [y]/n : ')
            redo = True if tmp.lower() in ['', 'y', 'yes', 'true'] else False
            tmp = input('Use ROI if defined in the par file ? y/[n] : ')
            useroi = False if tmp.lower() in ['', 'n', 'no', 'false'] else True

        print('______________________________________________________')
        print("checking for existing volumes...")
        mylist = []
        for scanname in mylistA:
            parname = scanname + os.sep + scanname + ".par"
            if os.path.exists(parname):
                pars = par_tools.par_read(parname)                
                if ('DO_PAGANIN' in pars.keys()) and (pars['DO_PAGANIN'][0]=='1'):
                    pag = True
                else:
                    pag = False

                # check if the vol.info file exists
                if pag:
                    infoname = scanname + os.sep + scanname + '_0_pag.vol.info'
                    rawinfoname = scanname + os.sep + scanname + '_0_pag.raw.info'
                else:
                    infoname = scanname + os.sep + scanname + '_0.vol.info'
                    rawinfoname = scanname + os.sep + scanname + '_0.raw.info'
                volexists = os.path.exists(infoname) or os.path.exists(rawinfoname)

                if redo or not volexists:
                    print("%s : will reconstruct" % scanname)
                    mylist.append(scanname)
                else:
                    print("%s : will skip !" % scanname )
            elif scanname in preprolist:
                mylist.append(scanname) # reconstruct after preprocessing
            else:
                print("parfile not found for %s" % scanname)

        print('______________________________________________________')


        if interactive:
            tmp = input('Save the list of scans as a text file for editing? y/[n] : ')
            savelist = True if tmp.lower() in ['y', 'yes', 'true'] else False
            if savelist:
                print('saving the list as reconstruction_list.txt')
                f = open('reconstruction_list.txt', 'wt')
                for name in mylist:
                    f.write('%s\n' % name)
                f.close()
                if len(preprolist)!=0:
                    print('saving the list as preprocess_list.txt')
                    f = open('preprocess_list.txt', 'wt')
                    for name in preprolist:
                        f.write('%s\n' % name)
                    f.close()
                print("quitting")
                return

        if interactive:
            tmp = input('Load a list of scans from a text file? y/[n] : ')
            loadlist = True if tmp.lower() in ['y', 'yes', 'true'] else False
            if loadlist:
                mylist = [] # clear the current list!
                filename = filedialog.askopenfilename(title='select a list of files')
                fh = open(filename, 'rt')
                mylistA = fh.readlines()
                fh.close()
                # remove end of line characters
                for name in mylistA:
                    mylist.append(name.strip())

        if interactive and (spooldir!=None):
            tmp = input('Load a list of scans for preprocessing from a text file? y/[n] : ')
            loadlist = True if tmp.lower() in ['y', 'yes', 'true'] else False
            if loadlist:
                preprolist = [] # clear the current list!
                filename = filedialog.askopenfilename(title='select a list of files')
                fh = open(filename, 'rt')
                mylistA = fh.readlines()
                fh.close()
                # remove end of line characters
                for name in mylistA:
                    preprolist.append(name.strip())
                # select the spool directory
                

    if instructions == None:
        tmp = input('Load a list of instructions? y/[n] : ')
        loadinst = True if tmp.lower() in ['y', 'yes', 'true'] else False
        if loadinst:
            filename = filedialog.askopenfilename(title='select an instructions file')
            fh = open(filename, 'rt')
            instructionsA = fh.readlines()
            fh.close()
            # remove end of line characters
            instructions = []
            for name in instructionsA:
                instructions.append(name.strip())
            print("Remember that the instructions should include the launch_reconstruction.py !")
        else:
            print("No instructions - will simply launch each reconstruction")
            if timeout == None:
                tmp = input('Timeout for reconstructions in seconds [0 - no timeout] : ')
                timeout = 0 if tmp == '' else int(tmp)



    tmp = input('Launch all %d preprocessing and %d reconstructions [y]/n : ' % (len(preprolist), len(mylist)))
    tmp = True if tmp.lower() in ['n', 'no', 'false'] else False
    if tmp:
        print('Not launch reconstructions - quitting')
        return


    # this is the new bit
    for scanname in mylist:
        # create the folder if needed
        scanfolder = expdir + os.sep + scanname
        if not os.path.exists(scanfolder):
            os.mkdir(scanfolder)
        # write the script file
        scriptname = scanfolder + os.sep + scanname + '_protocol.slurm'
        f = open(scriptname, 'wt')
        if scanname in preprolist:
            # need to preprocess first
            parts = scanname.split('_')
            if (parts[-2].isdigit()) and (parts[-3]=='fast'):
                # this looks like a fast scan
                print("preprocess fast series : %s" % scanname)
                fastnameroot = scanname[0:scanname.rfind('_fast_')] # just the first part of the name
                mystep = int(parts[-2]) # the step
                f.write('python %s/preprocess_fast.py %s -s %d -d %s\n' % (pythondir, fastnameroot, mystep, spooldir))
            elif scanname[-9:-2] == 'spiral_': 
                # spiral scan, make spiral name
                print("this is a spiral scan")
                spiraldir = spooldir + os.sep + scanname[:-3]              
                f.write('python %s/preprocess_all.py %s -d %s\n' % (pythondir, scanname, spiraldir))
            else:
                # hopefully a normal scan
                f.write('python %s/preprocess_all.py %s -d %s\n' % (pythondir, scanname, spooldir))
        # now, deal with the instructions
        if instructions == None:
            # just reconstruct
            f.write('python %s/launch_reconstruction.py %s -r %s -t %d\n' % (pythondir, scanname, useroi, timeout))
        else:
            # write for all the instructions
            for myinstruction in instructions:
                f.write(myinstruction.replace('XXX', scanname) + '\n')
        # and close the script file
        f.close()
        # and run it
        call_pyhst.batch_script(scriptname)
         
    



def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Reconstruct all scans for an experiment")
    parser.add_argument("-x", "--redo", help="Reconstruct even if a volume exists", default=False, type=testbool)
    parser.add_argument("-r", "--roi", help="Use ROI defined in par file True/[False]", default=False, type=testbool)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)

    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    reconstruct_all(redo=args.redo, interactive=args.interactive)

    # ----------------------------------------------------- #
