# python function to view the current slice, if one exists
# user input, display result
# launch from the experiment directory
# scan name is argument
# data structure is: 
# experimentdir/scanname/scanname_edfs/*edfs
# experimentdir/scanname/scanname.par

import argparse
import par_tools
import os
import pylab
pylab.ion()
from edf_read_dirty import vol_read_dirty
import numpy as np

# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()

def view_only(scanname, unit='mm', interactive=True):
    # no reason for not being interactive... keep default to false outside ipython to be consistent
    
    # make sure the unit is either pixels or mm
    unit = 'pixels' if  unit != 'mm' else 'mm'

    # do we have a reconstructed slice?
    parname = scanname + os.sep + scanname + "_slice.par"
    if os.path.isfile(parname):
        pars = par_tools.par_read(parname)
    else:
        print("No slice .par file found...")
    slicename = scanname + os.sep + scanname + "_slice.vol"
    if not os.path.isfile(parname):
        print("No slice .vol file found...")
        return
    print("load the current slice")

    # read the slice
    # x/y ROI is inherited from the main par file
    roi1 = int(pars['END_VOXEL_1'][0]) - int(pars['START_VOXEL_1'][0]) + 1
    roi2 = int(pars['END_VOXEL_2'][0]) - int(pars['START_VOXEL_2'][0]) + 1
    slicendx = int(float(pars['END_VOXEL_3'][0])) # just in case
    try:
        myslice = vol_read_dirty(slicename, volsize=[roi1, roi2 ,1], roi=[0,0,0,roi1,roi2,1], datatype=np.float32)
        myslice = myslice[:,:,0]
    except:
        print("vol_read failed for %s...  " % slicename)
        print("re-run \"v\" to visualise the slice")
        return

    # get orientation same as ImageJ
    mymin = myslice.mean()-(3*myslice.std())
    mymax = myslice.mean()+(3*myslice.std())

    # add the instrument coordinates
    pixsize = float(pars['IMAGE_PIXEL_SIZE_1'][0])/1000. # in mm
    imsizemmx = pixsize * roi1
    imsizemms = pixsize * roi2
    volsizemmz = pixsize * float(pars['NUM_IMAGE_2'][0])

    # what are the sample alignment motors?
    xmotors = []
    smotors = []
    zmotors = []
    for name in beamline.keys():
        if name.find('sample_tz_motor_alias')!=-1:
            zmotors.append(beamline[name][0])
        elif name.find('sample_tx_motor_alias')!=-1:
            xmotors.append(beamline[name][0])
        elif name.find('sample_ts_motor_alias')!=-1:
            smotors.append(beamline[name][0])
        else:
            pass

    # look for these motors in the par file comments - only expecting to find one
    # default positions and labels
    xpos = 0
    spos = 0
    zpos = 0
    xlabel = 'relative sample x (%s)' % unit
    slabel = 'relative sample s (%s)' % unit
    zname = None
    for name in xmotors:
        commentname = '#'+name
        if commentname in pars.keys():
            xlabel = '%s (%s)' % (name, unit)
            if unit == 'mm':
                xpos = float(pars['#'+name][0])
    for name in smotors:
        commentname = '#'+name
        if commentname in pars.keys():
            slabel = '%s (%s)' % (name, unit)
            if unit == 'mm':
                spos = float(pars['#'+name][0])
    for name in zmotors:
        commentname = '#'+name
        if commentname in pars.keys():
            zname = name
            if unit == 'mm':
                zpos = float(pars['#'+name][0])
                zpos = zpos + (volsizemmz/2) - (slicendx*pixsize)

    extentmm = [xpos+(imsizemmx/2), xpos-(imsizemmx/2), spos+(imsizemms/2), spos-(imsizemms/2)]

    if zname == 'ptomotz':
        # show ptz as well
        ptzpos = float(pars['#ptz'][0])

    if interactive:
        fig = pylab.figure()
        if unit=='mm':
            pylab.imshow(myslice.T, extent=extentmm)
            if zname == None:
                pylab.title(scanname + ', slice %d' % slicendx)
            else:
                if zname == 'ptomotz':
                    pylab.title(scanname + ', slice %d (%s=%0.2f + ptz=%0.2f)' % (slicendx, zname, zpos, ptzpos))
                else:
                    pylab.title(scanname + ', slice %d (%s=%0.2f)' % (slicendx, zname, zpos))
        else:
            pylab.imshow(myslice.T)
            pylab.title(scanname + ', slice %d' % slicendx)

        pylab.xlabel(xlabel)
        pylab.ylabel(slabel)
            
        

        pylab.clim(mymin, mymax)
        pylab.gray()
        pylab.show()
    else:
        # if not interactive, return the slice to be used
        return myslice.T

def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    # outside of ipython - handle arguments and defaults with argparse
    # from outside ipython, interactive off by default
    # from inside ipython, interactive on by default
    parser = argparse.ArgumentParser(description="Display the current reconstructed slice")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-u", "--unit", help="Unit for displaying images [mm]/pixel", default='mm')
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=False, type=testbool)
    args = parser.parse_args()
    
    print(args)

    # now, call movement_correction with these arguments
    view_only(args.scanname, unit=args.unit, interactive=args.interactive)

    # ----------------------------------------------------- #
