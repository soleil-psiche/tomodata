
import numpy as np
ifft2 = np.fft.ifft2
fft2 = np.fft.fft2 
ifft3 = np.fft.ifftn
fft3 = np.fft.fftn 

from scipy import signal

###########################################################################################################"
def correlateImages(im0, im1, filt=0):
    '''Correlate two images, return the shift. Optional filt is a high pass filter'''

    ysize,xsize = im0.shape

    if filt>0:
        im1s = signal.convolve2d(im1, np.ones((filt,filt))/(filt**2), 'same', 'symm')
        im0s = signal.convolve2d(im0, np.ones((filt,filt))/(filt**2), 'same', 'symm')
        im1 = im1 - im1s
        im0 = im0 - im0s

    # remove edges
    im1 = im1[filt:(ysize-filt), filt:(xsize-filt)]
    im0 = im0[filt:(ysize-filt), filt:(xsize-filt)]
    
    # correlate
    cor = np.real(ifft2(fft2((im1))*np.conj(fft2((im0)))))
    cy,cx = np.nonzero(cor == cor.max())
    if -(cx-(xsize-(2*filt))) < cx:
        cx = cx-(xsize-(2*filt))
    if -(cy-(ysize-(2*filt))) < cy:
        cy = cy-(ysize-(2*filt))

    return cx,cy
###########################################################################################################

###########################################################################################################"
def correlateVolumes(im0, im1, filt=0):
    '''Correlate two images, return the shift. Optional filt is a high pass filter'''

    ysize,xsize,zsize = im0.shape

    if filt>0: # 2D filtering only...
        for ii in range(zsize):
            im1s = signal.convolve2d(im1[:,:,ii], np.ones((filt,filt))/(filt**2), 'same', 'symm')
            im0s = signal.convolve2d(im0[:,:,ii], np.ones((filt,filt))/(filt**2), 'same', 'symm')
            im1[:,:,ii] = im1[:,:,ii] - im1s
            im0[:,:,ii] = im0[:,:,ii] - im0s

    # remove edges
    im1 = im1[filt:(ysize-filt), filt:(xsize-filt), :]
    im0 = im0[filt:(ysize-filt), filt:(xsize-filt), :]
    
    # correlate
    cor = np.real(ifft3(fft3((im1))*np.conj(fft3((im0)))))
    cy,cx,cz = np.nonzero(cor == cor.max())
    if -(cx-(xsize-(2*filt))) < cx:
        cx = cx-(xsize-(2*filt))
    if -(cy-(ysize-(2*filt))) < cy:
        cy = cy-(ysize-(2*filt))
    if -(cz-zsize) < cz:
        cz = cz-zsize

    return cx,cy,cz
###########################################################################################################

