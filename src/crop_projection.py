import argparse
import par_tools
import os
import numpy as np
import pylab
pylab.ion()
from fabio.edfimage import edfimage
edf = edfimage()
import time
import sys
from scipy import signal
import glob


def crop_projection(scanname, cp=100, interactive=True):
   
    # read the par file
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    nproj = int(pars['NUM_LAST_IMAGE'][0]) - int(pars['NUM_FIRST_IMAGE'][0]) + 1
    xsize = int(pars["NUM_IMAGE_1"][0])
    zsize = int(pars["NUM_IMAGE_2"][0])
    if interactive:
        tmp = input('cropsize:[%d] ' % cp)
        cropsize = cp if tmp == '' else int(tmp)

    ndx1 = int((int(zsize)-int(cropsize))/2)
    ndx2 = int((int(zsize)+int(cropsize))/2)
    expdir = os.getcwd()
    myfolder = expdir + os.sep + scanname + os.sep + 'crop'
    if not os.path.exists(myfolder):
        os.mkdir(myfolder)
    
    # read the references
    lastref = int(pars['FF_NUM_LAST_IMAGE'][0])
    refA = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'ref000000.edf').getData()*1.
    refB = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +('ref%06d.edf' % (lastref))).getData()*1.
    dark = edf.read(scanname + os.sep + scanname + "_edfs" + os.sep +'dark.edf').getData()*1.

    for ii in range(nproj):
        im = edf.read(scanname + os.sep + scanname + "_edfs"  + os.sep + ('%s%06d.edf' % (scanname, ii))).getData()*1.
        im = (im*1. - dark) / (refA - dark)
        imc = im[ndx1:ndx2, :]
        imc = np.float32(imc)
        edf.setData(imc)
        edf.write(scanname + os.sep + 'crop' + os.sep + ('crop_%s_%06d.edf' % (scanname, ii)))

    # update the normal par file - re-read
    parname = scanname + os.sep + scanname + ".par"
    pars = par_tools.par_read(parname)
    
    # remove the "normal" ff correction
    pars = par_tools.par_remove(pars, 'DOUBLEFFCORRECTION')
    # image paths 
    pars = par_tools.par_mod(pars, 'FILE_PREFIX', expdir + os.sep + scanname + os.sep + 'crop' + os.sep + "crop_" + scanname + "_")       
    pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', ('%d' % (cropsize)))
    pars = par_tools.par_mod(pars, 'START_VOXEL_3', ('%d' % (cropsize/2)))
    pars = par_tools.par_mod(pars, 'END_VOXEL_3', ('%d' % (cropsize/2)))
    pars = par_tools.par_mod(pars, 'SUBTRACT_BACKGROUND', 'NO')
    pars = par_tools.par_mod(pars, 'CORRECT_FLATFIELD', 'NO')
    # save the updated pars!
    parname2 = scanname + os.sep + scanname + "_crop.par"
    par_tools.par_write(parname2, pars)


def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Crop the projection for faster computation")
    parser.add_argument("scanname", help="The name of the scan you want to process", default=None)
    parser.add_argument("-i", "--interactive", help="Interactive mode [False]/True", default=True, type=testbool)
    parser.add_argument("-c", "--cropsize", help="vertical crop size", default=100, type=int)
    
    args = parser.parse_args()
    
    print(args)

    # now, call with these arguments
    crop_projection(args.scanname, cp=args.cropsize, interactive=args.interactive)

    # ----------------------------------------------------- #
    
