tomodata
========

Description
-----------

This is the Python code used at SOLEIL as a user interface to PyHST2.

It uses basic and standard Python libraries as much as possible.  It assumes that PyHST2 has been installed.

This is the main brach of the code, which assumes that projections are in edf format.

How to install
--------------

- Clone the code
- "local" config information is in two places:
- (1) ~/BEAMLINE_PARAMETERS.par
    This is a config file, using PyHST2 par file format.
    The aim is that all beamline specific information should be here, and that everything else is generic, except...
- (2) call_pyhst.py - This contains the syntax needed to call PyHST2 in different situations. 


Credits
-------
Andy King
PyHST2 from Alessandro Mirone at the ESRF
